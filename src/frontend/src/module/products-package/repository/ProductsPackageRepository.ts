import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CRUDSerialRepositoryInterface} from "../../crud/helpers/position/interfaces";
import {ProductsPackageEntity} from "../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {ListProductsPackageResponse200} from "../../../../definitions/src/definitions/products-package/paths/list";
import {updatePosition} from "../../crud/functions/updatePosition";
import {ProductsPackageRESTService} from "../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Injectable()
export class ProductsPackageRepository implements CRUDSerialRepositoryInterface
{
	private etg: ProductsPackageEntity[] = [];

	constructor(private rest: ProductsPackageRESTService) {
	}

	fetch(): Observable<ListProductsPackageResponse200> {
		let observable = this.rest.getProductsPackagesList();

		observable.subscribe(response => {
			this.etg = response.product_packages;
		});

		return observable;
	}

	getAll(): ProductsPackageEntity[] {
		return this.etg;
	}

	getById(id: number) {
		let result = this.etg.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`ETG with id "${id}" not found`);
		}

		return id;
	}

	addETG(entity: ProductsPackageEntity) {
		this.etg.push(entity);
	}

	removeETG(id: number) {
		this.etg = this.etg.filter(entity => entity.id !== id);
	}

	replaceETG(id: number, etg: ProductsPackageEntity) {
		this.etg = this.etg.map(entity => {
			return entity.id === id ? etg : entity;
		})
	}

	getTotal(): number {
		return this.etg.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}
}