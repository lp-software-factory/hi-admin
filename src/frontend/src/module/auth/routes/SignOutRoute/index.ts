import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthModalsService} from "../../component/Modals/service";

@Component({
	template: require('./template.jade')
})
export class SignOutRoute
{
	constructor(private authModals: AuthModalsService,
		private router: Router,) {
		this.authModals.closeAllModals();
	}

	close() {
		this.router.navigate(['/admin/protected']);
	}

	success() {
		this.router.navigate(['/admin/public/sign-in']);
	}
}