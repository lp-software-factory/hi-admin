import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {GetAllOAuth2HandlersResponse200} from "../../../../../definitions/src/definitions/oauth2/providers/paths/get-all-handlers";
import {OAuth2ProviderHandlersRepository} from "../repository/OAuth2ProviderHandlersRepository";

@Injectable()
export class OAuth2ProviderHandlersRepositoryResolve implements Resolve<GetAllOAuth2HandlersResponse200>
{
	constructor(private repository: OAuth2ProviderHandlersRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}