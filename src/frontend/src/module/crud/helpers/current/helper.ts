import {Injectable} from "@angular/core";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {DTCursor} from "../../../datatable/helpers/DTCursor";

@Injectable()
export class CRUDCurrentRecordHelper<T>
{
	constructor(private cursor: DTCursor<T>,
		private loadingManager: LoadingManager,) {
	}

	getCurrentEntity(): T {
		let cursor = this.cursor;

		if(cursor.has()) {
			return cursor.current();
		} else {
			throw new Error('No current record available');
		}
	}

	cleanCurrentEntity() {
		this.cursor._current = undefined;
	}

	hasActiveEntity(): boolean {
		return !this.loadingManager.isLoading() && this.cursor.has();
	}
}