import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EventTypeGroupEntity} from "../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";

@Component({
	selector: 'hi-event-type-group-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EventTypeGroupDataTable
{
	@Input('entities') entities: EventTypeGroupEntity[] = [];

	constructor(private cursor: DTCursor<EventTypeGroupEntity>,) {
	}

	getCursor(): DTCursor<EventTypeGroupEntity> {
		return this.cursor;
	}
}