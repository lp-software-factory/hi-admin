import {Observable} from "rxjs";
import {SerialEntity} from "../../../../../definitions/src/definitions/_common/markers/SerialEntity";
import {IdEntity} from "../../../../../definitions/src/definitions/_common/markers/IdEntity";
import {Success200} from "../../../../../definitions/src/definitions/_common/response/Success200";

export interface SerialAndIdEntity extends SerialEntity, IdEntity
{
}

export interface CRUDSerialRepositoryInterface
{
	getTotal(): number;
	moveUp(id: number, oldPosition: number, newPosition: number): void;
	moveDown(id: number, oldPosition: number, newPosition: number): void;
}

export interface UpdatePositionResponse200 extends Success200
{
	position: number;
}

export interface CRUDSerialEntityRESTService
{
	moveEntityUp(id: number): Observable<UpdatePositionResponse200>;
	moveEntityDown(id: number): Observable<UpdatePositionResponse200>;
}