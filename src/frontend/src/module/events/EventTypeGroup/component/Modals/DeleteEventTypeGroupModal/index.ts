import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {EventTypeGroupEntity} from "../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EventTypeGroupRESTService} from "../../../../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

@Component({
	selector: 'hi-event-type-group-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteEventTypeGroupModal implements AfterViewInit
{
	@Input('entity') entity: EventTypeGroupEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EventTypeGroupRESTService) {
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let eventTypeGroupId = this.entity.id;
		let loading = this.status.addLoading();

		this.service.deleteEventTypeGroup(eventTypeGroupId).subscribe(() => {
			this.successEvent.emit(eventTypeGroupId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}