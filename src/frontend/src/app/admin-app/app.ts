/// <reference path="./../../../typings/main/index.d.ts" />

import "es6-shim";
import "es6-promise";
import "reflect-metadata";
import "rxjs/Rx";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {AppModule} from "./module/app.module";

require('zone.js');

window['Cropper'] = require('cropperjs');

require('./../../styles/index.scss');
require('./../../styles/globals/index.head.scss');
require('./../../../node_modules/font-awesome/css/font-awesome.css');
require('./../../../node_modules/cropperjs/dist/cropper.min.css');

/* enableProdMode(); */

setTimeout(() => {
	platformBrowserDynamic().bootstrapModule(AppModule);
}, 0);