import {TranslationService} from "./service/TranslationService";
import {TranslatePipe} from "./pipe/TranslatePipe";
import {LocalizePipe} from "./pipe/LocalizePipe";

export const HII18nModule = {
	declarations: [
		TranslatePipe,
		LocalizePipe,
	],
	providers: [
		TranslationService,
	]
};