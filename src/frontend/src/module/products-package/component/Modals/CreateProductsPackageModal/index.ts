import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {ProductsPackageEntity} from "../../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {CreateProductsPackageRequest} from "../../../../../../definitions/src/definitions/products-package/paths/create";
import {PalettePresetsRepository} from "../../../../palette/repository/PalettePresetsRepository";
import {ProductsPackageRESTService} from "../../../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Component({
	selector: 'hi-products-package-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateProductsPackageModal
{
	@Input('repository') repository: any;

	@Output('success') successEvent: EventEmitter<ProductsPackageEntity> = new EventEmitter<ProductsPackageEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private model: CreateProductsPackageRequest;

	constructor(private service: ProductsPackageRESTService,
		private palettes: PalettePresetsRepository) {
	}

	ngOnInit() {
		this.model = {
			title: [],
			description: [],
			palette: this.palettes.getRandom(),
			base_price: {
				currency: this.repository.currencies[0].code,
				amount: 0
			}
		};
	}

	checkRequiredFields(): boolean {
		return this.model.title.length > 0;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		console.log(this.model);

		this.service.createProductsPackage(this.model).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.products_package);
			},
			error => {
				loading.is = false;
			}
		);
	}
}