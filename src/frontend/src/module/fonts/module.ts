import {FontCRUDRoute} from "./routes/FontCRUDRoute/index";
import {FontRepository} from "./repository/FontRepository";
import {FontRepositoryResolve} from "./resolve/FontRepositoryResolve";
import {FontDataTable} from "./component/Elements/FontDataTable/index";
import {FontForm} from "./component/Forms/FontForm/index";
import {FontRESTService} from "../../../definitions/src/services/font/FontRESTService";
import {AttachmentRESTService} from "../../../definitions/src/services/attachment/AttachmentRESTService";
import {CreateFontModal} from "./component/Modals/CreateFontModal/index";
import {EditFontModal} from "./component/Modals/EditFontModal/index";
import {DeleteFontModal} from "./component/Modals/DeleteFontModal/index";

export const HIFontModule = {
	routes: [
		FontCRUDRoute,
	],
	declarations: [
		FontDataTable,
		FontForm,
		CreateFontModal,
		EditFontModal,
		DeleteFontModal,
	],
	providers: [
		FontRepository,
		FontRepositoryResolve
	],
};

export const HIFontRoutes = {
	path: 'fonts',
	component: FontCRUDRoute,
	pathMatch: 'full',
	resolve: [
		FontRepositoryResolve,
	]
};