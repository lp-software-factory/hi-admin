import {Component, EventEmitter, Output, Input} from "@angular/core";
import {UIGlobalLoadingIndicatorService} from "../../../../../ui/service/UIGlobalLoadingIndicatorService";
import {LocaleService} from "../../../../../locale/service/LocaleService";
import {InviteCardSizeEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {InviteCardSizeRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";


@Component({
	selector: 'hi-card-size-selector',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CardSizeSelector
{
	@Input('chosenCardSize') chosenCardSize: number;
	@Input('card_sizes') card_sizes: Array<InviteCardSizeEntity>;
	@Output('changeCardSize') changeCardSize: any = new EventEmitter<any>();

	constructor(private localeService: LocaleService,
		private loading: UIGlobalLoadingIndicatorService,
		private inviteCardSizeRESTService: InviteCardSizeRESTService) {
	}

	getCardSizeId(cardSize: InviteCardSizeEntity) {
		let id = cardSize.id;
		this.changeCardSize.emit(id);
	}

	changeValue(event) {
		this.changeCardSize.emit(event);
	}
}


export interface InviteCardSizeTableEntity extends InviteCardSizeEntity
{
	localizedTitle: string;
	localizedDescription: string;
}