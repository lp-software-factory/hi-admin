import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EnvelopeTextureRepository} from "../../repository/EnvelopeTextureRepository";
import {EnvelopeTextureEntity} from "../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {EnvelopeTextureRESTService} from "../../../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EnvelopeTextureRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EnvelopeTextureRESTService
		},
	]
})

export class EnvelopeTextureCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EnvelopeTextureEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EnvelopeTextureRepository) {
	}

	getEntities(): EnvelopeTextureEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}
