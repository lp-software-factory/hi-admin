import {CurrencyCRUDRoute} from "./routes/CurrencyCRUDRoute/index";
import {CreateCurrencyModal} from "./component/Modals/CreateCurrencyModal/index";
import {DeleteCurrencyModal} from "./component/Modals/DeleteCurrencyModal/index";
import {EditCurrencyModal} from "./component/Modals/EditCurrencyModal/index";
import {CurrencyRepositoryResolve} from "./resolve/CurrencyRepositoryResolve";
import {CurrencyRepository} from "./repository/CurrencyRepository";
import {CurrencyDataTable} from "./component/Elements/CurrencyDatatable/index";
import {CurrencyForm} from "./component/Forms/CurrencyForm/index";
import {CurrencyRESTService} from "../../../definitions/src/services/currency/CurrencyRESTService";

export const HICurrencyModule = {
	routes: [
		CurrencyCRUDRoute,
	],
	declarations: [
		CurrencyForm,
		CurrencyDataTable,
		CreateCurrencyModal,
		DeleteCurrencyModal,
		EditCurrencyModal
	],
	providers: [
		CurrencyRepository,
		CurrencyRepositoryResolve,
	],
};

export const HICurrencyRoutes = {
	path: 'currencies',
	component: CurrencyCRUDRoute,
	pathMatch: 'full',
	resolve: [
		CurrencyRepositoryResolve,
	]
};