import {Component, Input} from "@angular/core";
import {DesignerRequestEntity} from "../../../../../../definitions/src/definitions/designer-requests/entity/DesignerRequestEntity";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-designer-requests-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DesignerRequestsDatatable
{
	@Input('entities') entities: DesignerRequestEntity[] = [];

	constructor(private cursor: DTCursor<DesignerRequestEntity>) {
	}
}