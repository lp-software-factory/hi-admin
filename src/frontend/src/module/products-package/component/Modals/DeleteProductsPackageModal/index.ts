import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {ProductsPackageEntity} from "../../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {TranslationService} from "../../../../i18n/service/TranslationService";
import {ProductsPackageRESTService} from "../../../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Component({
	selector: 'hi-products-package-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})

export class DeleteProductsPackageModal implements AfterViewInit
{
	@Input('products-package') products_package: ProductsPackageEntity;
	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: ProductsPackageRESTService,
		private t: TranslationService) {
	}

	getProductsPackage(): string {
		return this.t.localize(this.products_package.title);
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let productPackageId = this.products_package.id;
		let loading = this.status.addLoading();

		this.service.deleteProductsPackage(productPackageId).subscribe(() => {
			this.successEvent.emit(productPackageId);
			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}