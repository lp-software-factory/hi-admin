import {AppProtectedRoute} from "./routes/AppProtectedRoute/index";
import {AppPublicRoute} from "./routes/AppPublicRoute/index";
import {DashboardRoute} from "./routes/DashboardRoute/index";
import {AppRoute} from "./routes/AppRoute/index";
import {UISidebar} from "./component/UISidebar/index";
import {CommonRoute} from "./routes/CommonRoute/index";
import {CatalogsRoute} from "./routes/CatalogsRoute/index";
import {UIGlobalLoadingIndicatorService} from "./service/UIGlobalLoadingIndicatorService";
import {UILoading} from "./component/UILoading/index";
import {InviteCardsRoute} from "./routes/InviteCardsRoute/index";
import {ServicesRoute} from "./routes/ServicesRoute/index";
import {SystemRoute} from "./routes/SystemRoute/index";
import {DesignersRoute} from "./routes/DesignersRoute/index";
import {IntegrationsRoute} from "./routes/IntegrationsRoute/index";

export const HIUIModule = {
	declarations: [
		UISidebar,
	],
	providers: [
		UIGlobalLoadingIndicatorService,
	],
	routes: [
		AppRoute,
		AppProtectedRoute,
		AppPublicRoute,
		CommonRoute,
		DashboardRoute,
		CatalogsRoute,
		InviteCardsRoute,
		UILoading,
		ServicesRoute,
		SystemRoute,
		IntegrationsRoute,
		DesignersRoute
	]
};