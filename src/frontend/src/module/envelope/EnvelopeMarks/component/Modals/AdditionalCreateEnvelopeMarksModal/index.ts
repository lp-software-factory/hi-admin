import {Component, EventEmitter, Output, ViewChild} from "@angular/core";

import {EnvelopeAdditionalMarksFormModel, EnvelopeAdditionalMarksForm} from "../../Forms/EnvelopeAdditionalMarksForm/index";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";

@Component({
	selector: 'hi-envelope-marks-additional-crud-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalCreateEnvelopeMarksModal
{
	@ViewChild('definitionForm') definitionForm: EnvelopeAdditionalMarksForm;

	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalMarksFormModel> = new EventEmitter<EnvelopeAdditionalMarksFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();

	private formModel: EnvelopeAdditionalMarksFormModel = new EnvelopeAdditionalMarksFormModel();

	cancel() {
		this.cancelEvent.emit()
	}
	
	submitButtonActive(): boolean {
		if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.attachment !== undefined
		}
	}

	submit() {
		this.successEvent.emit(this.formModel);
	}
}