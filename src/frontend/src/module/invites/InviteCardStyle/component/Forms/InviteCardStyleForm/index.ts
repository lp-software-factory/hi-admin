import {Component, EventEmitter, Output, Input} from "@angular/core";
import {CreateInviteCardStyleRequest} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/paths/create";

export enum InviteCardStyleFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class InviteCardStyleFormModel implements CreateInviteCardStyleRequest
{
	title = [];
	description = []
}

@Component({
	selector: 'hi-invite-card-style-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class InviteCardStyleForm
{
	@Output() modelChange: EventEmitter<InviteCardStyleFormModel> = new EventEmitter<InviteCardStyleFormModel>();

	@Input('mode') mode: InviteCardStyleFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: InviteCardStyleFormModel;
}