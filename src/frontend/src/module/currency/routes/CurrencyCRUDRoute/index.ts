import {Component} from "@angular/core";
import {CurrencyEntity} from "../../../../../definitions/src/definitions/money/entity/CurrencyEntity";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {CurrencyRepository} from "../../repository/CurrencyRepository";
import {CurrencyRESTService} from "../../../../../definitions/src/services/currency/CurrencyRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: CurrencyRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: CurrencyRESTService
		},
	],
})
export class CurrencyCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<CurrencyEntity>,
		private m: CRUDGenericModalsHelper,
		private r: CurrencyRepository) {
	}

	getRows(): CurrencyEntity[] {
		return this.r.getAll();
	}
}