import {Component, EventEmitter, Output, OnInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {OAuth2ProviderEntity} from "../../../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";
import {OAuth2ProviderRepository} from "../../../repository/OAuth2ProvidersRepository";
import {OAuth2ProviderHandlersRepository} from "../../../repository/OAuth2ProviderHandlersRepository";
import {OAuth2ProviderFormModel} from "../../Forms/OAuth2ProviderForm/index";
import {OAuth2ProvidersRESTService} from "../../../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Component({
	selector: 'hi-oauth2-provider-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateOAuth2ProvidersModal implements OnInit
{
	@Output('success') successEvent: EventEmitter<OAuth2ProviderEntity> = new EventEmitter<OAuth2ProviderEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: OAuth2ProviderFormModel = {
		code: '',
		handler: '',
		config: {
			clientId: '',
			clientSecret: '',
			redirectUri: ''
		}
	};

	constructor(private service: OAuth2ProvidersRESTService,
		private providers: OAuth2ProviderRepository,
		private handlers: OAuth2ProviderHandlersRepository,) {
	}

	ngOnInit(): void {
		if(this.hasAvailableHandlers()) {
			this.formModel.handler = this.getAvailableHandlers()[0];
		}
	}

	hasAvailableHandlers(): boolean {
		return this.getAvailableHandlers().length > 0;
	}

	getAvailableHandlers(): string[] {
		let exists = this.providers.getAll().map(entity => entity.handler);

		return this.handlers.getAll().filter(handler => !~exists.indexOf(handler));
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createOAuth2Providers(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.provider);
			},
			error => {
				loading.is = false;
			}
		);
	}
}