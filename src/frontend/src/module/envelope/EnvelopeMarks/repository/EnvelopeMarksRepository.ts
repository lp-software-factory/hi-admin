import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EnvelopeMarks} from "../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {GetAllEnvelopeMarksResponse200} from "../../../../../definitions/src/definitions/envelope/marks/path/get-all";
import {EnvelopeMarksRESTService} from "../../../../../definitions/src/services/envelope/envelope-marks/EnvelopeMarksRESTService";

@Injectable()
export class EnvelopeMarksRepository implements CRUDSerialRepositoryInterface
{
	private marks: EnvelopeMarks[] = [];

	constructor(private rest: EnvelopeMarksRESTService) {
	}

	fetch(): Observable<GetAllEnvelopeMarksResponse200> {

		let observable = this.rest.getAllEnvelopeMarks();

		observable.subscribe(response => {
			this.marks = response.envelope_marks;
		});

		return observable;
	}

	getAll(): EnvelopeMarks[] {
		return this.marks;
	}

	getById(id: number): EnvelopeMarks {
		let result = this.marks.filter(entity => entity.entity.id === id);

		if(result.length === 0) {
			throw new Error(`Marks with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.marks.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.marks, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.marks, oldPosition, newPosition);
	}

	updatePosition(rows: Array<EnvelopeMarks>, oldPosition, newPosition) {
		let tempRow;

		for(let index = 0; index < rows.length; index++) {
			if(rows[index].entity.position === newPosition) {
				tempRow = rows[index];
				for(let ind = 0; ind < rows.length; ind++) {
					if(rows[ind].entity.position === oldPosition) {
						tempRow.entity.position = rows[ind].entity.position;
						rows[ind].entity.position = newPosition;
						break;
					}
				}
				break;
			}
		}
	}

	add(gamma: EnvelopeMarks) {
		this.marks.push(gamma);
	}

	updateAllPositions(position: number) {
		this.marks.forEach(marks => {
			if(marks.entity.position > position){
				marks.entity.position += -1;
			}
		});
	}

	remove(id: number) {
		this.marks.forEach(marks => {
			if(marks.entity.id === id) {
				this.updateAllPositions(marks.entity.position);
			}
		});

		this.marks = this.marks.filter(entity => entity.entity.id !== id);
	}

	replace(gamma: EnvelopeMarks) {
		this.marks = this.marks.map(entity => {
			if(entity.entity.id === gamma.entity.id) {
				return gamma;
			} else {
				return entity;
			}
		})
	}
}