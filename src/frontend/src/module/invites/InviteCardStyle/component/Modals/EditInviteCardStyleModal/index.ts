import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardStyleEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {InviteCardStyleFormModel} from "../../Forms/InviteCardStyleForm/index";
import {InviteCardStyleRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";


@Component({
	selector: 'hi-invite-card-style-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditInviteCardStyleModal
{
	@Input('invite_card_style') invite_card_style: InviteCardStyleEntity;
	@Output('success') successEvent: EventEmitter<InviteCardStyleEntity> = new EventEmitter<InviteCardStyleEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();


	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardStyleFormModel();

	constructor(private service: InviteCardStyleRESTService) {
	}


	ngOnInit() {
		this.initInviteCardStyle();
	}

	initInviteCardStyle() {
		this.formModel = JSON.parse(JSON.stringify(this.invite_card_style));
	}

	cancel() {
		this.initInviteCardStyle();
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();


		this.service.editInviteCardStyle(this.invite_card_style.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.style);
			},
			error => {
				loading.is = false;
			}
		);
	}
}