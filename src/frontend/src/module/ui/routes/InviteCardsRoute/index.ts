import {Component} from "@angular/core";

import {HIInviteCardStyleRoute} from "../../../invites/InviteCardStyle/module";
import {HIInviteCardSizeRoute} from "../../../invites/InviteCardSize/module";
import {HIInviteCardGammaRoute} from "../../../invites/InviteCardGamma/module";

@Component({
	template: '<router-outlet></router-outlet>'
})
export class InviteCardsRoute
{
}

export const HIInviteCardsRoutes = {
	path: 'invites',
	component: InviteCardsRoute,
	children: [
		HIInviteCardStyleRoute,
		HIInviteCardSizeRoute,
		HIInviteCardGammaRoute,
	]
};