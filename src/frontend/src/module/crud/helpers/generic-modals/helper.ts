import {Injectable} from "@angular/core";
import {ModalControl} from "../../../common/classes/ModalControl";
import {CRUDCurrentRecordHelper} from "../current/helper";

export class CRUDModals
{
	create: ModalControl = new ModalControl();
	delete: ModalControl = new ModalControl();
	edit: ModalControl = new ModalControl();
	set: ModalControl = new ModalControl();
	additionalCreate: ModalControl = new ModalControl();
	additionalDelete: ModalControl = new ModalControl();
	additionalEdit: ModalControl = new ModalControl();
	additionalSet: ModalControl = new ModalControl();
}

@Injectable()
export class CRUDGenericModalsHelper
{
	public modals: CRUDModals = new CRUDModals();

	constructor(private current: CRUDCurrentRecordHelper<any>) {}

	create() {
		this.modals.create.open();
	}

	edit() {
		if(this.current.hasActiveEntity()) {
			this.modals.edit.open();
		}
	}

	delete() {
		if(this.current.hasActiveEntity()) {
			this.modals.delete.open();
		}
	}

	set() {
		if(this.current.hasActiveEntity()) {
			this.modals.set.open();
		}
	}

	closeAllModals() {
		this.modals.create.close();
		this.modals.delete.close();
		this.modals.edit.close();
		this.modals.set.close();
		this.modals.additionalCreate.close();
		this.modals.additionalDelete.close();
		this.modals.additionalEdit.close();
		this.modals.additionalSet.close();
	}
}