import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CompanyEntity} from "../../../../../../definitions/src/definitions/company/entity/CompanyEntity";
import {CompanyRESTService} from "../../../../../../definitions/src/services/company/CompanyRESTService";

@Component({
	selector: 'hi-company-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class DeleteCompanyModal implements AfterViewInit
{
	@Input('entity') company: CompanyEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: CompanyRESTService) {
	}

	getCompanyName(): string {
		return this.company.name;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let companyId = this.company.id;
		let loading = this.status.addLoading();

		this.service.deleteCompany(companyId).subscribe(() => {
			this.successEvent.emit(companyId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}