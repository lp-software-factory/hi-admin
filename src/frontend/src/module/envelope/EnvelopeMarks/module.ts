import {EnvelopeMarksDatatable} from "./component/Elements/EnvelopeMarksDatatable/index";
import {CreateEnvelopeMarksModal} from "./component/Modals/CreateEnvelopeMarksModal/index";
import {EditEnvelopeMarksModal} from "./component/Modals/EditEnvelopeMarksModal/index";
import {DeleteEnvelopeMarksModal} from "./component/Modals/DeleteEnvelopeMarksModal/index";
import {EnvelopeMarksRepository} from "./repository/EnvelopeMarksRepository";
import {EnvelopeMarksRepositoryResolve} from "./resolve/EnvelopeMarksRepositoryResolve";
import {EnvelopeMarksCRUDRoute} from "./routes/EnvelopeMarksCRUDRoute/index";
import {EnvelopeMarkForm} from "./component/Forms/EnvelopeMarksForm/index";
import {EnvelopeMarksRESTService} from "../../../../definitions/src/services/envelope/envelope-marks/EnvelopeMarksRESTService";
import {EnvelopeAdditionalMarksCrudDataTable} from "./component/Elements/EnvelopeAdditionalMarksDatatable/index";
import {EnvelopeAdditionalMarksForm} from "./component/Forms/EnvelopeAdditionalMarksForm/index";
import {AdditionalCreateEnvelopeMarksModal} from "./component/Modals/AdditionalCreateEnvelopeMarksModal/index";
import {AdditionalEditEnvelopeMarksModal} from "./component/Modals/AdditionalEditEnvelopeMarksModal/index";
import {AdditionalDeleteEnvelopeMarksModal} from "./component/Modals/AdditionalDeleteEnvelopeMarksModal/index";

export const HIEnvelopeMarksModule = {
	routes: [
		EnvelopeMarksCRUDRoute,
	],
	declarations: [
		AdditionalCreateEnvelopeMarksModal,
		AdditionalEditEnvelopeMarksModal,
		AdditionalDeleteEnvelopeMarksModal,
		EnvelopeMarkForm,
		EnvelopeAdditionalMarksForm,
		EnvelopeMarksDatatable,
		EnvelopeAdditionalMarksCrudDataTable,
		CreateEnvelopeMarksModal,
		EditEnvelopeMarksModal,
		DeleteEnvelopeMarksModal
	],
	providers: [
		EnvelopeMarksRepository,
		EnvelopeMarksRepositoryResolve
	],
};

export const HIEnvelopeMarksRoute = {
	path: 'marks',
	component: EnvelopeMarksCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EnvelopeMarksRepositoryResolve
	]
};