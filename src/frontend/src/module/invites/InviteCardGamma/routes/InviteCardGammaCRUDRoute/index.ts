import {Component} from "@angular/core";
import {InviteCardGamma, InviteCardGammaEntity} from "../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {InviteCardGammaRepository} from "../../repository/InviteCardGammaRepository";
import {InviteCardGammaRESTService} from "../../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: InviteCardGammaRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: InviteCardGammaRESTService
		},
	]
})

export class InviteCardGammaCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<InviteCardGamma>,
		private m: CRUDGenericModalsHelper,
		private r: InviteCardGammaRepository) {
	}

	getEntities(): InviteCardGammaEntity[] {
		return this.r.getAll().map(entity => {
			return entity.entity
		}).sort((a, b) => {
			return a.position - b.position;
		});
	}
}

export interface InviteCardGammaTableEntity extends InviteCardGamma
{
	localizedTitle: string;
	localizedDescription: string;
}
