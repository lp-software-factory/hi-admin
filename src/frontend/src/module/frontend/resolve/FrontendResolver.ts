import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Frontend} from "../../../../definitions/src/definitions/frontend/entity/FrontendResponse";
import {FrontendService} from "../service/FrontendService";
import {MessageService, MESSAGE_LEVEL} from "../../message/service/MessageService";

@Injectable()
export class FrontendResolver implements Resolve<Frontend>
{
	constructor(private service: FrontendService,
		private messages: MessageService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let observable = this.service.fetch();

		observable.subscribe(
			response => FrontendResolver.removeLoader(),
			error => {
				this.messages.push(MESSAGE_LEVEL.Danger, 'Failed to download frontend configuration.');
				FrontendResolver.removeLoader();
			}
		);

		return observable;
	}

	private static removeLoader() {
		let element = document.getElementById('HIAppFrontendLoadingStatus');

		if(element) {
			element.remove();
		}
	}
}