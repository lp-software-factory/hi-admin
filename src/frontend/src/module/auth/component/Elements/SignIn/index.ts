import {Component, Output, EventEmitter, ViewChild, ElementRef} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {AuthService} from "../../../service/AuthService";
import {SignInResponse200, SignInRequest} from "../../../../../../definitions/src/definitions/auth/paths/sign-in";
import {MessageService, MESSAGE_LEVEL} from "../../../../message/service/MessageService";
import {AuthRESTService} from "../../../../../../definitions/src/services/auth/AuthRESTService";
import {withStatusCode} from "../../../../common/service/RESTService";

@Component({
	selector: 'hi-auth-sign-in',
	template: require('./template.jade')
})
export class SignIn
{
	@ViewChild('emailInput') emailInput: ElementRef;
	@ViewChild('passwordInput') passwordInput: ElementRef;

	@Output('success') successEvent = new EventEmitter();
	@Output('close') closeEvent = new EventEmitter<SignInModel>();

	private status: LoadingManager = new LoadingManager();

	private model: SignInModel = {
		email: '',
		password: ''
	};

	constructor(private RESTService: AuthRESTService,
		private service: AuthService,
		private messages: MessageService) {
	}

	ngAfterViewInit() {
		this.emailInput.nativeElement.focus();
	}

	submit() {
		let loading = this.status.addLoading();

		this.RESTService.signIn(this.model).subscribe(
			(response: SignInResponse200) => {
				this.service.signIn(response.token);

				if(this.service.isSignedIn()) {
					if(this.service.isAdmin()) {
						this.successEvent.emit(response);
					} else {
						this.model.email = '';
						this.model.password = '';
						this.emailInput.nativeElement.focus();

						this.messages.push(MESSAGE_LEVEL.Warning, "You're not admin!");
						this.service.signOut();
					}
				}

				loading.is = false;
			},
			error => {
				loading.is = false;

				this.model.password = '';
				this.passwordInput.nativeElement.focus();

				withStatusCode(404, error, () => {
					this.emailInput.nativeElement.focus();
				});
			}
		);
	}

	close() {
		this.closeEvent.emit(this.model);
	}
}

interface SignInModel extends SignInRequest
{
}