import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopePatternEntity} from "../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopePatternRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";

@Component({
	selector: 'hi-envelope-pattern-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteEnvelopePatternModal implements AfterViewInit
{
	@Input('envelope_pattern') envelope_pattern: EnvelopePatternEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EnvelopePatternRESTService) {
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	getTitle(): string {
		return this.envelope_pattern.title[0].value;
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let id = this.envelope_pattern.id;
		let loading = this.status.addLoading();

		this.service.deleteEnvelopePattern(id).subscribe(() => {
			this.successEvent.emit(id);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}