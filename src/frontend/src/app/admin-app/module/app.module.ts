import {NgModule, CUSTOM_ELEMENTS_SCHEMA, SecurityContext, Injectable} from "@angular/core";
import {APP_BASE_HREF} from "@angular/common";
import {BrowserModule, DomSanitizer} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {App} from "../component/App/app.component";
import {HI_MODULES} from "../../../module/modules.ts";
import {appRoutingProviders, routing} from "../routes/app.routing";
import {Angular2DataTableModule} from "angular2-data-table";
import {ColorPickerModule} from "../../../module/common/component/ColorPicker/module/index.ts";
import {RESTService} from "../../../module/common/service/RESTService";
import {LIST_SERVICES} from "../../../../definitions/src/services/list";

@Injectable()
export class NoSanitizationService
{
	sanitize(ctx: SecurityContext, value: any): string {
		return value;
	}
}

let providersArray: any = [
	appRoutingProviders,
	{
		provide: APP_BASE_HREF,
		useValue: '/',
	},
	{
		provide: DomSanitizer,
		useClass: NoSanitizationService
	}];

LIST_SERVICES.forEach(service => {
	providersArray.push({
		provide: service,
		useFactory: (adapter: RESTService) => {return new service(adapter)},
		deps: [RESTService]
	})
});

let moduleDeclaration = {
	declarations: [
		App,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	imports: [
		BrowserModule,
		routing,
		FormsModule,
		ReactiveFormsModule,
		HttpModule,
		Angular2DataTableModule,
		ColorPickerModule
	],
	providers: [
		RESTService,
		providersArray
		],
	bootstrap: [App]
};

for(let hiModule of HI_MODULES) {
	if(hiModule['declarations']) moduleDeclaration.declarations.push(<any>hiModule['declarations']);
	if(hiModule['routes']) moduleDeclaration.declarations.push(<any>hiModule['routes']);
	if(hiModule['providers']) moduleDeclaration.providers.push(<any>hiModule['providers']);
	if(hiModule['imports']) moduleDeclaration.imports.push(<any>hiModule['imports']);
}

@NgModule(moduleDeclaration)
export class AppModule
{
}