import {Component} from "@angular/core";

@Component({
	selector: 'nothing',
	template: '<div></div>'
})
export class Nothing
{
}