import {Component, EventEmitter, Output, Input} from "@angular/core";
import {EventTypeEntity} from "../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";

@Component({
	selector: 'hi-event-type-selector',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EventTypeSelector
{
	@Input('chosenEventType') chosenEventType: number;
	@Input('event_types') event_types: Array<EventTypeTableEntity>;

	@Output('changeEventType') changeEventType: any = new EventEmitter<any>();

	getEventTypeId(event_type: EventTypeEntity) {
		let id = event_type.id;
		this.changeEventType.emit(id);
	}

	changeValue(event) {
		this.changeEventType.emit(event);
	}
}

export interface EventTypeTableEntity extends EventTypeEntity
{
	localizedTitle: string;
	localizedDescription: string;
}
