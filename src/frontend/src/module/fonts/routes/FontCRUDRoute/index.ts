import {Component} from "@angular/core";

import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {FontRepository} from "../../repository/FontRepository";
import {Font} from "../../../../../definitions/src/definitions/font/entity/Font";
import {FontRESTService} from "../../../../../definitions/src/services/font/FontRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: FontRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: FontRESTService
		},
	],
})
export class FontCRUDRoute
{
	constructor(
		private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<Font>,
		private m: CRUDGenericModalsHelper,
		private r: FontRepository,
		private service: FontRESTService
	) {
	}

	getRows(): Font[] {
		return this.r.getAll();
	}
}