import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptionsArgs, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {RESTServiceInterface, RESTServiceInterfaceOptions, RESTServiceInterfaceHeader} from "../../../../definitions/src/essentials/RESTServiceInterface";
import {AuthService} from "../../auth/service/AuthService";
import {MessageService, MESSAGE_LEVEL} from "../../message/service/MessageService";


@Injectable()
export class RESTService implements RESTServiceInterface
{
	constructor(
		public http: Http,
		public auth: AuthService,
		private messages: MessageService) {
	}

	public get(url: string, options: RESTServiceInterfaceOptions = {}) {
		if(this.auth.hasJWT()) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.get(url, RESTService.prepareRequestOptions(options)));
	}

	public put(url: string, json: any = '{}', options: RESTServiceInterfaceOptions = {}) {
		if(this.auth.hasJWT()) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.put(url, JSON.stringify(json), RESTService.prepareRequestOptions(options)));
	}

	public post(url: string, json: any = '{}', options: RESTServiceInterfaceOptions = {}) {
		if(this.auth.hasJWT()) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.post(url, JSON.stringify(json), RESTService.prepareRequestOptions(options)));
	}

	public delete(url: string, options: RESTServiceInterfaceOptions = {}) {
		if(this.auth.hasJWT()) {
			options.headers = this.getAuthHeaders();
		}

		return this.handle(this.http.delete(url, RESTService.prepareRequestOptions(options)));
	}

	public parseError(error): string {
		if(typeof error === "object") {
			if(error.error && typeof error.error === 'string') {
				return error.error;
			} else {
				return 'Unknown error';
			}
		} else {
			return 'Failed to parse JSON';
		}
	}

	public getAuthHeaders(): RESTServiceInterfaceHeader {
		let authHeader = {};

		if(this.auth.hasJWT()) {
			authHeader['Authorization'] = this.auth.getJWT();
		}

		return authHeader;
	}

	private static prepareRequestOptions(options: RESTServiceInterfaceOptions): RequestOptionsArgs {
		let result = {};
		result['headers'] = options.headers;
		result['search'] = options.search;
		return result;
	}

	public handle(request) {
		let fork = request.publish().refCount();

		fork
			.catch(error => {
				return Observable.onErrorResumeNext(Observable.throw(this.handleError(error)));
			})
			.subscribe(() => {
			});

		return fork.map(res => res.json());
	}

	private handleError(error) {
		let response = {
			success: false,
			status: 500,
			error: 'Unknown error'
		};

		if(typeof error === "string") {
			this.genericError(error);
		} else if(typeof error === null) {
			this.unknownError();
		} else if(typeof error === "object") {
			if(error instanceof Response) {
				try {
					let parsed = error.json();

					this.httpError(parsed.error);

					response.error = parsed.error;
					response.status = error.status;
				} catch(error) {
					this.jsonParseError();
				}
			} else {
				this.unknownError();
			}
		} else {
			this.unknownError();
		}

		return response;
	}

	private unknownError() {
		this.messages.push(MESSAGE_LEVEL.Danger, ('common.http.unknown-error'))
	}

	private genericError(error: string) {
		this.messages.push(MESSAGE_LEVEL.Warning, error);
	}

	private jsonParseError() {
		this.messages.push(MESSAGE_LEVEL.Danger, ('common.http.parse-error'));
	}

	private httpError(error: string) {
		this.messages.push(MESSAGE_LEVEL.Danger, error);
	}
}

export function withStatusCode(code: number, error, callback: Function) {
	if(error instanceof Response) {
		if(error.status === code) {
			callback();
		}
	}
}