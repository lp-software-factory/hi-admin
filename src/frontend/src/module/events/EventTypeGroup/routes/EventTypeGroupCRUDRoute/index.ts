import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {EventTypeGroupEntity} from "../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {EventTypeGroupRepository} from "../../repository/EventTypeGroupRepository";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {EventTypeGroupRESTService} from "../../../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EventTypeGroupRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EventTypeGroupRESTService
		},
	]
})
export class EventTypeGroupCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EventTypeGroupEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EventTypeGroupRepository) {
	}

	getRows(): EventTypeGroupEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}