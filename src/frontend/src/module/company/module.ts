import {CompanyCRUDRoute} from "./routes/CompanyCRUDRoute/index";
import {CompanyForm} from "./component/Forms/CompanyForm/index";
import {CompanyDataTable} from "./component/Elements/CompanyDataTable/index";
import {CreateCompanyModal} from "./component/Modals/CreateCompanyModal/index";
import {EditCompanyModal} from "./component/Modals/EditCompanyModal/index";
import {DeleteCompanyModal} from "./component/Modals/DeleteCompanyModal/index";
import {CompanyRepositoryResolve} from "./resolve/CompanyRepositoryResolve";
import {CompanyRepository} from "./repository/CompanyRepository";
import {CompanyRESTService} from "../../../definitions/src/services/company/CompanyRESTService";

export const HICompanyModule = {
	routes: [
		CompanyCRUDRoute,
	],
	declarations: [
		CompanyDataTable,
		CompanyForm,
		CreateCompanyModal,
		EditCompanyModal,
		DeleteCompanyModal,
	],
	providers: [
		CompanyRepository,
		CompanyRepositoryResolve,
	],
};

export const HICompanyRoutes = {
	path: 'companies',
	component: CompanyCRUDRoute,
	pathMatch: 'full',
	resolve: [
		CompanyRepositoryResolve,
	]
};