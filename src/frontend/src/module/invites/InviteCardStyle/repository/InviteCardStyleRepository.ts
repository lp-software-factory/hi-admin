import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {InviteCardStyleEntity} from "../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {GetAllInviteCardStylesResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-style/paths/get-all";
import {InviteCardStyleRESTService} from "../../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";

@Injectable()
export class InviteCardStyleRepository implements CRUDSerialRepositoryInterface
{
	private styles: InviteCardStyleEntity[] = [];

	constructor(private rest: InviteCardStyleRESTService) {
	}

	fetch(): Observable<GetAllInviteCardStylesResponse200> {

		let observable = this.rest.getAllInviteCardStyles()

		observable.subscribe(response => {
			this.styles = response.styles;
		});

		return observable;
	}

	getAll(): InviteCardStyleEntity[] {
		return this.styles;
	}

	getById(id: number): InviteCardStyleEntity {
		let result = this.styles.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Style with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.styles.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.styles, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.styles, oldPosition, newPosition);
	}

	add(style: InviteCardStyleEntity) {
		this.styles.push(style);
	}

	remove(id: number) {
		this.styles = this.styles.filter(entity => entity.id !== id);
	}

	replace(style: InviteCardStyleEntity) {
		this.styles = this.styles.map(entity => {
			if(entity.id === style.id) {
				return style;
			} else {
				return entity;
			}
		})
	}
}