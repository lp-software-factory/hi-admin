import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeMarksEntity, EnvelopeMarks} from "../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopeMarksFormModel} from "../../Forms/EnvelopeMarksForm/index";
import {Observable} from "rxjs/Observable";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeMarksRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-marks/EnvelopeMarksRESTService";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeMarkDefinitionEntity} from "../../../../../../../definitions/src/definitions/envelope/mark-definition/entity/EnvelopeMarkDefinition";
import {EnvelopeAdditionalMarksFormModel} from "../../Forms/EnvelopeAdditionalMarksForm/index";
import {EnvelopeMarkDefinitionRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-mark-definition/EnvelopeMarkDefinitionRESTService";

@Component({
	selector: 'hi-envelope-marks-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor
	]
})
export class CreateEnvelopeMarksModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeMarks> = new EventEmitter<EnvelopeMarks>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	
	private formModel = new EnvelopeMarksFormModel();

	constructor(private service: EnvelopeMarksRESTService,
				private definitionService: EnvelopeMarkDefinitionRESTService,
				private attachment: AttachmentRESTService,
				private l: LoadingManager,
				private m: CRUDGenericModalsHelper,
				private cursor: DTCursor<EnvelopeMarkDefinitionEntity>,
	) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 &&
				this.formModel.definitions.length > 0 && ! this.l.isLoading();
	}

	addDefinition(entity: EnvelopeAdditionalMarksFormModel) {
		this.formModel.definitions.push(entity);
	}

	replaceDefinition(entity: EnvelopeAdditionalMarksFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			return compare;
		})
	}

	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(texture => JSON.stringify(texture) !== JSON.stringify(this.cursor.current()));
		this.cursor._current = undefined;
	}

	submit() {
		let loading = this.l.addLoading();
		let model: EnvelopeMarksFormModel = new EnvelopeMarksFormModel();
		model.title = JSON.parse(JSON.stringify(this.formModel.title));
		model.description = JSON.parse(JSON.stringify(this.formModel.description));
		this.service.createEnvelopeMarks(model).subscribe(
			response => {
				let observables = [];
				for (let definition of this.formModel.definitions) {
					if (definition.uploaded.attachment) {
						definition.attachment_id = definition.uploaded.attachment.id;
					}
					definition.owner_envelope_marks_id = response.envelope_marks.entity.id;
					let observable = this.definitionService.createEnvelopeMarkDefinition(definition);
					observable.subscribe(definitionResponse => {
						response.envelope_marks.entity.definitions.push(definitionResponse.envelope_mark_definition)
					});
					observables.push(observable)
				}
				Observable.forkJoin(observables).subscribe(responseAll => {
					loading.is = false;
					this.successEvent.emit(response.envelope_marks);
				});
			},
			error => {
				loading.is = false;
			}
		);
	}
}