import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {OAuth2ProviderRepository} from "../repository/OAuth2ProvidersRepository";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {GetAllOAuth2ProvidersResponse200} from "../../../../../definitions/src/definitions/oauth2/providers/paths/get-all-providers";

@Injectable()
export class OAuth2ProvidersRepositoryResolve implements Resolve<GetAllOAuth2ProvidersResponse200>
{
	constructor(private repository: OAuth2ProviderRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}