import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeTextureEntity} from "../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";

@Component({
	selector: 'hi-envelope-texture-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeTextureDatatable
{
	@Input('entities') entities: EnvelopeTextureEntity[] = [];

	constructor(private cursor: DTCursor<EnvelopeTextureEntity>) {
	}
}