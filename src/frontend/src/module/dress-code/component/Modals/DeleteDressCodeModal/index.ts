import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {DressCodeRESTService} from "../../../../../../definitions/src/services/dress-code/DressCodeRESTService";
import {DressCodeEntity} from "../../../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {TranslationService} from "../../../../i18n/service/TranslationService";

@Component({
	selector: 'hi-dress-code-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class DeleteDressCodeModal implements AfterViewInit
{
	@Input('entity') dress_code: DressCodeEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: DressCodeRESTService,
		private t: TranslationService) {
	}
	
	getDressCodeTitle(): string {
		return this.t.localize(this.dress_code.title);
	}
	
	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let dress_codeId = this.dress_code.id;
		let loading = this.status.addLoading();

		this.service.delete(dress_codeId).subscribe(() => {
			this.successEvent.emit(dress_codeId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}