import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {ProductRepository} from "../repository/ProductRepository";
import {ListProductsResponse200} from "../../../../definitions/src/definitions/products/paths/list";

@Injectable()
export class ProductRepositoryResolve implements Resolve<ListProductsResponse200>
{
	constructor(private repository: ProductRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}