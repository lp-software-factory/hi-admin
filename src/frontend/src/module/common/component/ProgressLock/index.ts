import {Component} from "@angular/core";

@Component({
	selector: 'hi-form-progress-lock',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class ProgressLock
{
}