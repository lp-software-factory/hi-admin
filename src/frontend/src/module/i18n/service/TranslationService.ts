import {Injectable} from "@angular/core";

import {LocaleService} from "../../locale/service/LocaleService";
import {LocalizedString} from "../../../../definitions/src/definitions/locale/definitions/LocalizedString";

var isTranslationServiceDefined = false;

@Injectable()
export class TranslationService
{
	constructor(private localeService: LocaleService) {
		TranslationStaticService.service = this;
		isTranslationServiceDefined = true;
	}

	private target = this.localeService.current;
	private fallback = 'en';
	private dictionaries = {
		ru: require('./../../../translations/ru.json'),
		en: require('./../../../translations/en.json'),
	};

	translate(key: string): string {
		if(this.dictionaries[this.target.language][key] !== undefined) {
			return this.dictionaries[this.target.language][key];
		} else if(this.dictionaries[this.fallback][key] !== undefined) {
			console.log(`TranslationService: Using fallback for ${key}`);

			return this.dictionaries[this.fallback][key];
		} else {
			console.log(`Notice (TranslationService): Cannot translate key "${key}"!`);

			return key;
		}
	}

	localize(input: LocalizedString[]): string {
		return this.localeService.getLocalization(input).value;
	}
}

export class TranslationStaticService
{
	public static service: TranslationService;
}

export function translate(key) {
	return isTranslationServiceDefined
		? TranslationStaticService.service.translate(key)
		: key
		;
}