import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EnvelopeBackdropEntity} from "../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {EnvelopeBackdropGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/backdrop/path/get-all";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {EnvelopeBackdropRESTService} from "../../../../../definitions/src/services/envelope/envelope-backdrop/EnvelopeBackdropRESTService";

@Injectable()
export class EnvelopeBackdropRepository implements CRUDSerialRepositoryInterface
{
	private backdrops: EnvelopeBackdropEntity[] = [];

	constructor(private rest: EnvelopeBackdropRESTService) {
	}

	fetch(): Observable<EnvelopeBackdropGetAllResponse200> {

		let observable = this.rest.getAllEnvelopeBackdrop();

		observable.subscribe(response => {
			this.backdrops = response.envelope_backdrops.map(entity => {
				return entity.entity
			});
		});

		return observable;
	}

	getAll(): EnvelopeBackdropEntity[] {
		return this.backdrops;
	}

	getById(id: number): EnvelopeBackdropEntity {
		let result = this.backdrops.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Marks with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.backdrops.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.backdrops, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.backdrops, oldPosition, newPosition);
	}

	add(element: EnvelopeBackdropEntity) {
		this.backdrops.push(element);
	}

	remove(id: number) {
		this.backdrops = this.backdrops.filter(entity => entity.id !== id);
	}

	replace(element: EnvelopeBackdropEntity) {
		this.backdrops = this.backdrops.map(entity => {
			if(entity.id === element.id) {
				return element;
			} else {
				return entity;
			}
		})
	}
}