import {Component} from "@angular/core";
import {ProductEntity} from "../../../../../definitions/src/definitions/products/entity/ProductEntity";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDPositionEntityHelper} from "../../../crud/helpers/position/helper";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {ProductRepository} from "../../repository/ProductRepository";
import {CurrencyRepository} from "../../../currency/repository/CurrencyRepository";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {CRUDRESTService, CRUDRepository} from "../../../crud/provide/provides";
import {ProductRESTService} from "../../../../../definitions/src/services/product/ProductRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: ProductRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: ProductRESTService
		},
	]
})
export class ProductCRUDRoute
{
	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<ProductEntity>,
		private m: CRUDGenericModalsHelper,
		private r: ProductRepository,
		private rCurrencies: CurrencyRepository,) {
	}

	getAdditionalRepository() {
		return {
			currencies: this.rCurrencies.getAll()
		};
	}

	getRows(): ProductEntity[] {
		return this.r.getAll();
	}
}