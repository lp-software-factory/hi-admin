import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CompanyEntity} from "../../../../definitions/src/definitions/company/entity/CompanyEntity";
import {ListCompanyResponse200} from "../../../../definitions/src/definitions/company/paths/list";
import {CompanyRESTService} from "../../../../definitions/src/services/company/CompanyRESTService";

@Injectable()
export class CompanyRepository
{
	public static DEFAULT_OFFSET: number = 0;
	public static DEFAULT_LIMIT: number = 100;

	private entity: CompanyEntity[] = [];

	constructor(private rest: CompanyRESTService) {
	}

	fetch(offset: number = CompanyRepository.DEFAULT_OFFSET, limit: number = CompanyRepository.DEFAULT_LIMIT): Observable<ListCompanyResponse200> {
		let observable = this.rest.getCompaniesList(offset, limit);

		observable.subscribe(
			response => {
				this.entity = response.companies;
			}
		);

		return observable;
	}

	getAll(): CompanyEntity[] {
		return this.entity;
	}

	getById(id: number): CompanyEntity {
		let result = this.entity.filter(company => company.id === id);

		if(result.length === 0) {
			throw new Error(`Company with ID "${id}" not found`)
		}

		return result[0];
	}

	addCompany(entity: CompanyEntity) {
		this.entity.push(entity);
	}

	removeCompany(id: number) {
		this.entity = this.entity.filter(company => company.id !== id);
	}

	replaceCompany(entity: CompanyEntity) {
		this.entity = this.entity.map(company => {
			return company.id === entity.id ? entity : company;
		})
	}
}