import {Component, EventEmitter, Output, Input} from "@angular/core";

import {Validators} from "../../../../../common/functions/validators";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AttachmentEntity} from "../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

export enum EnvelopeAdditionalMarksFormMode
{
	Create = <any>"create",
	Edit = <any>"edit"
}

export class EnvelopeAdditionalMarksFormModel
{
	id: number = undefined;
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	attachment_id: number = undefined;
	owner_envelope_marks_id: number = undefined;
	uploaded: {
		attachment: AttachmentEntity<ImageAttachmentMetadata>
	} = {
		attachment: undefined
	};
}

@Component({
	selector: 'hi-envelope-additional-marks-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopeAdditionalMarksForm
{
	@Output() modelChange: EventEmitter<EnvelopeAdditionalMarksFormModel> = new EventEmitter<EnvelopeAdditionalMarksFormModel>();

	@Input('mode') mode: EnvelopeAdditionalMarksFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EnvelopeAdditionalMarksFormModel = new EnvelopeAdditionalMarksFormModel();

	public attachment: AttachmentEntity<ImageAttachmentMetadata>;
	
	attachAttachment($event) {
		this.model.uploaded.attachment = $event;
	}
	
	deleteFile(type: string) {
		if(type === 'attachment'){
			this.model.uploaded.attachment = undefined;
		}
	}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.withId(this.model.uploaded.attachment)},
			() => { return Validators.localeInputNotEmpty(this.model.title) },
			() => { return Validators.localeInputNotEmpty(this.model.description) }
		];

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}
}