import {Component} from "@angular/core";

import {HILocaleRoutes} from "../../../locale/module";
import {HICurrencyRoutes} from "../../../currency/module";
import {HICompanyRoutes} from "../../../company/module";
import {HIEventTypeGroupRoutes} from "../../../events/EventTypeGroup/module";
import {HIEventTypeRoutes} from "../../../events/EventType/module";
import {HIFontRoutes} from "../../../fonts/module";

@Component({
	template: '<router-outlet></router-outlet>'
})
export class CatalogsRoute
{
}

export const HICatalogRoutes = {
	path: 'catalogs',
	component: CatalogsRoute,
	children: [
		HILocaleRoutes,
		HICurrencyRoutes,
		HICompanyRoutes,
		HIEventTypeGroupRoutes,
		HIEventTypeRoutes,
		HIFontRoutes
	]
};