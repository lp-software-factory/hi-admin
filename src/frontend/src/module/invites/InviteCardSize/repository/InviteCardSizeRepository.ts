import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {InviteCardSizeEntity} from "../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {GetAllInviteCardSizesResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-sizes/paths/get-all";
import {InviteCardSizeRESTService} from "../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";

@Injectable()
export class InviteCardSizeRepository implements CRUDSerialRepositoryInterface
{
	private sizes: InviteCardSizeEntity[] = [];

	constructor(private rest: InviteCardSizeRESTService) {
	}

	fetch(): Observable<GetAllInviteCardSizesResponse200> {

		let observable = this.rest.getAllInviteCardSizes()

		observable.subscribe(response => {
			this.sizes = response.card_sizes;
		});

		return observable;
	}

	getAll(): InviteCardSizeEntity[] {
		return this.sizes;
	}

	getById(id: number): InviteCardSizeEntity {
		let result = this.sizes.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Size with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.sizes.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.sizes, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.sizes, oldPosition, newPosition);
	}

	add(size: InviteCardSizeEntity) {
		this.sizes.push(size);
	}

	remove(id: number) {
		this.sizes = this.sizes.filter(entity => entity.id !== id);
	}

	replace(size: InviteCardSizeEntity) {
		this.sizes = this.sizes.map(entity => {
			if(entity.id === size.id) {
				return size;
			} else {
				return entity;
			}
		})
	}
}