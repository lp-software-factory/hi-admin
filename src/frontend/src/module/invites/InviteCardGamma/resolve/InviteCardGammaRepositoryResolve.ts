import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {InviteCardGammaRepository} from "../repository/InviteCardGammaRepository";
import {GetAllInviteCardGammasResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-gamma/paths/get-all";

@Injectable()
export class InviteCardGammaRepositoryResolve implements Resolve<GetAllInviteCardGammasResponse200>
{
	constructor(private repository: InviteCardGammaRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}