import {ModalComponent} from "./component/Modal/index";
import {ModalBoxComponent} from "./component/Modal/box/index";
import {ProgressLock} from "./component/ProgressLock/index";
import {LoadingLinearIndicator} from "./component/LoadingLinearIndicator/index";
import {HelpModal} from "./component/HelpModal/index";
import {TextParser} from "./component/TextParser/index";
import {ImageCropper} from "./component/ImageCropper/index";
import {Nothing} from "./component/Nothing/index";
import {WorkInProgress} from "./component/WorkInProgress/index";
import {LoadingIndicator} from "./component/LoadingIndicator/index";
import {UploadImageModal} from "./component/UploadImage/index";
import {PalettePicker} from "./component/PalettePicker/index";
import {GatewayRoute} from "./component/GatewayRoute/index";
import {ImageFileInput} from "./component/ImageFileInput/index";
import {ImagePreview} from "./component/ImagePreview/index";
import {TableFilter} from "./pipes/TableFilter";

export const HICommonModule = {
	declarations: [
		TableFilter,
		ModalComponent,
		ModalBoxComponent,
		ProgressLock,
		LoadingLinearIndicator,
		HelpModal,
		TextParser,
		ImageCropper,
		Nothing,
		WorkInProgress,
		LoadingIndicator,
		UploadImageModal,
		PalettePicker,
		GatewayRoute,
		ImageFileInput,
		ImagePreview,
	]
};