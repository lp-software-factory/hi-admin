import {Routes, RouterModule} from "@angular/router";

import {AppRoute} from "../../../module/ui/routes/AppRoute/index";
import {AppProtectedRoute} from "../../../module/ui/routes/AppProtectedRoute/index";
import {AppPublicRoute} from "../../../module/ui/routes/AppPublicRoute/index";
import {DashboardRoute} from "../../../module/ui/routes/DashboardRoute/index";
import {SignInRoute} from "../../../module/auth/routes/SignInRoute/index";
import {ModuleWithProviders} from "@angular/core";
import {FrontendResolver} from "../../../module/frontend/resolve/FrontendResolver";
import {JWTValidationResolver} from "../../../module/auth/resolve/JWTValidationResolver";
import {CommonRoute} from "../../../module/ui/routes/CommonRoute/index";
import {SignOutRoute} from "../../../module/auth/routes/SignOutRoute/index";
import {SystemRoute} from "../../../module/ui/routes/SystemRoute/index";
import {DesignersRoute} from "../../../module/ui/routes/DesignersRoute/index";
import {HIDesignerRequestRoutes} from "../../../module/designer-requests/module";
import {HIOAuth2ProvidersRoute} from "../../../module/oauth2/providers/module";
import {HICatalogRoutes, CatalogsRoute} from "../../../module/ui/routes/CatalogsRoute/index";
import {HIInviteCardsRoutes, InviteCardsRoute} from "../../../module/ui/routes/InviteCardsRoute/index";
import {HIServicesRoutes, ServicesRoute} from "../../../module/ui/routes/ServicesRoute/index";
import {HIEnvelopeRoute} from "../../../module/envelope/Envelope/module";
import {IntegrationsRoute} from "../../../module/ui/routes/IntegrationsRoute/index";
import {HIFontRoutes} from "../../../module/fonts/module";
import {HIInviteCardGammaRoute} from "../../../module/invites/InviteCardGamma/module";
import {HIInviteCardStyleRoute} from "../../../module/invites/InviteCardStyle/module";
import {HIInviteCardSizeRoute} from "../../../module/invites/InviteCardSize/module";
import {HILocaleRoutes} from "../../../module/locale/module";
import {HICurrencyRoutes} from "../../../module/currency/module";
import {HICompanyRoutes} from "../../../module/company/module";
import {HIEventTypeGroupRoutes} from "../../../module/events/EventTypeGroup/module";
import {HIEventTypeRoutes} from "../../../module/events/EventType/module";
import {HIProductModuleRoutes} from "../../../module/products/module";
import {HIProductsPackageRoutes} from "../../../module/products-package/module";
import {HIDressCodeRoutes} from "../../../module/dress-code/module";

export const appRoutes: Routes = [
	{
		path: '',
		redirectTo: '/admin',
		pathMatch: 'full'
	},
	{
		path: 'admin',
		component: AppRoute,
		resolve: {
			auth: JWTValidationResolver,
		},
		children: [
			{
				path: '',
				redirectTo: 'protected',
				pathMatch: 'full'
			},
			{
				path: 'protected',
				component: AppProtectedRoute,
				resolve: {
					frontend: FrontendResolver
				},
				children: [
					{
						path: 'designers',
						component: DesignersRoute,
						children: [
							HIDesignerRequestRoutes,
						],
					},
					{
						path: 'system',
						component: SystemRoute,
						children: []
					},
					{
						path: 'integrations',
						component: IntegrationsRoute,
						children: [
							HIOAuth2ProvidersRoute,
						]
					},
					{
						path: '',
						redirectTo: 'dashboard',
						pathMatch: 'full'
					},
					{
						path: 'dashboard',
						component: DashboardRoute,
						pathMatch: 'full'
					},
					{
						path: 'c',
						component: CommonRoute,
						children: [
							{
								path: 'sign-out',
								component: SignOutRoute,
								pathMatch: 'full'
							}
						]
					},
					HICatalogRoutes,
					HIInviteCardsRoutes,
					HIEnvelopeRoute,
					HIServicesRoutes,
					{
						path: 'catalogs',
						component: CatalogsRoute,
						children: [
							HILocaleRoutes,
							HICurrencyRoutes,
							HICompanyRoutes,
							HIEventTypeGroupRoutes,
							HIEventTypeRoutes,
							HIFontRoutes,
							HIDressCodeRoutes,
						]
					},
					{
						path: 'invites',
						component: InviteCardsRoute,
						children: [
							HIInviteCardStyleRoute,
							HIInviteCardSizeRoute,
							HIInviteCardGammaRoute,
						]
					},
					HIEnvelopeRoute,
					{
						path: 'services',
						component: ServicesRoute,
						children: [
							HIProductModuleRoutes,
							HIProductsPackageRoutes,
						]
					}
				]
			},
			{
				path: 'public',
				component: AppPublicRoute,
				resolve: {
					frontend: FrontendResolver
				},
				children: [
					{
						path: '',
						redirectTo: 'sign-in',
						pathMatch: 'full'
					},
					{
						path: 'sign-in',
						component: SignInRoute,
						pathMatch: 'full'
					}
				]
			}
		]
	}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);