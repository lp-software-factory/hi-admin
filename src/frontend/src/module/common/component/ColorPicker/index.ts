import {Component, Input, Output, EventEmitter} from "@angular/core";

import {ColorPickerService} from "./module";

@Component({
	selector: 'hi-color-picker',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class ColorPicker
{
	private _color: string;

	constructor(private cpService: ColorPickerService) {
	}

	@Output() valueChange = new EventEmitter<string>();

	@Input()
	get value() {
		return this._color;
	}

	set value(val) {
		this._color = val;
		this.valueChange.emit(val);
	}
}