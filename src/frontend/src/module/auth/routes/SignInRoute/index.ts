import {Component} from "@angular/core";
import {AuthModalsService} from "../../component/Modals/service";

@Component({
	template: require('./template.jade')
})
export class SignInRoute
{
	constructor(private authModals: AuthModalsService) {
		authModals.signIn();
	}
}