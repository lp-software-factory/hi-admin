import {Nothing} from "../../common/component/Nothing/index";
import {EventTypeCRUDRoute} from "./routes/EventTypeCRUDRoute/index";
import {CreateEventTypeModal} from "./component/Modals/CreateEventTypeModal/index";
import {DeleteEventTypeModal} from "./component/Modals/DeleteEventTypeModal/index";
import {EditEventTypeModal} from "./component/Modals/EditEventTypeModal/index";
import {EventTypeSelector} from "./component/Elements/EventTypeSelector/index";
import {EventTypeRepository} from "./repository/EventTypeRepository";
import {EventTypeRepositoryResolve} from "./resolve/EventTypeRepositoryResolve";
import {EventTypeGroupRepositoryResolve} from "../EventTypeGroup/resolve/EventTypeGroupRepositoryResolve";
import {EventTypeGatewayRoute} from "./routes/EventTypeGatewayRoute/index";
import {EventTypeForm} from "./component/Forms/EventTypeForm/index";
import {EventTypeDataTable} from "./component/Elements/EventTypeDatatable/index";
import {EventTypeRESTService} from "../../../../definitions/src/services/event/event-type/EventTypeRESTService";

export const HIEventTypeModule = {
	routes: [
		EventTypeCRUDRoute,
		EventTypeGatewayRoute,
	],
	declarations: [
		EventTypeForm,
		EventTypeDataTable,
		CreateEventTypeModal,
		DeleteEventTypeModal,
		EditEventTypeModal,
		EventTypeSelector
	],
	providers: [
		EventTypeRepository,
		EventTypeRepositoryResolve,
	],
};

export const HIEventTypeRoutes = {
	path: 'event_type',
	component: EventTypeGatewayRoute,
	resolve: [
		EventTypeGroupRepositoryResolve,
	],
	children: [
		{
			path: '',
			component: Nothing,
			pathMatch: 'full'
		},
		{
			path: ':eventGroupTypeId',
			component: EventTypeCRUDRoute,
			pathMath: 'full',
			resolve: [
				EventTypeRepositoryResolve,
			]
		}
	]
};