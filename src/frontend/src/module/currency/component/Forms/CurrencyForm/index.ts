import {Component, EventEmitter, Output, Input} from "@angular/core";
import {CreateCurrencyRequest} from "../../../../../../definitions/src/definitions/money/paths/create";

export enum CurrencyFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class CurrencyFormModel implements CreateCurrencyRequest
{
	code = '';
	sign = '';
	title = [];
}

@Component({
	selector: 'hi-currency-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class CurrencyForm
{
	@Output() modelChange: EventEmitter<CurrencyFormModel> = new EventEmitter<CurrencyFormModel>();

	@Input('mode') mode: CurrencyFormMode = CurrencyFormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: CurrencyFormModel;
}