import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {LocaleService} from "../../../../../locale/service/LocaleService";
import {InviteCardGammaEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {InviteCardGammaRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";


@Component({
	selector: 'hi-invite-card-gamma-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteInviteCardGammaModal implements AfterViewInit
{
	@Input('invite_card_gamma') invite_card_gamma: InviteCardGammaEntity;
	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: InviteCardGammaRESTService, private localeService: LocaleService) {
	}


	getInviteCardGammaName(): string {
		return this.localeService.getLocalization(this.invite_card_gamma.title).value;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let InviteCardGammaId = this.invite_card_gamma.id;
		let loading = this.status.addLoading();

		this.service.deleteInviteCardGamma(InviteCardGammaId).subscribe(() => {
			this.successEvent.emit(InviteCardGammaId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}