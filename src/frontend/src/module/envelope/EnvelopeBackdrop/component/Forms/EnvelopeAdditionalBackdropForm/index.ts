import {Component, EventEmitter, Output, Input} from "@angular/core";

import {Validators} from "../../../../../common/functions/validators";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AttachmentEntity} from "../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {getFileTitle} from "../../../functions/getFileTitle";
import {ImageAttachmentMetadata} from "../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

export enum EnvelopeAdditionalBackdropFormMode
{
	Create = <any>"create",
	Edit = <any>"edit"
}

export class EnvelopeAdditionalBackdropFormModel
{
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	files: {
		resource: any,
		preview: any,
	} = {
		resource: undefined,
		preview: undefined,
	};

	uploaded: {
		resource: AttachmentEntity<ImageAttachmentMetadata>,
		preview: AttachmentEntity<ImageAttachmentMetadata>,
	} = {
		resource: undefined,
		preview: undefined,
	};
}

@Component({
	selector: 'hi-envelope-additional-backdrop-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopeAdditionalBackdropForm
{
	@Output() modelChange: EventEmitter<EnvelopeAdditionalBackdropFormModel> = new EventEmitter<EnvelopeAdditionalBackdropFormModel>();

	@Input('mode') mode: EnvelopeAdditionalBackdropFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EnvelopeAdditionalBackdropFormModel = new EnvelopeAdditionalBackdropFormModel();

	onFileAttachmentChange(event) {
		this.model.files.resource = event.target.files[0];
	}
	
	deleteFile(type: string) {
		if(type === 'resource'){
			this.model.uploaded.resource = undefined;
			this.model.files.resource = undefined;
		} else if(type === 'preview') {
			this.model.uploaded.preview = undefined;
			this.model.files.preview = undefined;
		}
	}

	onFilePreviewChange(event) {
		this.model.files.preview = event.target.files[0];
	}

	getFileTitle(type: string): string {
		return getFileTitle(type, this.model);
	}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.withId(this.model.uploaded.resource) || (this.model.files.resource instanceof Blob) },
			() => { return Validators.withId(this.model.uploaded.preview) || this.model.files.preview instanceof Blob },
			() => { return Validators.localeInputNotEmpty(this.model.title) },
			() => { return Validators.localeInputNotEmpty(this.model.description) }
		];

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}
}