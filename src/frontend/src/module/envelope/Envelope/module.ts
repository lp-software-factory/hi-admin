import {EnvelopeRoute} from "./routes/EnvelopeRoute/index";

import {HIEnvelopeMarksRoute} from "../EnvelopeMarks/module";
import {HIEnvelopeColorRoute} from "../EnvelopeColor/module";
import {HIEnvelopeTextureRoute} from "../EnvelopeTexture/module";
import {HIEnvelopePatternRoute} from "../EnvelopePattern/module";
import {HIEnvelopeBackdropRoute} from "../EnvelopeBackdrop/module";

export const HIEnvelopeModule = {
    routes: [
        EnvelopeRoute,
    ],
    declarations: [
    ],
    providers: [
    ],
};

export const HIEnvelopeRoute =  {
    path: 'envelope',
    component: EnvelopeRoute,
    children: [
        HIEnvelopeMarksRoute,
        HIEnvelopeColorRoute,
        HIEnvelopeTextureRoute,
        HIEnvelopePatternRoute,
        HIEnvelopeBackdropRoute
    ]
};