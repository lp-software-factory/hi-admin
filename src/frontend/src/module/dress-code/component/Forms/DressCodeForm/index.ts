import {Component, EventEmitter, Output, Input} from "@angular/core";
import {DressCodeRequestCreate} from "../../../../../../definitions/src/definitions/dress-code/path/create";
import {LocalizedString} from "../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AttachmentEntity} from "../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

export enum DressCodeFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class DressCodeFormModel implements DressCodeRequestCreate
{
	image_attachment_id: number = undefined;
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	description_men: LocalizedString[] = [];
	description_women: LocalizedString[] = [];
}

@Component({
	selector: 'hi-dress-code-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class DressCodeForm
{
	@Output() modelChange: EventEmitter<DressCodeFormModel> = new EventEmitter<DressCodeFormModel>();
	@Input('mode') mode: DressCodeFormMode = DressCodeFormMode.Create;

	@Input('preview') preview: AttachmentEntity<ImageAttachmentMetadata>;

	attachPreview($event: AttachmentEntity<ImageAttachmentMetadata>) {
		this.model.image_attachment_id = $event.id;
	}

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: DressCodeFormModel;
}