import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {EnvelopePatternRepository} from "../repository/EnveloperPatternRepository";
import {GetAllEnvelopePatternsResponse200} from "../../../../../definitions/src/definitions/envelope/pattern/path/get-all";

@Injectable()
export class EnvelopePatternRepositoryResolve implements Resolve<GetAllEnvelopePatternsResponse200>
{
	constructor(private repository: EnvelopePatternRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}