import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CompanyEntity} from "../../../../../../definitions/src/definitions/company/entity/CompanyEntity";

@Component({
	selector: 'hi-company-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CompanyDataTable
{
	@Input('entities') entities: CompanyEntity[] = [];

	constructor(private cursor: DTCursor<CompanyEntity>,) {
	}
}