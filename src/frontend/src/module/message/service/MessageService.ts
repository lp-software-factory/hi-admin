import {Injectable} from "@angular/core";

export enum MESSAGE_LEVEL
{
	Message = <any>"message",
	Primary = <any>"primary",
	Info = <any>"info",
	Success = <any>"success",
	Warning = <any>"warning",
	Danger = <any>"danger",
}

@Injectable()
export class MessageService
{
	private static NEXT_ID = 0;
	private static DEFAULT_TIMEOUT = 5 /* sec */ * 1000 /* ms */;

	private messages: Message[] = [];

	public push(level: MESSAGE_LEVEL, message: string, translate: boolean = false) {
		let id = ++MessageService.NEXT_ID;

		this.messages.push({
			id: id,
			level: level,
			translate: translate,
			content: message,
		});

		setTimeout(() => {
			this.remove(id);
		}, MessageService.DEFAULT_TIMEOUT);
	}

	public remove(id: number) {
		this.messages = this.messages.filter(message => {
			return message.id !== id;
		})
	}

	public list(): Message[] {
		return this.messages;
	}
}

export interface Message
{
	id: number;
	level: MESSAGE_LEVEL;
	translate: boolean;
	content: string;
}