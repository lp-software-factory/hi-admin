import {Component, Input} from "@angular/core";

import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {Font} from "../../../../../../definitions/src/definitions/font/entity/Font";

@Component({
	selector: 'hi-font-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class FontDataTable
{
	@Input('entities') entities: Font[] = [];

	private searchStr: string = '';
	private sortType: string = '';

	constructor(
		private cursor: DTCursor<Font>
	) {}

	getPreview(font: Font): string {
		return font.entity.preview.link.url;
	}
}