import {Component, EventEmitter, Output, Input} from "@angular/core";

import {Validators} from "../../../../../common/functions/validators";
import {AttachmentEntity} from "../../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {getEnvelopeTextureFileTitle} from "../../../functions/getFileTitle";
import {ImageAttachmentMetadata} from "../../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";

export enum EnvelopeAdditionalTextureFormMode
{
	Create = <any>"create",
	Edit = <any>"edit"
}

export class EnvelopeAdditionalTextureFormModel
{
	files: {
		preview: any,
	} = {
		preview: undefined,
	};

	uploaded: {
		preview: AttachmentEntity<ImageAttachmentMetadata>,
	} = {
		preview: undefined,
	};
}

@Component({
	selector: 'hi-envelope-additional-texture-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopeAdditionalTextureForm
{
	@Output() modelChange: EventEmitter<EnvelopeAdditionalTextureFormModel> = new EventEmitter<EnvelopeAdditionalTextureFormModel>();

	@Input('mode') mode: EnvelopeAdditionalTextureFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EnvelopeAdditionalTextureFormModel = new EnvelopeAdditionalTextureFormModel();
	
	deleteFile(type: string) {
		if (type === 'preview') {
			this.model.uploaded.preview = undefined;
			this.model.files.preview = undefined;
		}
	}

	onFilePreviewChange(event) {
		this.model.files.preview = event.target.files[0];
	}

	getFileTitle(type: string): string {
		return getEnvelopeTextureFileTitle(type, this.model);
	}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.withId(this.model.uploaded.preview) || this.model.files.preview instanceof Blob }
		];

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}
}