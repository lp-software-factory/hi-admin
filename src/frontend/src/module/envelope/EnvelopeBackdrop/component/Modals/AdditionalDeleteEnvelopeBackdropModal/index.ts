import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalBackdropFormModel} from "../../Forms/EnvelopeAdditionalBackdropForm/index";


@Component({
	selector: 'hi-envelope-backdrop-additional-crud-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class AdditionalDeleteEnvelopeBackdropModal implements AfterViewInit
{
	@Input('entity') backdrop: EnvelopeAdditionalBackdropFormModel;

	@Output('success') successEvent: EventEmitter<any> = new EventEmitter<any>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	getAttachmentTitle(): string {
		if(this.backdrop.files.resource !== undefined){
			if(this.backdrop.files.resource.name !== undefined){
				return this.backdrop.files.resource.name.substr(0, this.backdrop.files.resource.name.length - (this.backdrop.files.resource.name.length - 30));
			}
		} else if(this.backdrop.uploaded.resource !== undefined){
			if(this.backdrop.uploaded.resource.title !== undefined){
				return this.backdrop.uploaded.resource.title.substr(0, this.backdrop.uploaded.resource.title.length - (this.backdrop.uploaded.resource.title.length - 30));
			}
		}
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		this.successEvent.emit();
	}

	cancel() {
		this.cancelEvent.emit();
	}
}