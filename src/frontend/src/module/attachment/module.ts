import {AttachmentInstantInput} from "./component/AttachmentInstantInput/index";
import {AttachmentImageInstantInput} from "./component/AttachmentImageInstantInput/index";

export const HIAttachmentModule = {
    routes: [
    ],
    declarations: [
        AttachmentInstantInput,
        AttachmentImageInstantInput,
    ],
    providers: [
    ],
};