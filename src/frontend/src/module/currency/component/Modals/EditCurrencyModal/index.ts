import {Component, EventEmitter, Output, Input, OnChanges, SimpleChanges} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CurrencyEntity} from "../../../../../../definitions/src/definitions/money/entity/CurrencyEntity";
import {clone} from "../../../../common/functions/clone";
import {CurrencyFormModel} from "../../Forms/CurrencyForm/index";
import {CurrencyRESTService} from "../../../../../../definitions/src/services/currency/CurrencyRESTService";

@Component({
	selector: 'hi-currency-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditCurrencyModal implements OnChanges
{
	@Input('currency') currency: CurrencyEntity;

	@Output('success') successEvent: EventEmitter<CurrencyEntity> = new EventEmitter<CurrencyEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new CurrencyFormModel();

	constructor(private service: CurrencyRESTService) {
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.formModel = {
			code: this.currency.code,
			sign: this.currency.sign,
			title: clone(this.currency.title),
		};
	}

	ngOnInit() {
		this.formModel = JSON.parse(JSON.stringify(this.currency));
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.editCurrency(this.currency.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.currency);
			},
			error => {
				loading.is = false;
			}
		);
	}
}