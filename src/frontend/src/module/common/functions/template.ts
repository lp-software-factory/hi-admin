var S = require('string');

export function template(value, replaces = {}) {
	return "" + S(value).template(replaces, '{', '}');
}