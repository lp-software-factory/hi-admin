var webpack = require("webpack");

function WebpackConfigBuilder() {
  this.publicPath = __dirname + '/../www/admin/app/dist';
  this.wwwPath = '/admin/app/dist';
  this.bundlesDir = 'bundles';
}

WebpackConfigBuilder.prototype = {
  build: function() {
    return {
      node: {
        fs: "empty"
      },
      entry: {
        main: './src/app/admin-app/app.ts'
      },
      output: {
        filename: '[name].js',
        path: this.publicPath + '/' + this.bundlesDir,
        publicPath: this.wwwPath + '/' + this.bundlesDir + '/'
      },
      devtool: 'cheap-module-source-map',
      resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
      },
      watchOptions: {
        poll: true,
        ignored: /node_modules/
      },
      module: {
        loaders: [
          {
            test: /\.jpe?g$|\.gif$|\.png$/i,
            loader: "file"
          },
          {
            test: /\.css$/,
            loader: "style-loader!css-loader"
          },
          {
            test: /\.ts$/,
            loader: 'ts-loader',
            exclude: []
          },
          {
            test: /\.json$/,
            loader: 'json-loader'
          },
          {
            test: /\.html$/,
            loader: 'raw-loader'
          },
          {
            test: /\.help.md$/,
            loader: 'raw-loader'
          },
          {
            test: /\.head.scss$/,
            loaders: ["style", "css", "sass"]
          },
          {
            test: /\.shadow.scss$/,
            loaders: ["raw-loader", "sass"]
          },
          {
            test: /\.scss$/,
            loaders: ["style", "css", "sass"],
            exclude: /head|shadow\.scss$/
          },
          {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "url-loader?limit=10000&minetype=application/font-woff"
          },
          {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            loader: "file-loader"
          },
          {
            test: /\.jade$/,
            loaders: ['raw-loader', 'jade-html']
          }
        ] 
      },
      plugins: [
        new webpack.ContextReplacementPlugin(
          /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
          __dirname
        )
      ]
    }
  }
};

module.exports = (new WebpackConfigBuilder()).build();