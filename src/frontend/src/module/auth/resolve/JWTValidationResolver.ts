import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {CurrentResponse200} from "../../../../definitions/src/definitions/auth/paths/current";
import {AuthService} from "../service/AuthService";
import {AuthRESTService} from "../../../../definitions/src/services/auth/AuthRESTService";

@Injectable()
export class JWTValidationResolver implements Resolve<CurrentResponse200>
{
	constructor(private authRESTService: AuthRESTService,
		private authService: AuthService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		if(this.authService.hasJWT() && this.authService.getJWT() !== undefined) {
			let observable = this.authRESTService.current();

			observable.subscribe(
				response => {
					this.authService.signIn(response.token);
				},
				error => {
					this.authService.signOut();
				}
			);

			return observable;
		} else {
			return true;
		}
	}
}