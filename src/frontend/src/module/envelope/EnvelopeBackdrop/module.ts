import {EnvelopeBackdropForm} from "./component/Forms/EnvelopeBackdropForm/index";
import {EnvelopeBackdropDatatable} from "./component/Elements/EnvelopeBackdropDatatable/index";
import {CreateEnvelopeBackdropModal} from "./component/Modals/CreateEnvelopeBackdropModal/index";
import {DeleteEnvelopeBackdropModal} from "./component/Modals/DeleteEnvelopeBackdropModal/index";
import {EnvelopeBackdropRepository} from "./repository/EnvelopeBackdropRepository";
import {EnvelopeBackdropRepositoryResolve} from "./resolve/EnvelopeBackdropRepositoryResolve";
import {EnvelopeBackdropCRUDRoute} from "./routes/EnvelopeBackdropCRUDRoute/index";
import {EditEnvelopeBackdropModal} from "./component/Modals/EditEnvelopeBackdropModal/index";
import {EnvelopeAdditionalBackdropCrudDataTable} from "./component/Elements/EnvelopeAdditionalBackdropDatatable/index";
import {AdditionalCreateEnvelopeBackdropModal} from "./component/Modals/AdditionalCreateEnvelopeBackdropModal/index";
import {AdditionalEditEnvelopeBackdropModal} from "./component/Modals/AdditionalEditEnvelopeBackdropModal/index";
import {AdditionalDeleteEnvelopeBackdropModal} from "./component/Modals/AdditionalDeleteEnvelopeBackdropModal/index";
import {EnvelopeAdditionalBackdropForm} from "./component/Forms/EnvelopeAdditionalBackdropForm/index";
import {AttachmentRESTService} from "../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeBackdropRESTService} from "../../../../definitions/src/services/envelope/envelope-backdrop/EnvelopeBackdropRESTService";

export const HIEnvelopeBackdropModule = {
	routes: [
		EnvelopeBackdropCRUDRoute,
	],
	declarations: [
		AdditionalCreateEnvelopeBackdropModal,
		AdditionalEditEnvelopeBackdropModal,
		AdditionalDeleteEnvelopeBackdropModal,
		EnvelopeAdditionalBackdropForm,
		EnvelopeBackdropForm,
		EnvelopeBackdropDatatable,
		EnvelopeAdditionalBackdropCrudDataTable,
		CreateEnvelopeBackdropModal,
		EditEnvelopeBackdropModal,
		DeleteEnvelopeBackdropModal
	],
	providers: [
		EnvelopeBackdropRepository,
		EnvelopeBackdropRepositoryResolve
	],
};

export const HIEnvelopeBackdropRoute = {
	path: 'backdrop',
	component: EnvelopeBackdropCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EnvelopeBackdropRepositoryResolve
	]
};