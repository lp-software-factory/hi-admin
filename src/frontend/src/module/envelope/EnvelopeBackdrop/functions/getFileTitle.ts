import {EnvelopeAdditionalBackdropFormModel} from "../component/Forms/EnvelopeAdditionalBackdropForm/index";

export function getFileTitle(type: string, entity: EnvelopeAdditionalBackdropFormModel) {
    if(type == 'resource') {
        if(entity.files.resource !== undefined){
            if(entity.files.resource.name !== undefined){
                return entity.files.resource.name.substr(0, entity.files.resource.name.length - (entity.files.resource.name.length - 30));
            }
        } else if(entity.uploaded.resource !== undefined){
            if(entity.uploaded.resource.title !== undefined){
                return entity.uploaded.resource.title.substr(0, entity.uploaded.resource.title.length - (entity.uploaded.resource.title.length - 30));
            }
        }
    }else if(type === 'preview') {
        if(entity.files.preview !== undefined){
            if(entity.files.preview.name !== undefined){
                return entity.files.preview.name.substr(0, entity.files.preview.name.length - (entity.files.preview.name.length - 30));
            }
        } else if(entity.uploaded.preview !== undefined){
            if(entity.uploaded.preview.title !== undefined){
                return entity.uploaded.preview.title.substr(0, entity.uploaded.preview.title.length - (entity.uploaded.preview.title.length - 30));
            }
        }
    }else{
        throw new Error(`Unknown type "${type}"`);
    }
}