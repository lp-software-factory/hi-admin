import {Component, Input} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeAdditionalBackdropFormModel} from "../../Forms/EnvelopeAdditionalBackdropForm/index";
import {getFileTitle} from "../../../functions/getFileTitle";

@Component({
	selector: 'hi-envelope-additional-backdrop-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeAdditionalBackdropCrudDataTable
{
	@Input('entities') entities: EnvelopeAdditionalBackdropFormModel[] = [];

	constructor(private cursor: DTCursor<EnvelopeAdditionalBackdropFormModel>)
	{}
	
	getFileTitle(type: string, entity: EnvelopeAdditionalBackdropFormModel) {
		return getFileTitle(type, entity);
	}
}