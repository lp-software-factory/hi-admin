import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {DesignerRequestEntity} from "../../../../definitions/src/definitions/designer-requests/entity/DesignerRequestEntity";
import {GetDesignerRequestsListRequest, GetDesignerRequestsListResponse200} from "../../../../definitions/src/definitions/designer-requests/paths/get-designer-requests-list";
import {DesignerRequestsRESTService} from "../../../../definitions/src/services/designer-request/DesignerRequestsRESTService";

@Injectable()
export class DesignerRequestsRepository
{
	private requests: DesignerRequestEntity[] = [];

	constructor(private rest: DesignerRequestsRESTService) {
	}

	fetch(criteria: GetDesignerRequestsListRequest): Observable<GetDesignerRequestsListResponse200> {
		let observable = this.rest.getDesignerRequestsList(criteria);

		observable.subscribe(response => {
			this.requests = response.designer_requests;
		});

		return observable;
	}

	getAll(): DesignerRequestEntity[] {
		return this.requests;
	}

	getById(id: number): DesignerRequestEntity {
		let result = this.requests.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Designer with ID "${id}" not found`);
		}

		return result[0];
	}

	addDesignerRequest(entity: DesignerRequestEntity) {
		this.requests.push(entity);
	}

	removeDesignerRequest(entityId: number) {
		this.requests = this.requests.filter(entity => entity.id !== entityId);
	}

	replaceDesignerRequest(entityId: number, entity: DesignerRequestEntity) {
		this.requests = this.requests.map(dr => {
			return dr = dr.id === entityId ? entity : dr
		});
	}

	updateRows(replace?: DesignerRequestEntity) {
		if(replace) {
			this.replaceDesignerRequest(replace.id, replace);
		}

		this.requests = this.getAll();
	}
}