import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalMarksFormModel} from "../../Forms/EnvelopeAdditionalMarksForm/index";


@Component({
	selector: 'hi-envelope-marks-additional-crud-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class AdditionalDeleteEnvelopeMarksModal implements AfterViewInit
{
	@Input('entity') marks: EnvelopeAdditionalMarksFormModel;

	@Output('success') successEvent: EventEmitter<any> = new EventEmitter<any>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	getAttachmentTitle(): string {
		if(this.marks.uploaded.attachment !== undefined){
			if(this.marks.uploaded.attachment.title !== undefined){
				return this.marks.uploaded.attachment.title.substr(0, this.marks.uploaded.attachment.title.length - (this.marks.uploaded.attachment.title.length - 30));
			}
		}
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		this.successEvent.emit();
	}

	cancel() {
		this.cancelEvent.emit();
	}
}