import {Component, Input, Output, EventEmitter} from "@angular/core";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

export enum EventTypeGroupMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export interface EventTypeGroupFormModel
{
	url: string,
	title: LocalizedString[];
	description: LocalizedString[];
}

@Component({
	selector: 'hi-event-type-group-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EventTypeGroupForm
{
	@Output() modelChange: EventEmitter<EventTypeGroupFormModel> = new EventEmitter<EventTypeGroupFormModel>();

	@Input('mode') mode: EventTypeGroupMode = EventTypeGroupMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EventTypeGroupFormModel;
}