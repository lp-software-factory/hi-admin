import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EnvelopeColorRepository} from "../../repository/EnvelopeColorRepository";
import {EnvelopeMarks} from "../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopeColorEntity} from "../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {EnvelopeColorRESTService} from "../../../../../../definitions/src/services/envelope/envelope-color/EnvelopeColorRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EnvelopeColorRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EnvelopeColorRESTService
		},
	]
})

export class EnvelopeColorCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EnvelopeMarks>,
		private m: CRUDGenericModalsHelper,
		private r: EnvelopeColorRepository) {
	}

	getEntities(): EnvelopeColorEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}
