import {Component, Input, Output, EventEmitter} from "@angular/core";
import {PaletteEntity} from "../../../../../definitions/src/definitions/colors/entity/PaletteEntity";
import {FrontendService} from "../../../frontend/service/FrontendService";
import {PalettePresetsRepository} from "../../../palette/repository/PalettePresetsRepository";


@Component({
	selector: 'hi-palette-picker',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class PalettePicker
{
	private palettes: PaletteEntity[] = [];

	private currentPalette: PaletteEntity;

	@Output() valueChange = new EventEmitter<PaletteEntity>();

	@Input()
	get value() {
		return this.currentPalette;
	}

	set value(val) {
		this.currentPalette = val;
		this.valueChange.emit(val);
	}

	select(palette: PaletteEntity) {
		this.value = palette;
		this.valueChange.emit(this.value);
	}


	constructor(frontend: FrontendService,
		palettePresetsRepository: PalettePresetsRepository) {
		let palettes = palettePresetsRepository.getAll();

		for(let code in palettes) {
			if(palettes.hasOwnProperty(code)) {
				this.palettes.push(palettes[code]);
			}
		}
	}
}