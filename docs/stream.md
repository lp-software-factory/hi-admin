Используемые инструменты
========================

- http://bulma.io/documentation/
- https://swimlane.gitbooks.io/angular2-data-table/content/

CSS Names convention
====================

- Компоненты должны иметь следующие два класса: `component`, `component-...`
- Компоненты-роуты используют другие два класса: `route`, `route-...`

Роуты
=====

- Роуты прописывать в module.ts
- URL: прописывать в definition, подставлять параметры с помощью String.template
- .component - не требуется писать div
- .route