import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {EventTypeEntity} from "../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {GetEventTypesOfGroupResponse200} from "../../../../../definitions/src/definitions/events/event-type/paths/all-of-group";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {EventTypeRESTService} from "../../../../../definitions/src/services/event/event-type/EventTypeRESTService";

@Injectable()
export class EventTypeRepository implements CRUDSerialRepositoryInterface
{
	private etgId: number;
	private et: EventTypeEntity[] = [];

	constructor(private rest: EventTypeRESTService) {
	}

	fetch(etgId: number): Observable<GetEventTypesOfGroupResponse200> {
		let observable = this.rest.getEventTypesOfGroup(etgId);

		observable.subscribe(response => {
			this.et = response.event_types;
			this.etgId = etgId;
		});

		return observable;
	}

	getAll(): EventTypeEntity[] {
		return this.et;
	}

	getById(id: number): EventTypeEntity {
		let result = this.et.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`EventType with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.et.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.et, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.et, oldPosition, newPosition);
	}

	getCurrentETG(): number {
		return this.etgId;
	}

	addET(et: EventTypeEntity) {
		this.et.push(et);
	}

	removeET(etId: number) {
		this.et = this.et.filter(et => et.id !== etId);
	}

	replaceET(et: EventTypeEntity) {
		this.et = this.et.map(entity => {
			if(entity.id === et.id) {
				return et;
			} else {
				return entity;
			}
		})
	}
}