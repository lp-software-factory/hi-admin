import {Component, EventEmitter, Output, Input, OnChanges, SimpleChanges} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {FontFormModel} from "../../Forms/FontForm/index";
import {FontEntity, Font} from "../../../../../../definitions/src/definitions/font/entity/Font";
import {FontRESTService} from "../../../../../../definitions/src/services/font/FontRESTService";


@Component({
	selector: 'hi-font-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditFontModal
{
	@Input('entity') font: Font;

	@Output('success') successEvent: EventEmitter<Font> = new EventEmitter<Font>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: FontFormModel = new FontFormModel();

	constructor(private service: FontRESTService) {
	}
	
	ngOnInit() {
		this.formModel.files = JSON.parse(JSON.stringify(this.font.entity.files.map(file => {return file.id})));
		this.formModel.font_face = JSON.parse(JSON.stringify(this.font.entity.font_face));
		this.formModel.is_activated = JSON.parse(JSON.stringify(this.font.entity.is_activated));
		this.formModel.preview = JSON.parse(JSON.stringify(this.font.entity.preview.id));
		this.formModel.title = JSON.parse(JSON.stringify(this.font.entity.title));
	}
	

	canSave(): boolean {
		return this.status.isLoading() || (JSON.stringify(this.formModel.files) === JSON.stringify(this.font.entity.files.map(file => {return file.id})) &&
			JSON.stringify(this.formModel.font_face) === JSON.stringify(this.font.entity.font_face) &&
			JSON.stringify(this.formModel.is_activated) === JSON.stringify(this.font.entity.is_activated) &&
			JSON.stringify(this.formModel.preview) === JSON.stringify(this.font.entity.preview.id) &&
			JSON.stringify(this.formModel.title) === JSON.stringify(this.font.entity.title)
			);
	}
	
	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.edit(this.font.entity.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.font);
			},
			error => {
				loading.is = false;
			}
		);
	}
}