import {FrontendService} from "./service/FrontendService";
import {FrontendResolver} from "./resolve/FrontendResolver";
import {FrontendRESTService} from "../../../definitions/src/services/frontend/FrontendRESTService";

export const HIFrontendModule = {
	declarations: [],
	providers: [
		FrontendService,
		FrontendResolver,
	],
};