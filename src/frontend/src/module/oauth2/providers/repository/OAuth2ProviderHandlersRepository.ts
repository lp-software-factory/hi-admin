import {Observable} from "rxjs";
import {GetAllOAuth2HandlersResponse200} from "../../../../../definitions/src/definitions/oauth2/providers/paths/get-all-handlers";
import {Injectable} from "@angular/core";
import {OAuth2ProvidersRESTService} from "../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Injectable()
export class OAuth2ProviderHandlersRepository
{
	private currencies: string[] = [];

	constructor(private rest: OAuth2ProvidersRESTService) {
	}

	fetch(): Observable<GetAllOAuth2HandlersResponse200> {
		let observable = this.rest.getAllOAuth2Handlers();

		observable.subscribe(
			response => {
				this.currencies = response.handlers;
			}
		);

		return observable;
	}

	getAll(): string[] {
		return this.currencies;
	}
}