import {LocaleCRUDRoute} from "./routes/LocaleCRUDRoute/index";
import {CreateLocaleModal} from "./component/Modals/CreateLocaleModal/index";
import {LocaleService} from "./service/LocaleService";
import {DeleteLocaleModal} from "./component/Modals/DeleteLocaleModal/index";
import {LocaleInput} from "./component/Elements/LocaleInput/index";
import {LocaleSelector} from "./component/Elements/LocaleSelector/index";
import {LocaleRepositoryResolve} from "./resolve/LocaleRepositoryResolve";
import {LocaleRepository} from "./repository/LocaleRepository";
import {LocaleDataTable} from "./component/Elements/LocaleDatatable/index";
import {LocaleForm} from "./component/Form/LocaleForm/index";
import {LocaleRESTService} from "../../../definitions/src/services/locale/LocaleRESTService";

export const HILocaleModule = {
	routes: [
		LocaleCRUDRoute,
	],
	declarations: [
		LocaleForm,
		CreateLocaleModal,
		DeleteLocaleModal,
		LocaleInput,
		LocaleSelector,
		LocaleDataTable,
	],
	providers: [
		LocaleService,
		LocaleRepository,
		LocaleRepositoryResolve,
	],
};

export const HILocaleRoutes = {
	path: 'locales',
	component: LocaleCRUDRoute,
	pathMatch: 'full',
	resolve: [
		LocaleRepositoryResolve,
	]
};