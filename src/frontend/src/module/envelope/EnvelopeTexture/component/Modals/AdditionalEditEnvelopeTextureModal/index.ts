import {Component, EventEmitter, Output, Input, ViewChild} from "@angular/core";

import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalTextureFormModel, EnvelopeAdditionalTextureForm} from "../../Forms/EnvelopeAdditionalTextureForm/index";

@Component({
	selector: 'hi-envelope-texture-additional-crud-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalEditEnvelopeTextureModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalTextureFormModel> = new EventEmitter<EnvelopeAdditionalTextureFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	
	@Input('entity') formModel: EnvelopeAdditionalTextureFormModel;

	private status: LoadingManager = new LoadingManager();

	cancel() {
		this.cancelEvent.emit();
	}

	submitButtonActive(): boolean {
		if(this.formModel.files !== undefined) {
			return this.formModel.files.preview !== undefined;
		} else if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.preview !== undefined
		}
	}

	submit() {
		this.successEvent.emit(this.formModel);
	}
}