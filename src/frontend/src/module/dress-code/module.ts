import {DressCodeCRUDRoute} from "./routes/DressCodeCRUDRoute/index";
import {DressCodeDataTable} from "./component/Elements/DressCodeDataTable/index";
import {DressCodeForm} from "./component/Forms/DressCodeForm/index";
import {CreateDressCodeModal} from "./component/Modals/CreateDressCodeModal/index";
import {EditDressCodeModal} from "./component/Modals/EditDressCodeModal/index";
import {DeleteDressCodeModal} from "./component/Modals/DeleteDressCodeModal/index";
import {DressCodeRepository} from "./repository/DressCodeRepository";
import {DressCodeRepositoryResolve} from "./resolve/DressCodeRepositoryResolve";

export const HIDressCodeModule = {
	routes: [
		DressCodeCRUDRoute,
	],
	declarations: [
		DressCodeDataTable,
		DressCodeForm,
		CreateDressCodeModal,
		EditDressCodeModal,
		DeleteDressCodeModal,
	],
	providers: [
		DressCodeRepository,
		DressCodeRepositoryResolve,
	],
};

export const HIDressCodeRoutes = {
	path: 'dress_codes',
	component: DressCodeCRUDRoute,
	pathMatch: 'full',
	resolve: [
		DressCodeRepositoryResolve,
	]
};