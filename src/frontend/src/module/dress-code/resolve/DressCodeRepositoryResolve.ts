import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {DressCodeRepository} from "../repository/DressCodeRepository";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {DressCodeListResponse200} from "../../../../definitions/src/definitions/dress-code/path/list";

@Injectable()
export class DressCodeRepositoryResolve implements Resolve<DressCodeListResponse200>
{
	constructor(private repository: DressCodeRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}