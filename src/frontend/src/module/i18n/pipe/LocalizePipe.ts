import {PipeTransform, Pipe} from "@angular/core";
import {TranslationService} from "../service/TranslationService";
import {LocalizedString} from "../../../../definitions/src/definitions/locale/definitions/LocalizedString";

@Pipe({
	name: 'localize'
})
export class LocalizePipe implements PipeTransform
{
	constructor(private i18n: TranslationService) {
	}

	transform(value: LocalizedString[], ...args: any[]): string {
		return this.i18n.localize(value);
	}
}