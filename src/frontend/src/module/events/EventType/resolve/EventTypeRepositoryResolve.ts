import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {GetEventTypesOfGroupResponse200} from "../../../../../definitions/src/definitions/events/event-type/paths/all-of-group";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {EventTypeRepository} from "../repository/EventTypeRepository";

@Injectable()
export class EventTypeRepositoryResolve implements Resolve<GetEventTypesOfGroupResponse200>
{
	constructor(private repository: EventTypeRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch(route.params['eventGroupTypeId']);

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}