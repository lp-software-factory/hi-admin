import {Component, EventEmitter, Output, Input} from "@angular/core";

import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {AttachmentEntity, AttachmentMetadata} from "../../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {ImageAttachmentMetadata} from "../../../../../../definitions/src/definitions/attachment/entity/metadata/ImageAttachmentMetadata";
import {FONT_SUPPORT_NORMAL, FONT_SUPPORT_BOLD, FONT_SUPPORT_ITALIC, FONT_SUPPORT_BOLD_ITALIC, Font} from "../../../../../../definitions/src/definitions/font/entity/Font";
import {Validators} from "../../../../common/functions/validators";

export function createFormModelForFont(font: Font): FontFormModel
{
	return {
		preview: font.entity.preview.id,
		title: font.entity.title,
		supports: font.entity.supports,
		license: font.entity.license,
		font_face: font.entity.font_face,
		files: font.entity.files.map(file => {
			return file.id;
		}),
		is_activated: font.entity.is_activated
	};
}

export enum FontFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class FontFormModel
{
	preview: number;
	title: string = '';
	supports: number = FONT_SUPPORT_NORMAL | FONT_SUPPORT_BOLD | FONT_SUPPORT_ITALIC | FONT_SUPPORT_BOLD_ITALIC;
	license: string = '';
	font_face: string = '';
	files: number[] = [];
	is_activated: boolean = true;
}

@Component({
	selector: 'hi-font-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class FontForm
{
	@Output() modelChange: EventEmitter<FontFormModel> = new EventEmitter<FontFormModel>();

	@Input('attachments') attachments: AttachmentEntity<AttachmentMetadata>[] = [];
	@Input('preview') preview: AttachmentEntity<ImageAttachmentMetadata>;
	@Input('mode') mode: FontFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: FontFormModel;

	constructor(
		private l: LoadingManager,
	) {}


	toggleBold() {
		if(!!(this.model.supports & FONT_SUPPORT_BOLD)) {
			this.model.supports = this.model.supports ^ FONT_SUPPORT_BOLD;
		} else {
			this.model.supports = this.model.supports | FONT_SUPPORT_BOLD;
		}
	}

	toggleItalic() {
		if(!!(this.model.supports & FONT_SUPPORT_ITALIC)) {
			this.model.supports = this.model.supports ^ FONT_SUPPORT_ITALIC;
		} else {
			this.model.supports = this.model.supports | FONT_SUPPORT_ITALIC;
		}
	}

	toggleNormal() {
		if(!!(this.model.supports & FONT_SUPPORT_NORMAL)) {
			this.model.supports = this.model.supports ^ FONT_SUPPORT_NORMAL;
		} else {
			this.model.supports = this.model.supports | FONT_SUPPORT_NORMAL;
		}
	}

	toggleBoldItalic() {
		if(!!(this.model.supports & FONT_SUPPORT_BOLD_ITALIC)) {
			this.model.supports = this.model.supports ^ FONT_SUPPORT_BOLD_ITALIC;
		} else {
			this.model.supports = this.model.supports | FONT_SUPPORT_BOLD_ITALIC;
		}
	}

	isBold(): boolean {
		return !!(this.model.supports & FONT_SUPPORT_BOLD)
	}

	isItalic(): boolean {
		return !!(this.model.supports & FONT_SUPPORT_ITALIC)
	}

	isNormal(): boolean {
		return !!(this.model.supports & FONT_SUPPORT_NORMAL)
	}

	isBoldItalic(): boolean {
		return !!(this.model.supports & FONT_SUPPORT_BOLD_ITALIC);
	}

	attachPreview($event: AttachmentEntity<ImageAttachmentMetadata>){
		this.model.preview = $event.id;
	}

	attachAttachment($event: AttachmentEntity<AttachmentMetadata>[]){
		this.model.files = $event.map(file => {
			return file.id;
		});
	}
	
	deleteAttachments() {
		this.model.files = [];
	}
	
	deleteAttachment($event: AttachmentEntity<AttachmentMetadata>) {
		this.model.files = this.model.files.filter(file_id => {
			return $event.id !== file_id;
		})
	}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.withId(this.model.preview) },
			() => { return Validators.stringNotEmpty(this.model.title) },
			() => { return Validators.stringNotEmpty(this.model.license) },
			() => { return Validators.stringNotEmpty(this.model.font_face) },
		];

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}
}

