import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeMarksEntity, EnvelopeMarks} from "../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopeMarksFormModel} from "../../Forms/EnvelopeMarksForm/index";
import {EnvelopeMarksRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-marks/EnvelopeMarksRESTService";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeMarkDefinitionEntity} from "../../../../../../../definitions/src/definitions/envelope/mark-definition/entity/EnvelopeMarkDefinition";
import {EnvelopeAdditionalMarksFormModel} from "../../Forms/EnvelopeAdditionalMarksForm/index";
import {compareArrays} from "../../../../../common/functions/compareArrays";
import {EnvelopeMarkDefinitionRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-mark-definition/EnvelopeMarkDefinitionRESTService";
import {Observable} from "rxjs";
import {clone} from "../../../../../common/functions/clone";

@Component({
	selector: 'hi-envelope-marks-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor
	]
})
export class EditEnvelopeMarksModal
{
	@Input('envelope_marks') envelope_marks: EnvelopeMarksEntity;

	@Output('success') successEvent: EventEmitter<EnvelopeMarks> = new EventEmitter<EnvelopeMarks>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeMarksFormModel();

	constructor(private service: EnvelopeMarksRESTService,
				private definitionService: EnvelopeMarkDefinitionRESTService,
				private attachment: AttachmentRESTService,
				private l: LoadingManager,
				private m: CRUDGenericModalsHelper,
				private cursor: DTCursor<EnvelopeMarkDefinitionEntity>,
	) {}

	ngOnInit() {
		this.formModel.title = JSON.parse(JSON.stringify(this.envelope_marks.title));
		this.formModel.description = JSON.parse(JSON.stringify(this.envelope_marks.description));
		this.formModel.definitions = this.envelope_marks.definitions.map(entity => {
			return {id: entity.entity.id, title: entity.entity.title, description: entity.entity.description,
					attachment_id: undefined, owner_envelope_marks_id: this.envelope_marks.id,
					uploaded: {attachment: entity.entity.attachment}}
		})
	}


	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && ! this.status.isLoading();
	}

	checkChanges(): boolean{
		return !compareArrays(this.formModel.title, this.envelope_marks.title) ||
			!compareArrays(this.formModel.description, this.envelope_marks.description) ||
			!compareArrays(this.formModel.definitions.map(entity =>
					{return entity.uploaded.attachment ? entity.uploaded.attachment.id : undefined}),
					this.envelope_marks.definitions.map(entity => {return entity.entity.attachment.id})) ||
			!compareArrays(this.formModel.definitions.map(entity => {return entity.title}), this.envelope_marks.definitions.map(entity => {return entity.entity.title})) ||
			!compareArrays(this.formModel.definitions.map(entity => {return entity.description}), this.envelope_marks.definitions.map(entity => {return entity.entity.description}))
	}

	isSubmitButtonActive(): boolean {
		return this.checkRequiredFields() && this.checkChanges();
	}

	cancel() {
		this.cancelEvent.emit();
	}

	addDefinition(entity: EnvelopeAdditionalMarksFormModel) {
		this.formModel.definitions.push(entity);
	}

	replaceDefinition(entity: EnvelopeAdditionalMarksFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			if(compare.id === entity.id) {
				return entity;
			} else return compare;
		})
	}

	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(texture => JSON.stringify(texture) !== JSON.stringify(this.cursor.current()));
		this.cursor._current = undefined;
	}

	submit() {
		let loading = this.l.addLoading();
		let model: EnvelopeMarksFormModel = new EnvelopeMarksFormModel();
		model.title = JSON.parse(JSON.stringify(this.formModel.title));
		model.description = JSON.parse(JSON.stringify(this.formModel.description));

		this.service.editEnvelopeMarks(this.envelope_marks.id, model).subscribe(
			response => {
				let observables = [];
				let definitionsCopy = clone(response.envelope_marks.entity.definitions);
				for (let definition of this.formModel.definitions) {
					let observable = undefined;
					if (definition.uploaded.attachment) {
						definition.attachment_id = definition.uploaded.attachment.id;
					}
					definition.owner_envelope_marks_id = response.envelope_marks.entity.id;
					let found = false;
					for (let i in this.envelope_marks.definitions) {
						let respEntity = response.envelope_marks.entity.definitions[i].entity;
						if (respEntity.id == definition.id) {
							found = true;
							definitionsCopy = definitionsCopy.filter(entity => {
								return entity.entity.id !== definition.id;
							});
							if (!compareArrays(respEntity.title, definition.title) ||
										!compareArrays(respEntity.description, definition.description) ||
										respEntity.attachment.id != (definition.uploaded.attachment ? definition.uploaded.attachment.id: undefined)) {
								observable = this.definitionService.editEnvelopeMarkDefinition(definition.id, definition);
								observable.subscribe(definitionResponse => {
									response.envelope_marks.entity.definitions[i] = definitionResponse.envelope_mark_definition;
								});
								observables.push(observable);
							}
							break;
						}
					}
					if (!found) {
						observable = this.definitionService.createEnvelopeMarkDefinition(definition);
						observable.subscribe(definitionResponse => {
							response.envelope_marks.entity.definitions.push(definitionResponse.envelope_mark_definition)
						});
						observables.push(observable);
					}
				}
				for (let definitionToDelete of definitionsCopy) {
					let observable = this.definitionService.deleteEnvelopeMarkDefinition(definitionToDelete.entity.id);
					observable.subscribe(definitionResponse => {
						response.envelope_marks.entity.definitions = response.envelope_marks.entity.definitions.filter(entity => {
							return entity.entity.id != definitionToDelete.entity.id;
						})
					});
				}
				Observable.forkJoin(observables).subscribe(responseAll => {
					loading.is = false;
					this.successEvent.emit(response.envelope_marks);
				});
			},
			error => {
				loading.is = false;
			}
		);
	}
}