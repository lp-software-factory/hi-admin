import {Component, EventEmitter, Output, Input} from "@angular/core";
import {ProductEntity} from "../../../../../../definitions/src/definitions/products/entity/ProductEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {EditProductRequest} from "../../../../../../definitions/src/definitions/products/paths/edit";
import {LocalizedString} from "../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {ProductRESTService} from "../../../../../../definitions/src/services/product/ProductRESTService";


@Component({
	selector: 'hi-product-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditProductModal
{
	@Input('product') product: ProductEntity;
	@Input('repository') repository: any;

	@Output('success') successEvent: EventEmitter<ProductEntity> = new EventEmitter<ProductEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();


	private status: LoadingManager = new LoadingManager();
	private model: EditProductRequest = {
		title: Array<LocalizedString>(),
		description: Array<LocalizedString>(),
		is_activated: false,
		base_price: {
			currency: '',
			amount: 0
		}
	};

	constructor(private service: ProductRESTService) {
	}

	ngOnInit() {
		this.initProduct();
	}


	initProduct() {
		this.model.title = JSON.parse(JSON.stringify(this.product.title));
		this.model.description = JSON.parse(JSON.stringify(this.product.description));
		this.model.is_activated = this.product.is_activated;
		this.model.base_price.currency = this.product.base_price.currency.code;
		this.model.base_price.amount = this.product.base_price.amount;
	}

	updateColorId(event) {
		console.log(event);
	}

	cancel() {
		this.initProduct();
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();


		this.service.editProduct(this.product.id, this.model).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.product);
			},
			error => {
				loading.is = false;
			}
		);
	}
}