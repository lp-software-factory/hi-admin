import {clone} from "./clone";

export function compareArrays(a1, a2): boolean {
    if (!a1 || !a2) {
        return a1 == a2;
    }
    if (a1.constructor !== Array || a2.constructor !== Array) {
        return JSON.stringify(a1) === JSON.stringify(a2);
    }
    if (a1.length != a2.length) {
        return false;
    }
    let a2Copy = clone(a2);
    let found = -1;
    for (let a = 0; a < a1.length; a++) {
        found = -1;
        for (let b = 0; b < a2Copy.length; b++) {
            if (compareArrays(a1[a], a2Copy[b])) {
                found = b;
                break;
            }
        }
        if (found == -1) {
            return false;
        }
        a2Copy.splice(found, 1);
    }
    return true;
}
