import {DesignerRepository} from "./repository/DesignerRepository";
import {DesignerRepositoryResolve} from "./resolve/DesignerRepositoryResolve";
import {DesignerRESTService} from "../../../definitions/src/services/designer/DesignerRESTService";

export const HIDesignersModule = {
    routes: [
    ],
    declarations: [
    ],
    providers: [
        DesignerRepository,
        DesignerRepositoryResolve,
    ],
};