import {Component} from "@angular/core";
import {InviteCardStyleEntity} from "../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {InviteCardStyleRepository} from "../../repository/InviteCardStyleRepository";
import {InviteCardStyleRESTService} from "../../../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: InviteCardStyleRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: InviteCardStyleRESTService
		},
	]
})
export class InviteCardStyleCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<InviteCardStyleEntity>,
		private m: CRUDGenericModalsHelper,
		private r: InviteCardStyleRepository) {
	}

	getEntities(): InviteCardStyleEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}

export interface InviteCardStyleTableEntity extends InviteCardStyleEntity
{
	localizedTitle: string;
	localizedDescription: string;
}
