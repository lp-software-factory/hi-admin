import {Component, EventEmitter, Output, Input, OnChanges, SimpleChanges} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EventTypeGroupEntity} from "../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {EventTypeGroupFormModel} from "../../Forms/EventTypeGroupForm/index";
import {clone} from "../../../../../common/functions/clone";
import {EventTypeGroupRESTService} from "../../../../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

@Component({
	selector: 'hi-event-type-group-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EditEventTypeGroupModal implements OnChanges
{
	@Input('entity') entity: EventTypeGroupEntity;

	@Output('success') successEvent: EventEmitter<EventTypeGroupEntity> = new EventEmitter<EventTypeGroupEntity>();
	@Output('cancel') cancelEvent: EventEmitter<void> = new EventEmitter<void>();

	private formModel: EventTypeGroupFormModel;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EventTypeGroupRESTService,) {
	}

	ngOnChanges(changes: SimpleChanges): void {
		if(this.entity) {
			this.formModel = {
				url: this.entity.url,
				title: clone(this.entity.title),
				description: clone(this.entity.description),
			};
		}
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.editEventTypeGroup(this.entity.id, this.formModel).subscribe(
			response => {
				this.successEvent.emit(response.event_type_group);

				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		)
	}
}