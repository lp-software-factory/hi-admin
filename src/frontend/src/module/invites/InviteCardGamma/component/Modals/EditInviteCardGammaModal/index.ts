import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardGamma, InviteCardGammaEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {InviteCardGammmaFormModel} from "../../Forms/InviteCardGammaForm/index";
import {InviteCardGammaRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";


@Component({
	selector: 'hi-invite-card-gamma-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditInviteCardGammaModal
{
	@Input('invite_card_gamma') invite_card_gamma: InviteCardGammaEntity;
	@Output('success') successEvent: EventEmitter<InviteCardGamma> = new EventEmitter<InviteCardGamma>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardGammmaFormModel();

	constructor(private service: InviteCardGammaRESTService) {
	}

	ngOnInit() {
		this.initInviteCardGamma();
	}

	initInviteCardGamma() {
		this.formModel.gamma.title = JSON.parse(JSON.stringify(this.invite_card_gamma.title));
		this.formModel.gamma.description = JSON.parse(JSON.stringify(this.invite_card_gamma.description));
	}

	cancel() {
		this.initInviteCardGamma();
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();


		this.service.editInviteCardGamma(this.invite_card_gamma.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.gamma);
			},
			error => {
				loading.is = false;
			}
		);
	}
}