import {Component, EventEmitter, Output, Input} from "@angular/core";

export enum CompanyFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export interface CompanyFormModel
{
	name: string;
}

@Component({
	selector: 'hi-company-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class CompanyForm
{
	@Output() modelChange: EventEmitter<CompanyFormModel> = new EventEmitter<CompanyFormModel>();

	@Input('mode') mode: CompanyFormMode = CompanyFormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: CompanyFormModel;
}