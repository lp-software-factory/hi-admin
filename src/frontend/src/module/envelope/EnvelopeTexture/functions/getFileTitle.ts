import {EnvelopeAdditionalTextureFormModel} from "../component/Forms/EnvelopeAdditionalTextureForm/index";

import {basename} from "../../../common/functions/basename";

export function getEnvelopeTextureFileTitle(type: string, entity: EnvelopeAdditionalTextureFormModel) {
    if(type === 'preview') {
        if(entity.files.preview !== undefined){
            if(entity.files.preview.name !== undefined){
                return entity.files.preview.name.substr(0, entity.files.preview.name.length - (entity.files.preview.name.length - 30));
            }
        } else if(entity.uploaded.preview !== undefined){
            if(entity.uploaded.preview.title !== undefined){
                return entity.uploaded.preview.title.substr(0, entity.uploaded.preview.title.length - (entity.uploaded.preview.title.length - 30));
            }
        }
    }else{
        throw new Error(`Unknown type "${type}"`);
    }
}