import {Component, Output, EventEmitter, ElementRef, ViewChild} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {AuthService} from "../../../service/AuthService";

@Component({
	selector: 'hi-auth-sign-out',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class SignOut
{
	@Output('close') closeEvent: EventEmitter<Boolean> = new EventEmitter<Boolean>();
	@Output('success') successEvent: EventEmitter<Boolean> = new EventEmitter<Boolean>();
	@ViewChild('yesButton') private yesButton: ElementRef;

	private status: LoadingManager = new LoadingManager();

	constructor(private authService: AuthService) {
	}

	ngOnViewInit() {
		this.yesButton.nativeElement.focus();
	}

	yes() {
		let loading = this.status.addLoading();

		this.authService.signOut();
		this.successEvent.emit(true);
	}

	no() {
		this.closeEvent.emit(true);
	}

	cancel() {
		this.closeEvent.emit(true);
	}
}