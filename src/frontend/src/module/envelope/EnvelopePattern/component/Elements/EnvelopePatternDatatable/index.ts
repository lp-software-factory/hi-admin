import {Component, Input} from "@angular/core";
import {EnvelopePatternEntity} from "../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-envelope-pattern-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopePatternDatatable
{
	@Input('entities') entities: EnvelopePatternEntity[] = [];

	constructor(private cursor: DTCursor<EnvelopePatternEntity>) {
	}
}