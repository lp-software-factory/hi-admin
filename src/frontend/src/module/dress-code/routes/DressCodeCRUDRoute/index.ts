import {Component} from "@angular/core";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {DressCodeRepository} from "../../repository/DressCodeRepository";
import {DressCodeEntity} from "../../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {DressCodeRESTService} from "../../../../../definitions/src/services/dress-code/DressCodeRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: DressCodeRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: DressCodeRESTService
		},
	],
})
export class DressCodeCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<DressCodeEntity>,
		private m: CRUDGenericModalsHelper,
		private r: DressCodeRepository) {
	}

	getRows(): DressCodeEntity[] {
		return this.r.getAll();
	}
}