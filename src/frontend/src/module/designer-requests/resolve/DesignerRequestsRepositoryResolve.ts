import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {DesignerRequestsRepository} from "../repository/DesignerRequestsRepository";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {SortCriteriaDirection} from "../../../../definitions/src/definitions/_common/criteria/SortCriteria";

@Injectable()
export class DesignerRequestsRepositoryResolve
{
	public static DEFAULT_LIMIT = 300;

	constructor(private repository: DesignerRequestsRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch({
			criteria: {
				seek: {
					offset: 0,
					limit: DesignerRequestsRepositoryResolve.DEFAULT_LIMIT,
				},
				sort: {
					field: 'id',
					direction: SortCriteriaDirection.Desc,
				},
			}
		});

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}