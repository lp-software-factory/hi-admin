import {Injectable} from "@angular/core";
import {Observable, Observer, ReplaySubject} from "rxjs";
import {Frontend} from "../../../../definitions/src/definitions/frontend/entity/FrontendResponse";
import {FrontendRESTService} from "../../../../definitions/src/services/frontend/FrontendRESTService";

@Injectable()
export class FrontendService
{
	public replay: ReplaySubject<Frontend>;
	private observable: Observable<Frontend>;
	private observer: Observer<Frontend>;

	constructor(private rest: FrontendRESTService) {
		this.replay = new ReplaySubject<Frontend>(1);

		this.observable = Observable.create(observer => {
			this.observer = observer;
		}).share();

		this.observable.subscribe(this.replay);
	}

	fetch(): Observable<Frontend> {
		let observable = this.rest.getFrontend();

		observable.subscribe(
			response => {
				this.observer.next(response);
			}
		);

		return observable;
	}
}