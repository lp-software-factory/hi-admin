import {Component, EventEmitter, Output, Input} from "@angular/core";

import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalBackdropFormModel} from "../../Forms/EnvelopeAdditionalBackdropForm/index";

@Component({
	selector: 'hi-envelope-backdrop-additional-crud-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalEditEnvelopeBackdropModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalBackdropFormModel> = new EventEmitter<EnvelopeAdditionalBackdropFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	
	@Input('entity') formModel: EnvelopeAdditionalBackdropFormModel;

	private status: LoadingManager = new LoadingManager();

	cancel() {
		this.cancelEvent.emit();
	}
	
	submitButtonActive(): boolean {
		if(this.formModel.files !== undefined) {
			return this.formModel.files.resource !== undefined && this.formModel.files.preview !== undefined;
		} else if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.resource !== undefined && this.formModel.uploaded.preview !== undefined
		}
	}

	submit() {
		this.successEvent.emit(this.formModel);
	}
}