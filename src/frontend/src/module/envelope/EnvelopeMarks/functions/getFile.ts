import {EnvelopeAdditionalMarksFormModel} from "../component/Forms/EnvelopeAdditionalMarksForm/index";

export function getFile(entity: EnvelopeAdditionalMarksFormModel) {
    if(entity.uploaded.attachment !== undefined){
        if(entity.uploaded.attachment.link !== undefined){
            return entity.uploaded.attachment.link.url;
        } 
    }
}