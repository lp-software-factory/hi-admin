import {CardGammaSelector} from "./component/Elements/CardGammaSelector/index";
import {InviteCardGammaCRUDRoute} from "./routes/InviteCardGammaCRUDRoute/index";
import {CreateInviteCardGammaModal} from "./component/Modals/CreateInviteCardGammaModal/index";
import {EditInviteCardGammaModal} from "./component/Modals/EditInviteCardGammaModal/index";
import {DeleteInviteCardGammaModal} from "./component/Modals/DeleteInviteCardGammaModal/index";
import {InviteCardGammaRepository} from "./repository/InviteCardGammaRepository";
import {InviteCardGammaRepositoryResolve} from "./resolve/InviteCardGammaRepositoryResolve";
import {InviteCardGammmaForm} from "./component/Forms/InviteCardGammaForm/index";
import {InviteCardGammaDatatable} from "./component/Elements/InviteCardGammaDatatable/index";
import {InviteCardGammaRESTService} from "../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";

export const HIInviteCardGammaModule = {
	routes: [
		InviteCardGammaCRUDRoute,
	],
	declarations: [
		CardGammaSelector,
		InviteCardGammmaForm,
		InviteCardGammaDatatable,
		CreateInviteCardGammaModal,
		EditInviteCardGammaModal,
		DeleteInviteCardGammaModal
	],
	providers: [
		InviteCardGammaRepository,
		InviteCardGammaRepositoryResolve
	],
};

export const HIInviteCardGammaRoute = {
	path: 'gammas',
	component: InviteCardGammaCRUDRoute,
	pathMatch: 'full',
	resolve: [
		InviteCardGammaRepositoryResolve
	]
};