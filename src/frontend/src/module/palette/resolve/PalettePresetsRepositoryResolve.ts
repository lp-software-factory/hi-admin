import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {PalettePresetsRepository} from "../repository/PalettePresetsRepository";

@Injectable()
export class PalettePresetsRepositoryResolve implements Resolve<any>
{
	constructor(private repository: PalettePresetsRepository) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		return this.repository.fetch();
	}
}