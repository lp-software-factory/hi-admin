import {Component, EventEmitter, Output, Input} from "@angular/core";
import {ProductsPackageEntity} from "../../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {EditProductsPackageRequest} from "../../../../../../definitions/src/definitions/products-package/paths/edit";
import {LocalizedString} from "../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {PalettePresetsRepository} from "../../../../palette/repository/PalettePresetsRepository";
import {ProductsPackageRESTService} from "../../../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Component({
	selector: 'hi-products-package-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})

export class EditProductsPackageModal
{
	@Input('products-package') products_package: ProductsPackageEntity;
	@Input('repository') repository: any;

	@Output('success') successEvent: EventEmitter<ProductsPackageEntity> = new EventEmitter<ProductsPackageEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();


	private status: LoadingManager = new LoadingManager();
	private model: EditProductsPackageRequest = new EditProductsPackageRequest();

	constructor(private service: ProductsPackageRESTService,
		private palettes: PalettePresetsRepository) {
	}

	ngOnInit() {
		this.model = {
			title: Array<LocalizedString>(),
			description: Array<LocalizedString>(),
			palette: undefined,
			is_activated: false,
			base_price: {
				currency: '',
				amount: 0
			}
		};

		this.initProduct();
	}

	initProduct() {
		this.model.title = JSON.parse(JSON.stringify(this.products_package.title));
		this.model.description = JSON.parse(JSON.stringify(this.products_package.description));
		this.model.is_activated = this.products_package.is_activated;
		this.model.base_price.currency = this.products_package.base_price.currency.code;
		this.model.base_price.amount = this.products_package.base_price.amount;
		this.model.palette = JSON.parse(JSON.stringify(this.products_package.palette));
	}

	cancel() {
		this.initProduct();
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();


		this.service.editProductsPackage(this.products_package.id, this.model).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.products_package);
			},
			error => {
				loading.is = false;
			}
		);
	}
}