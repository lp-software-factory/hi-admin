import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardStyleEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {InviteCardStyleFormModel} from "../../Forms/InviteCardStyleForm/index";
import {InviteCardStyleRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";

@Component({
	selector: 'hi-invite-card-style-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateInviteCardStyleModal
{
	@Output('success') successEvent: EventEmitter<InviteCardStyleEntity> = new EventEmitter<InviteCardStyleEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardStyleFormModel();

	constructor(private service: InviteCardStyleRESTService) {
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createInviteCardStyle(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.style);
			},
			error => {
				loading.is = false;
			}
		);
	}
}