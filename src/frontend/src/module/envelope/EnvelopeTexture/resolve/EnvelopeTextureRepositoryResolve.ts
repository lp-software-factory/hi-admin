import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {EnvelopeTextureRepository} from "../repository/EnvelopeTextureRepository";
import {EnvelopeTextureGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/texture/path/get-all";

@Injectable()
export class EnvelopeTextureRepositoryResolve implements Resolve<EnvelopeTextureGetAllResponse200>
{
	constructor(private repository: EnvelopeTextureRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}