import {Component, EventEmitter, Output, ViewChild} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopePatternEntity, EnvelopePattern} from "../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopePatternFormModel, EnvelopePatternForm} from "../../Forms/EnvelopePatternForm/index";
import {CreateEnvelopePatternRequest} from "../../../../../../../definitions/src/definitions/envelope/pattern/path/create";
import {Observable} from "rxjs/Observable";
import {UploadAttachmentResponse200} from "../../../../../../../definitions/src/definitions/attachment/entity/paths/upload";
import {EnvelopePatternRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";

@Component({
	selector: 'hi-envelope-pattern-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateEnvelopePatternModal
{
	@Output('success') successEvent: EventEmitter<EnvelopePattern> = new EventEmitter<EnvelopePattern>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	
	@ViewChild('envelopePatternForm') envelopePatternForm: EnvelopePatternForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopePatternFormModel();

	constructor(private service: EnvelopePatternRESTService,
			private attachmentService: AttachmentRESTService,
			private l: LoadingManager,
	) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 &&
				this.formModel.preview_file !== undefined && this.formModel.texture_file !== undefined &&
			    !this.status.isLoading();
	}
	
	submit($event) {
		$event.preventDefault();
		
		this.envelopePatternForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();
			
			this.service.createEnvelopePattern(request).subscribe(
				response => {
					loading.is = false;
					this.successEvent.emit(response.envelope_pattern);
				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}