import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EnvelopeBackdropRepository} from "../../repository/EnvelopeBackdropRepository";
import {EnvelopeBackdropEntity} from "../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {EnvelopeBackdropRESTService} from "../../../../../../definitions/src/services/envelope/envelope-backdrop/EnvelopeBackdropRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EnvelopeBackdropRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EnvelopeBackdropRESTService
		},
	]
})

export class EnvelopeBackdropCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EnvelopeBackdropEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EnvelopeBackdropRepository) {
	}

	getEntities(): EnvelopeBackdropEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}
