import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {LocaleService} from "../../../../../locale/service/LocaleService";
import {InviteCardStyleEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";
import {InviteCardStyleRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";


@Component({
	selector: 'hi-invite-card-style-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteInviteCardStyleModal implements AfterViewInit
{
	@Input('invite_card_style') invite_card_style: InviteCardStyleEntity;
	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: InviteCardStyleRESTService, private localeService: LocaleService) {
	}


	getInviteCardStyleName(): string {
		return this.localeService.getLocalization(this.invite_card_style.title).value;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let inviteCardStyleId = this.invite_card_style.id;
		let loading = this.status.addLoading();

		this.service.deleteInviteCardStyle(inviteCardStyleId).subscribe(() => {
			this.successEvent.emit(inviteCardStyleId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}