import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {DressCodeEntity} from "../../../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {TranslationService} from "../../../../i18n/service/TranslationService";

@Component({
	selector: 'hi-dress-code-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DressCodeDataTable
{
	@Input('entities') entities: DressCodeEntity[] = [];

	constructor(
		private cursor: DTCursor<DressCodeEntity>,
		private t: TranslationService
	) {}

	hasAttachment(entity) {
		return entity.image !== undefined;
	}
}