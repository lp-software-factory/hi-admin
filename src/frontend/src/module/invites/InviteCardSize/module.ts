import {InviteCardSizeCRUDRoute} from "./routes/InviteCardSizeCRUDRoute/index";
import {CreateInviteCardSizeModal} from "./component/Modals/CreateInviteCardSizesModal/index";
import {DeleteInviteCardSizeModal} from "./component/Modals/DeleteInviteCardSizesModal/index";
import {EditInviteCardSizeModal} from "./component/Modals/EditInviteCardSizesModal/index";
import {CardSizeSelector} from "./component/Elements/CardSizeSelector/index";
import {InviteCardSizeRepositoryResolve} from "./resolve/InviteCardSizeRepositoryResolve";
import {InviteCardSizeDatatable} from "./component/Elements/InviteCardSizeDatatable/index";
import {InviteCardSizeRepository} from "./repository/InviteCardSizeRepository";
import {InviteCardSizeForm} from "./component/Forms/InviteCardSizeForm/index";
import {InviteCardSizeRESTService} from "../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";

export const HIInviteCardSizeModule = {
	routes: [
		InviteCardSizeCRUDRoute,
	],
	declarations: [
		InviteCardSizeDatatable,
		InviteCardSizeForm,
		CreateInviteCardSizeModal,
		DeleteInviteCardSizeModal,
		EditInviteCardSizeModal,
		CardSizeSelector
	],
	providers: [
		InviteCardSizeRepository,
		InviteCardSizeRepositoryResolve
	],
};

export const HIInviteCardSizeRoute = {
	path: 'sizes',
	component: InviteCardSizeCRUDRoute,
	pathMatch: 'full',
	resolve: [
		InviteCardSizeRepositoryResolve
	]
};