import {EnvelopePatternCRUDRoute} from "./routes/EnvelopePatternCRUDRoute/index";
import {EnvelopePatternForm} from "./component/Forms/EnvelopePatternForm/index";
import {EnvelopePatternDatatable} from "./component/Elements/EnvelopePatternDatatable/index";
import {CreateEnvelopePatternModal} from "./component/Modals/CreateEnvelopePatternModal/index";
import {EditEnvelopePatternModal} from "./component/Modals/EditEnvelopePatternModal/index";
import {DeleteEnvelopePatternModal} from "./component/Modals/DeleteEnvelopePatternModal/index";
import {EnvelopePatternRepository} from "./repository/EnveloperPatternRepository";
import {EnvelopePatternRepositoryResolve} from "./resolve/EnvelopePatternRepositoryResolve";
import {EnvelopePatternRESTService} from "../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";
import {AttachmentRESTService} from "../../../../definitions/src/services/attachment/AttachmentRESTService";

export const HIEnvelopePatternModule = {
	routes: [
		EnvelopePatternCRUDRoute,
	],
	declarations: [
		EnvelopePatternForm,
		EnvelopePatternDatatable,
		CreateEnvelopePatternModal,
		EditEnvelopePatternModal,
		DeleteEnvelopePatternModal
	],
	providers: [
		EnvelopePatternRepository,
		EnvelopePatternRepositoryResolve
	],
};

export const HIEnvelopePatternRoute = {
	path: 'pattern',
	component: EnvelopePatternCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EnvelopePatternRepositoryResolve
	]
};