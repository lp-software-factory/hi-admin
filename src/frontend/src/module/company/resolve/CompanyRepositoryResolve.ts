import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {CompanyRepository} from "../repository/CompanyRepository";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {ListCompanyResponse200} from "../../../../definitions/src/definitions/company/paths/list";

@Injectable()
export class CompanyRepositoryResolve implements Resolve<ListCompanyResponse200>
{
	constructor(private repository: CompanyRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}