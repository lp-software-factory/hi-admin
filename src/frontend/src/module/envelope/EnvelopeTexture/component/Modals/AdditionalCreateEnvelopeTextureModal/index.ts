import {Component, EventEmitter, Output, ViewChild, Input} from "@angular/core";

import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalTextureFormModel, EnvelopeAdditionalTextureForm} from "../../Forms/EnvelopeAdditionalTextureForm/index";

@Component({
	selector: 'hi-envelope-texture-additional-crud-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalCreateEnvelopeTextureModal
{
	@ViewChild('definitionForm') definitionForm: EnvelopeAdditionalTextureForm;

	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalTextureFormModel> = new EventEmitter<EnvelopeAdditionalTextureFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: EnvelopeAdditionalTextureFormModel = new EnvelopeAdditionalTextureFormModel();

	cancel() {
		this.cancelEvent.emit()
	}
	
	submitButtonActive(): boolean {
		if(this.formModel.files !== undefined) {
			return this.formModel.files.preview !== undefined;
		} else if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.preview !== undefined
		}
	}

	submit() {
		this.successEvent.emit(this.formModel);
	}
}