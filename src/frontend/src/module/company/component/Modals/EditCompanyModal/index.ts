import {Component, EventEmitter, Output, Input, OnChanges, SimpleChanges} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CompanyEntity} from "../../../../../../definitions/src/definitions/company/entity/CompanyEntity";
import {CompanyFormModel} from "../../Forms/CompanyForm/index";
import {CompanyRESTService} from "../../../../../../definitions/src/services/company/CompanyRESTService";

@Component({
	selector: 'hi-company-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditCompanyModal implements OnChanges
{
	@Input('entity') company: CompanyEntity;

	@Output('success') successEvent: EventEmitter<CompanyEntity> = new EventEmitter<CompanyEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: CompanyFormModel = {
		name: '',
	};

	constructor(private service: CompanyRESTService) {
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.formModel = {
			name: this.company.name,
		};
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.editCompany(this.company.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.company);
			},
			error => {
				loading.is = false;
			}
		);
	}
}