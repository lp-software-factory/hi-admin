import {Component, Input} from "@angular/core";
import {InviteCardGammaEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-invite-card-gamma-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class InviteCardGammaDatatable
{
	@Input('entities') entities: InviteCardGammaEntity[] = [];

	constructor(private cursor: DTCursor<InviteCardGammaEntity>) {
	}
}