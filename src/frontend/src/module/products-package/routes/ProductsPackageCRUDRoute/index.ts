import {Component} from "@angular/core";
import {ProductsPackageEntity} from "../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {ProductsPackageRepository} from "../../repository/ProductsPackageRepository";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDPositionEntityHelper} from "../../../crud/helpers/position/helper";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {PalettePresetsRepository} from "../../../palette/repository/PalettePresetsRepository";
import {CurrencyRepository} from "../../../currency/repository/CurrencyRepository";
import {ProductsPackageRESTService} from "../../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: ProductsPackageRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: ProductsPackageRESTService
		},
	]
})
export class ProductsPackageCRUDRoute
{
	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<ProductsPackageEntity>,
		private m: CRUDGenericModalsHelper,
		private r: ProductsPackageRepository,
		private rCurrencies: CurrencyRepository,
		private rColors: PalettePresetsRepository,) {
	}

	getAdditionalRepository() {
		return {
			colors: this.rColors.getAll(),
			currencies: this.rCurrencies.getAll()
		};
	}

	getRows(): ProductsPackageEntity[] {
		return this.r.getAll();
	}
}
