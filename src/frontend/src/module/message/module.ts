import {MessageNotifications} from "./component/MessageNotifications/index";
import {MessageService} from "./service/MessageService";

export const HIMessageModule = {
	declarations: [
		MessageNotifications,
	],
	providers: [
		MessageService,
	],
};