import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardGamma} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {InviteCardGammmaFormModel} from "../../Forms/InviteCardGammaForm/index";
import {InviteCardGammaRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";

@Component({
	selector: 'hi-invite-card-gamma-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateInviteCardGammaModal
{
	@Output('success') successEvent: EventEmitter<InviteCardGamma> = new EventEmitter<InviteCardGamma>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardGammmaFormModel();

	constructor(private service: InviteCardGammaRESTService) {
	}

	checkRequiredFields(): boolean {
		return this.formModel.gamma.title.length > 0 && this.formModel.gamma.description.length > 0;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createInviteCardGamma(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.gamma);
			},
			error => {
				loading.is = false;
			}
		);
	}
}