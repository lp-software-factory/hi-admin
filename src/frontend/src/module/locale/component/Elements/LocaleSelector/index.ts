import {Component, EventEmitter, Output} from "@angular/core";
import {LocaleService} from "../../../service/LocaleService";
import {LocaleEntity} from "../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";

@Component({
	selector: 'hi-locale-selector',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class LocaleSelector
{
	@Output('changeLocaleRegion') changeLocaleRegion: any = new EventEmitter<any>();

	private locales: Array<LocaleEntity> = JSON.parse(JSON.stringify(this.localeService.locales));
	private chosenRegion: string = this.localeService.current.region;

	constructor(private localeService: LocaleService) {
	}

	getFlag() {
		return this.chosenRegion.split('_')[1].toUpperCase();
	}

	changeValue(event) {
		for(let i = 0; i <= this.locales.length; i++) {
			if(event === this.locales[i].region) {
				return this.changeLocaleRegion.emit(this.locales[i].id);
			}
		}
	}
}