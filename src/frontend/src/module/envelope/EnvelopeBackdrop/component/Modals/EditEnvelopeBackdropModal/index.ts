import {Component, EventEmitter, Output, Input, ViewChild} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeBackdropFormModel, EnvelopeBackdropForm} from "../../Forms/EnvelopeBackdropForm/index";
import {EnvelopeBackdropEntity} from "../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {EnvelopeAdditionalBackdropFormModel} from "../../Forms/EnvelopeAdditionalBackdropForm/index";
import {clone} from "../../../../../common/functions/clone";
import {EnvelopeBackdropRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-backdrop/EnvelopeBackdropRESTService";
import {EnvelopeBackdropDefinition} from "../../../../../../../definitions/src/definitions/envelope/backdrop-definition/entity/EnvelopeBackdropDefinition";

@Component({
	selector: 'hi-envelope-backdrop-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor
	]
})
export class EditEnvelopeBackdropModal
{
	@Input('envelope_backdrop') envelope_backdrop: EnvelopeBackdropEntity;

	@Output('success') successEvent: EventEmitter<EnvelopeBackdropEntity> = new EventEmitter<EnvelopeBackdropEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('envelopeBackdropForm') envelopeBackdropForm: EnvelopeBackdropForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeBackdropFormModel();

	constructor(
		private service: EnvelopeBackdropRESTService,
		private l: LoadingManager,
		private m: CRUDGenericModalsHelper,
		private cursor: DTCursor<EnvelopeBackdropDefinition>,
	) {}

	ngOnInit() {
		this.formModel.title = clone(this.envelope_backdrop.title);
		this.formModel.description = clone(this.envelope_backdrop.description);
		this.formModel.definitions = this.envelope_backdrop.definitions.map(definition => {
			return clone({
				title: definition.entity.title,
				description: definition.entity.description,
				files: {
					preview: undefined,
					resource: undefined
				},
				uploaded: {
					preview: definition.entity.preview,
					resource: definition.entity.resource
				}
			});
		});
	}

	checkChanges(): boolean {
		return JSON.stringify(this.formModel.title) !== JSON.stringify(this.envelope_backdrop.title) ||
			    JSON.stringify(this.formModel.description) !== JSON.stringify(this.envelope_backdrop.description) ||
				JSON.stringify(this.formModel.definitions) !== JSON.stringify(this.envelope_backdrop.definitions);
	}

	isSubmitButtonActive(): boolean {
		return this.checkRequiredFields() && this.checkChanges();
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && this.formModel.definitions.length > 0 && ! this.status.isLoading();
	}

	addDefinition(entity: EnvelopeAdditionalBackdropFormModel) {
		this.formModel.definitions.push(entity);
	}

	replaceDefinition(entity: EnvelopeAdditionalBackdropFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			return compare; // TODO: Fix or remove
		})
	}

	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(backdrop => JSON.stringify(backdrop) !== JSON.stringify(this.cursor.current()));
	}

	submit($event) {
		$event.preventDefault();

		this.envelopeBackdropForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();

			this.service.editEnvelopeBackdrop(this.envelope_backdrop.id, request).subscribe(
				response => {
					loading.is = false;

					this.successEvent.emit(response.envelope_backdrop.entity);
				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}