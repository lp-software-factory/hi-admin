export class CRUDCursor<T>
{
	private _current: T;

	current(): T {
		if(!this.has()) {
			throw new Error('Nothing selected');
		}

		return this._current;
	}

	blur(): void {
		this._current = undefined;
	}

	select(entity: T) {
		this._current = entity;
	}

	has(): boolean {
		return this._current !== undefined;
	}
}