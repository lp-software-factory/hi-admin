import {Component} from "@angular/core";
import {UIGlobalLoadingIndicatorService} from "../../service/UIGlobalLoadingIndicatorService";

@Component({
	selector: 'hi-ui-loading',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class UILoading
{
	constructor(private service: UIGlobalLoadingIndicatorService) {
	}
}