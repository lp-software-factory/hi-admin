import {Component} from "@angular/core";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {LocaleRepository} from "../../repository/LocaleRepository";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {LocaleEntity} from "../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {LocaleRESTService} from "../../../../../definitions/src/services/locale/LocaleRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: LocaleRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: LocaleRESTService
		},
	],
})
export class LocaleCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<LocaleEntity>,
		private m: CRUDGenericModalsHelper,
		private r: LocaleRepository) {
	}

	getRows(): LocaleEntity[] {
		return this.r.getAll();
	}
}