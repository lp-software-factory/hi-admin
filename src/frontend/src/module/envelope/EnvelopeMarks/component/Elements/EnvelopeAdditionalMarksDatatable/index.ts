import {Component, Input} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeAdditionalMarksFormModel} from "../../Forms/EnvelopeAdditionalMarksForm/index";
import {getFile} from "../../../functions/getFile";

@Component({
	selector: 'hi-envelope-additional-marks-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeAdditionalMarksCrudDataTable
{
	@Input('entities') entities: EnvelopeAdditionalMarksFormModel[] = [];

	constructor(private cursor: DTCursor<EnvelopeAdditionalMarksFormModel>)
	{}
	
	getFile(entity: EnvelopeAdditionalMarksFormModel) {
		return getFile(entity);
	}
	
	hasAttachment(entity) {
		return entity.uploaded.attachment !== undefined
	}
}