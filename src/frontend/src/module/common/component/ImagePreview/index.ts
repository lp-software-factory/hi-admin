import {Component, Input, Output, ViewChild, EventEmitter, ElementRef, OnInit, OnDestroy} from "@angular/core";

import Timer = NodeJS.Timer;

@Component({
    selector: 'hi-image-preview',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ]
})
export class ImagePreview implements OnInit, OnDestroy
{
    static PADDING = 15;

    @Input('image') image: string;
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();
    @ViewChild('screen') screen: ElementRef;

    private interval: Timer;
    private imgSizes: { width: number, height: number };

    ngOnInit(): void {
        this.interval = setInterval(() => {
            this.updateContainer();
        }, 1000);
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    updateContainer() {
        let containerSizes: { width: number, height: number } = {
            width: this.screen.nativeElement.clientWidth,
            height: this.screen.nativeElement.clientHeight,
        };
    }

    close() {
        this.closeEvent.emit();
    }
}