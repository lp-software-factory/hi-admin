export function updatePosition(rows: Array<any>, oldPosition, newPosition) {
	let tempRow;

	for(let index = 0; index < rows.length; index++) {
		if(rows[index].position === newPosition) {
			tempRow = rows[index];
			for(let ind = 0; ind < rows.length; ind++) {
				if(rows[ind].position === oldPosition) {
					tempRow.position = rows[ind].position;
					rows[ind].position = newPosition;
					break;
				}
			}
			break;
		}
	}
}