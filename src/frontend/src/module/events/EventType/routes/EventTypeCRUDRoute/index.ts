import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EventTypeRepository} from "../../repository/EventTypeRepository";
import {EventTypeEntity} from "../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {ActivatedRoute} from "@angular/router";
import {EventTypeRESTService} from "../../../../../../definitions/src/services/event/event-type/EventTypeRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EventTypeRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EventTypeRESTService
		},
	]
})
export class EventTypeCRUDRoute
{
	private currentGroupId: number;

	private help: string = require('./inline.help.md');

	constructor(private route: ActivatedRoute,
		private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EventTypeEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EventTypeRepository) {
		this.route.params.subscribe(params => {
			this.currentGroupId = parseInt(params['eventGroupTypeId'], 10);
		});
	}

	getRows(): EventTypeEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}