import {CreateOAuth2ProvidersModal} from "./component/Modals/CreateOAuth2ProviderModal/index";
import {DeleteOAuth2ProvidersModal} from "./component/Modals/DeleteOAuth2Modal/index";
import {OAuth2ProvidersCRUDRoute} from "./routes/OAuth2ProvidersCRUDRoute/index";
import {OAuth2ProvidersDataTable} from "./component/Elements/OAuth2ProvidersDatatable/index";
import {OAuth2ProviderHandlersRepository} from "./repository/OAuth2ProviderHandlersRepository";
import {OAuth2ProviderForm} from "./component/Forms/OAuth2ProviderForm/index";
import {OAuth2ProviderRepository} from "./repository/OAuth2ProvidersRepository";
import {OAuth2ProviderHandlersRepositoryResolve} from "./resolve/OAuth2ProviderHandlersRepositoryResolve";
import {OAuth2ProvidersRepositoryResolve} from "./resolve/OAuth2ProvidersRepositoryResolve";
import {EditOAuth2ProvidersModal} from "./component/Modals/EditOAuth2ProviderModal/index";
import {OAuth2ProvidersRESTService} from "../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

export const HIOAuth2ProvidersModule = {
	routes: [
		OAuth2ProvidersCRUDRoute,
	],
	declarations: [
		OAuth2ProviderForm,
		OAuth2ProvidersDataTable,
		CreateOAuth2ProvidersModal,
		EditOAuth2ProvidersModal,
		DeleteOAuth2ProvidersModal,
	],
	providers: [
		OAuth2ProviderHandlersRepository,
		OAuth2ProviderRepository,
		OAuth2ProviderHandlersRepositoryResolve,
		OAuth2ProvidersRepositoryResolve,
	],
};

export const HIOAuth2ProvidersRoute = {
	path: 'oauth2-providers',
	component: OAuth2ProvidersCRUDRoute,
	pathMatch: 'full',
	resolve: [
		OAuth2ProviderHandlersRepositoryResolve,
		OAuth2ProvidersRepositoryResolve,
	]
};