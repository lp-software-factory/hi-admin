import {Observable} from "rxjs";
import {clone} from "./clone";

export interface UploadObservableProgress
{
    progress: number;
    loaded: number;
    total: number;
}

export interface UploadObservableFactoryOptions
{
    url: string;
    method?: string;
    file: Blob;
}

export class UploadObservableFactory<T>
{
    public create(options: UploadObservableFactoryOptions): Observable<UploadObservableProgress|T> {
        if(! options.method) options.method = 'POST';

        return Observable.create(observer => {
            let xhrRequest = new XMLHttpRequest();
            let formData = new FormData();

            formData.append("file", options.file);

            xhrRequest.open(options.method, options.url);
            xhrRequest.send(formData);

            xhrRequest.onprogress = (e) => {
                if(e.lengthComputable) {
                    let progress: UploadObservableProgress = {
                        progress: Math.floor((e.loaded / e.total) * 100),
                        total: e.total,
                        loaded: e.loaded,
                    };

                    observer.next(progress);
                }
            };

            xhrRequest.onreadystatechange = () => {
                if(xhrRequest.readyState === 4) {
                    try {
                        let json = JSON.parse(xhrRequest.response);

                        if(xhrRequest.status === 200) {
                            observer.next(json);
                            observer.complete();
                        } else {
                            observer.error(json);
                        }
                    } catch(e) {
                        observer.error('Failed to parse JSON');
                    }
                }
            };
        });
    }
}