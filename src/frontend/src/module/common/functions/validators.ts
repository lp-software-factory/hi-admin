import {LocalizedString} from "../../../../definitions/src/definitions/locale/definitions/LocalizedString";

var trim = require('trim');

export class Validators {
    static isId(input: any): boolean {
        console.log('Validators.isId', input);

        return ! isNaN(input) && (typeof input === "number") && input > 0;
    }

    static withId(input: any): boolean {
        console.log('Validators.withId', input);

        return (typeof input === "object")
            && (! isNaN(input.id))
            && (typeof input.id === "number")
            && (Math.ceil(input.id) === input.id)
            && input.id > 0;
    }

    static localeInputNotEmpty(input: LocalizedString[]): boolean {
        console.log('Validators.localeInputNotEmpty', input);

        return (input instanceof Array)
            && (input.length > 0)
            && (input.map(e => e.value).filter(v => ((typeof v !== "string") || (v.length === 0))).length === 0);
    }

    static isHexCode(input: string): boolean {
        console.log('Validators.isHexCode', input);

        return (typeof input === "string")
            && !!input.match(/^\#[abcdefABCDEF0-9]{6}$/);
    }

    static isPositiveNumber(input: number): boolean {
        console.log('Validators.isPositiveNumber', input);

        return (typeof input === "number")
            && (input > 0)
            && (Math.ceil(input) === input);
    }

    static isPositiveOrZeroNumber(input: number): boolean {
        console.log('Validators.isPositiveOrZeroNumber', input);

        return (typeof input === "number")
            && (input >= 0)
            && (Math.ceil(input) === input);
    }

    static stringNotEmpty(input: string): boolean {
        console.log('Validators.stringNotEmpty', input);

        return (typeof input === "string")
            && trim(input).length > 0;
    }
}