import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {ProductsPackageEntity} from "../../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {ProductsPackageRepository} from "../../../repository/ProductsPackageRepository";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {ProductEntity} from "../../../../../../definitions/src/definitions/products/entity/ProductEntity";
import {ProductRepository} from "../../../../products/repository/ProductRepository";
import {SetProductsForPackageRequest} from "../../../../../../definitions/src/definitions/products-package/paths/set-products";
import {ProductsPackageRESTService} from "../../../../../../definitions/src/services/products-package/ProductsPackageRESTService";

@Component({
	selector: 'hi-products-package-set-product-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
@Component({
	selector: 'hi-set-products-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: ProductsPackageRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: ProductsPackageRESTService
		},
	]
})

export class SetPackagesModal
{
	@Input('products-package') products_package: ProductsPackageEntity;

	@Output('success') successEvent: EventEmitter<ProductsPackageEntity> = new EventEmitter<ProductsPackageEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private tempProducts: Array<ProductEntity>;

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<ProductEntity>,
		private r: ProductRepository,
		private service: ProductsPackageRESTService,) {
	}

	ngOnInit() {
		this.tempProducts = JSON.parse(JSON.stringify(this.products_package.products));
	}

	checkChanges() {
		return JSON.stringify(this.tempProducts) !== JSON.stringify(this.products_package.products);
	}

	addProduct() {
		this.products_package.products.push(this.c.getCurrentEntity());
	}

	removeButtonActive(): boolean {
		if(this.c.hasActiveEntity())
			return this.transformProductsToIds(this.products_package.products).includes(this.c.getCurrentEntity().id);
		else return this.c.hasActiveEntity();
	}

	addButtonActive() {
		if(this.c.hasActiveEntity())
			return !this.transformProductsToIds(this.products_package.products).includes(this.c.getCurrentEntity().id);
		else return this.c.hasActiveEntity();
	}

	removeProduct() {
		this.products_package.products = JSON.parse(JSON.stringify(this.products_package.products.filter(entity => {
			return entity.id !== this.c.getCurrentEntity().id
		})));
	}

	getAddedProducts(): ProductEntity[] {
		return this.products_package.products;
	}

	getAllProducts(): ProductEntity[] {
		return this.r.getAll();
	}

	transformProductsToIds(products: Array<ProductEntity>): Array<number> {
		return products.map(product => {
			return product.id;
		});
	}

	getNotAddedProducts(): ProductEntity[] {
		return this.getAllProducts().filter(entity => {
			return !this.transformProductsToIds(this.getAddedProducts()).includes(entity.id);
		});
	}

	submit() {
		let request: SetProductsForPackageRequest = {
			product_ids: this.transformProductsToIds(this.getAddedProducts())
		};

		this.service.setProductsForPackage(this.products_package.id, request).subscribe(success => {
			this.successEvent.emit();
			this.l.notLoading();
		})
	}

	cancel() {
		this.cancelEvent.emit();
	}

}