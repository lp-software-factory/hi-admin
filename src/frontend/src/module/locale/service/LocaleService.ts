import {Injectable} from "@angular/core";

import {LocaleEntity} from "../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {FrontendService} from "../../frontend/service/FrontendService";
import {LocalizedString} from "../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {LocaleRESTService} from "../../../../definitions/src/services/locale/LocaleRESTService";

@Injectable()
export class LocaleService
{
	private fallback: string[] = ['en_US', 'en_GB'];

	public current: LocaleEntity;
	public locales: LocaleEntity[] = [];

	constructor(
		private frontend: FrontendService,
		private service: LocaleRESTService,
		private loading: UIGlobalLoadingIndicatorService
	) {
		frontend.replay.subscribe(response => {
			this.current = response.locale.current;
			this.locales = response.locale.available;
		});
	}

	public registerLocal(locale: LocaleEntity) {
		this.locales.push(locale);
	}

	public unregisterLocale(localeId: number) {
		if(this.locales.length <= 1) {
			console.log('Warning', 'No way to remove the last locale');
		} else {
			this.locales = this.locales.filter(compare => {
				return compare.id !== localeId;
			});

			if(this.current.id === localeId) {
				this.current = this.locales[0];
			}
		}
	}

	getLocaleByRegion(region: string): LocaleEntity {
		let result = this.locales.filter(compare => compare.region === region);

		if(result.length === 0) {
			throw new Error(`Locale with region "${region}" not found`);
		}

		return result[0];
	}

	public changeCurrentLocale(region) {
		let loading = this.loading.createLoadingIndicator();

		this.service.setCurrentLocale(region).subscribe(entity => {
			this.current = entity.locale;
			loading.is = false;
			location.reload();
		})
	}

	public getLocalization(input: LocalizedString[]): LocalizedString {
		if(input.length === 0) {
			return {
				region: 'en_GB',
				value: ''
			};
		}
		for(let i = 0; i < input.length; i++) {
			if(input[i].region === this.current.region)
				return input[i];
		}


		for(let fallback of this.fallback) {
			if(input.hasOwnProperty(fallback)) {
				return input[fallback];
			}
		}
		return input[0];
	}
}