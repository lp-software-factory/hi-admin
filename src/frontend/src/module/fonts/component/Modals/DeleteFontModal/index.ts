import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {TranslationService} from "../../../../i18n/service/TranslationService";
import {FontRESTService} from "../../../../../../definitions/src/services/font/FontRESTService";
import {FontEntity, Font} from "../../../../../../definitions/src/definitions/font/entity/Font";

@Component({
	selector: 'hi-font-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class DeleteFontModal implements AfterViewInit
{
	@Input('entity') font: Font;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: FontRESTService) {
	}
	
	getFontTitle(): string {
		return this.font.entity.title;
	}
	
	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let fontId = this.font.entity.id;
		let loading = this.status.addLoading();

		this.service.delete(fontId).subscribe(() => {
			this.successEvent.emit(fontId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}