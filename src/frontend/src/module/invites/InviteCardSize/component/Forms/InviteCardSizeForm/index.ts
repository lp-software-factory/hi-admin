import {Component, EventEmitter, Output, Input} from "@angular/core";
import {CreateInviteCardSizeRequest} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/paths/create";

export enum InviteCardSizeFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class InviteCardSizeFormModel implements CreateInviteCardSizeRequest
{
	title = [];
	description = [];
	width = 0;
	height = 0;
}

@Component({
	selector: 'hi-invite-card-size-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class InviteCardSizeForm
{
	@Output() modelChange: EventEmitter<InviteCardSizeFormModel> = new EventEmitter<InviteCardSizeFormModel>();

	@Input('mode') mode: InviteCardSizeFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: InviteCardSizeFormModel;
}