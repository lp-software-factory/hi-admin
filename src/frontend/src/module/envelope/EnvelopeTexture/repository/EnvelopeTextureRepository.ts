import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EnvelopeTextureEntity} from "../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {EnvelopeTextureGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/texture/path/get-all";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {EnvelopeTextureRESTService} from "../../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";

@Injectable()
export class EnvelopeTextureRepository implements CRUDSerialRepositoryInterface
{
	private textures: EnvelopeTextureEntity[] = [];

	constructor(private rest: EnvelopeTextureRESTService) {
	}

	fetch(): Observable<EnvelopeTextureGetAllResponse200> {

		let observable = this.rest.getAllEnvelopeTexture();

		observable.subscribe(response => {
			this.textures = response.envelope_textures.map(entity => {
				return entity.entity
			});
		});

		return observable;
	}

	getAll(): EnvelopeTextureEntity[] {
		return this.textures;
	}

	getById(id: number): EnvelopeTextureEntity {
		let result = this.textures.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Marks with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.textures.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.textures, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.textures, oldPosition, newPosition);
	}

	add(element: EnvelopeTextureEntity) {
		this.textures.push(element);
	}

	updateAllPositions(position: number) {
		this.textures.forEach(textures => {
			if(textures.position > position){
				textures.position += -1;
			}
		});
	}

	remove(id: number) {
		this.textures.forEach(textures => {
			if(textures.id === id) {
				this.updateAllPositions(textures.position);
			}
		});

		this.textures = this.textures.filter(entity => entity.id !== id);
	}

	replace(element: EnvelopeTextureEntity) {
		this.textures = this.textures.map(entity => {
			if(entity.id === element.id) {
				return element;
			} else {
				return entity;
			}
		})
	}
}