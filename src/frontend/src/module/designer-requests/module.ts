import {DesignerRequestsRoute} from "./routes/DesignerRequestsRoute/index";
import {ViewDesignerRequestModal} from "./component/Modals/ViewDesignerRequestsModal/index";
import {DesignerRequestsRepository} from "./repository/DesignerRequestsRepository";
import {DesignerRequestsRepositoryResolve} from "./resolve/DesignerRequestsRepositoryResolve";
import {DesignerRequestsDatatable} from "./component/Elements/DesignerRequestsDatatable/index";
import {DesignerRequestsRESTService} from "../../../definitions/src/services/designer-request/DesignerRequestsRESTService";

export const HIDesignerRequestsModule = {
	routes: [
		DesignerRequestsRoute,
	],
	declarations: [
		DesignerRequestsDatatable,
		ViewDesignerRequestModal
	],
	providers: [
		DesignerRequestsRepository,
		DesignerRequestsRepositoryResolve,
	],
};

export const HIDesignerRequestRoutes = {
	path: 'requests',
	component: DesignerRequestsRoute,
	pathMatch: 'full',
	resolve: [
		DesignerRequestsRepositoryResolve,
	]
};