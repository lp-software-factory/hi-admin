import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CRUDSerialRepositoryInterface} from "../../crud/helpers/position/interfaces";
import {updatePosition} from "../../crud/functions/updatePosition";
import {ListProductsResponse200} from "../../../../definitions/src/definitions/products/paths/list";
import {ProductEntity} from "../../../../definitions/src/definitions/products/entity/ProductEntity";
import {ProductRESTService} from "../../../../definitions/src/services/product/ProductRESTService";

@Injectable()
export class ProductRepository implements CRUDSerialRepositoryInterface
{
	private etg: ProductEntity[] = [];

	constructor(private rest: ProductRESTService) {
	}

	fetch(): Observable<ListProductsResponse200> {
		let observable = this.rest.getProductsList();

		observable.subscribe(response => {
			this.etg = response.products;
		});

		return observable;
	}

	getAll(): ProductEntity[] {
		return this.etg;
	}

	getById(id: number) {
		let result = this.etg.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`ETG with id "${id}" not found`);
		}

		return id;
	}

	addETG(entity: ProductEntity) {
		this.etg.push(entity);
	}

	removeETG(id: number) {
		this.etg = this.etg.filter(entity => entity.id !== id);
	}

	replaceETG(id: number, etg: ProductEntity) {
		this.etg = this.etg.map(entity => {
			return entity.id === id ? etg : entity;
		})
	}

	getTotal(): number {
		return this.etg.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}
}