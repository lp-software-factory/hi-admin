import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalTextureFormModel} from "../../Forms/EnvelopeAdditionalTextureForm/index";


@Component({
	selector: 'hi-envelope-texture-additional-crud-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class AdditionalDeleteEnvelopeTextureModal implements AfterViewInit
{
	@Input('entity') texture: EnvelopeAdditionalTextureFormModel;

	@Output('success') successEvent: EventEmitter<any> = new EventEmitter<any>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	getPreviewTitle(): string {
		if(this.texture.files.preview !== undefined){
			if(this.texture.files.preview.name !== undefined){
				return this.texture.files.preview.name.substr(0, this.texture.files.preview.name.length - (this.texture.files.preview.name.length - 30));
			}
		} else if(this.texture.uploaded.preview !== undefined){
			if(this.texture.uploaded.preview.title !== undefined){
				return this.texture.uploaded.preview.title.substr(0, this.texture.uploaded.preview.title.length - (this.texture.uploaded.preview.title.length - 30));
			}
		}
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		this.successEvent.emit();
	}

	cancel() {
		this.cancelEvent.emit();
	}
}