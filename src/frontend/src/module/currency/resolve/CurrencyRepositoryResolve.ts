import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {CurrencyRepository} from "../repository/CurrencyRepository";
import {ListCurrencyResponse200} from "../../../../definitions/src/definitions/money/paths/list-currency";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";

@Injectable()
export class CurrencyRepositoryResolve implements Resolve<ListCurrencyResponse200>
{
	constructor(private repository: CurrencyRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			},
		);

		return observable;
	}
}