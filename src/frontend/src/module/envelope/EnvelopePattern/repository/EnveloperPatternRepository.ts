import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EnvelopePattern} from "../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {GetAllEnvelopePatternsResponse200} from "../../../../../definitions/src/definitions/envelope/pattern/path/get-all";
import {EnvelopePatternRESTService} from "../../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";

@Injectable()
export class EnvelopePatternRepository implements CRUDSerialRepositoryInterface
{
	private patterns: EnvelopePattern[] = [];

	constructor(private rest: EnvelopePatternRESTService) {
	}

	fetch(): Observable<GetAllEnvelopePatternsResponse200> {

		let observable = this.rest.getAllEnvelopePatterns();

		observable.subscribe(response => {
			this.patterns = response.envelope_patterns;
		});

		return observable;
	}

	getAll(): EnvelopePattern[] {
		return this.patterns;
	}

	getById(id: number): EnvelopePattern {
		let result = this.patterns.filter(entity => entity.entity.id === id);

		if(result.length === 0) {
			throw new Error(`Pattern with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.patterns.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.patterns, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.patterns, oldPosition, newPosition);
	}

	updatePosition(rows: Array<EnvelopePattern>, oldPosition, newPosition) {
		let tempRow;

		for(let index = 0; index < rows.length; index++) {
			if(rows[index].entity.position === newPosition) {
				tempRow = rows[index];
				for(let ind = 0; ind < rows.length; ind++) {
					if(rows[ind].entity.position === oldPosition) {
						tempRow.entity.position = rows[ind].entity.position;
						rows[ind].entity.position = newPosition;
						break;
					}
				}
				break;
			}
		}
	}

	add(entity: EnvelopePattern) {
		this.patterns.push(entity);
	}

	updateAllPositions(position: number) {
		this.patterns.forEach(pattern => {
			if(pattern.entity.position > position){
				pattern.entity.position += -1;
			}
		});
	}

	remove(id: number) {
		this.patterns.forEach(pattern => {
			if(pattern.entity.id === id) {
				this.updateAllPositions(pattern.entity.position);
			}
		});

		this.patterns = this.patterns.filter(entity => entity.entity.id !== id);
	}

	replace(pattern: EnvelopePattern) {
		this.patterns = this.patterns.map(entity => {
			if(entity.entity.id === pattern.entity.id) {
				return pattern;
			} else {
				return entity;
			}
		})
	}
}