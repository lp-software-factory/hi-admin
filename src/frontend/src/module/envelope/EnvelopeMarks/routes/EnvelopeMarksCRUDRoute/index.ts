import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EnvelopeMarksRepository} from "../../repository/EnvelopeMarksRepository";
import {EnvelopeMarks, EnvelopeMarksEntity} from "../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopeMarksRESTService} from "../../../../../../definitions/src/services/envelope/envelope-marks/EnvelopeMarksRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EnvelopeMarksRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EnvelopeMarksRESTService
		},
	]
})

export class EnvelopeMarksCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EnvelopeMarksEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EnvelopeMarksRepository,
		private service: EnvelopeMarksRESTService) {
	}
	
	isActivated(): boolean {
		return this.c.getCurrentEntity().is_activated;
	}
	
	activateMarks() {
		let loading = this.l.addLoading();
		this.service.activateEnvelopeMarks(this.c.getCurrentEntity().id).subscribe(response => {
			this.r.replace(response.envelope_marks);
			loading.is = false;
		});
	}
	
	deactivateMarks() {
		let loading = this.l.addLoading();
		this.service.deactivateEnvelopeMarks(this.c.getCurrentEntity().id).subscribe(response => {
			this.r.replace(response.envelope_marks);
			loading.is = false;
		});
	}

	getEntities(): EnvelopeMarksEntity[] {
		return this.r.getAll().map(entity => {
			return entity.entity
		}).sort((a, b) => {
			return a.position - b.position;
		});
	}
}