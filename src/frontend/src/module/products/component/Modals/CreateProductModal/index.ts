import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {ProductEntity} from "../../../../../../definitions/src/definitions/products/entity/ProductEntity";
import {CreateProductRequest} from "../../../../../../definitions/src/definitions/products/paths/create";
import {ProductRESTService} from "../../../../../../definitions/src/services/product/ProductRESTService";


@Component({
	selector: 'hi-product-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateProductModal
{
	@Input('repository') repository: any;

	@Output('success') successEvent: EventEmitter<ProductEntity> = new EventEmitter<ProductEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private model: CreateProductRequest;

	constructor(private service: ProductRESTService) {
	}

	ngOnInit() {
		this.model = {
			title: [],
			description: [],
			base_price: {
				currency: this.repository.currencies[0].code,
				amount: 0
			}
		};
	}

	checkRequiredFields(): boolean {
		return this.model.title.length > 0;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createProduct(this.model).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.product);
			},
			error => {
				loading.is = false;
			}
		);
	}
}