import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CurrencyEntity} from "../../../../../../definitions/src/definitions/money/entity/CurrencyEntity";
import {CurrencyRESTService} from "../../../../../../definitions/src/services/currency/CurrencyRESTService";

@Component({
	selector: 'hi-currency-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class DeleteCurrencyModal implements AfterViewInit
{
	@Input('currency') currency: CurrencyEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: CurrencyRESTService) {
	}

	getCurrencyName(): string {
		return this.currency.code + '/' + this.currency.sign;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let currencyId = this.currency.id;
		let loading = this.status.addLoading();

		this.service.deleteCurrency(currencyId).subscribe(() => {
			this.successEvent.emit(currencyId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}