import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {OAuth2ProviderEntity} from "../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";
import {GetAllOAuth2ProvidersResponse200} from "../../../../../definitions/src/definitions/oauth2/providers/paths/get-all-providers";
import {OAuth2ProvidersRESTService} from "../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Injectable()
export class OAuth2ProviderRepository
{
	private currencies: OAuth2ProviderEntity[] = [];

	constructor(private rest: OAuth2ProvidersRESTService) {
	}

	fetch(): Observable<GetAllOAuth2ProvidersResponse200> {
		let observable = this.rest.getAllOAuth2Providers();

		observable.subscribe(
			response => {
				this.currencies = response.providers;
			}
		);

		return observable;
	}

	getAll(): OAuth2ProviderEntity[] {
		return this.currencies;
	}

	getById(id: number): OAuth2ProviderEntity {
		let result = this.currencies.filter(oAuth2Provider => oAuth2Provider.id === id);

		if(result.length === 0) {
			throw new Error(`OAuth2Provider with ID "${id}" not found`)
		}

		return result[0];
	}

	addOAuth2Provider(entity: OAuth2ProviderEntity) {
		this.currencies.push(entity);
	}

	removeOAuth2Provider(code: string) {
		this.currencies = this.currencies.filter(oAuth2Provider => oAuth2Provider.code !== code);
	}

	replaceOAuth2Provider(entity: OAuth2ProviderEntity) {
		this.currencies = this.currencies.map(oAuth2Provider => {
			return oAuth2Provider.id === entity.id ? entity : oAuth2Provider;
		})
	}
}