import {Component, Input} from "@angular/core";
import {LocaleEntity} from "../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-locale-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class LocaleDataTable
{
	@Input('entities') entities: LocaleEntity[] = [];

	constructor(private cursor: DTCursor<LocaleEntity>,) {
	}
}