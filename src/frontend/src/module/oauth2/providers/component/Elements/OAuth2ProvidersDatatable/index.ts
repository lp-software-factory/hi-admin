import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {OAuth2ProviderEntity} from "../../../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";

@Component({
	selector: 'hi-oauth2-providers-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class OAuth2ProvidersDataTable
{
	@Input('entities') entities: OAuth2ProviderEntity[] = [];

	constructor(private cursor: DTCursor<OAuth2ProviderEntity>,) {
	}
}