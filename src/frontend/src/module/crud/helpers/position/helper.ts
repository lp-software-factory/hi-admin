import {Injectable} from "@angular/core";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDRepository, CRUDRESTService} from "../../provide/provides";
import {CRUDCurrentRecordHelper} from "../current/helper";
import {SerialAndIdEntity} from "./interfaces";

@Injectable()
export class CRUDPositionEntityHelper
{
	constructor(private loading: LoadingManager,
		private repository: CRUDRepository,
		private current: CRUDCurrentRecordHelper<SerialAndIdEntity>,
		private rest: CRUDRESTService,) {
	}

	canMoveUp(entity: SerialAndIdEntity): boolean {
		return (this.current.hasActiveEntity() && this.current.getCurrentEntity().position !== 1);
	}

	canMoveDown(entity: SerialAndIdEntity): boolean {
		return (this.current.hasActiveEntity() && this.current.getCurrentEntity().position !== this.repository.getTotal());
	}

	up() {
		let current = this.current.getCurrentEntity();

		if(current.position !== 1) {
			let loading = this.loading.addLoading();
			let oldPosition = current.position;

			this.rest.moveEntityUp(current.id).subscribe(response => {
				this.repository.moveUp(current.id, oldPosition, response.position);
				loading.is = false;
			}, error => {
				loading.is = false;
			});
		}
	}

	down() {
		let current = this.current.getCurrentEntity();

		if(current.position !== this.repository.getTotal()) {
			let loading = this.loading.addLoading();
			let oldPosition = current.position;

			this.rest.moveEntityDown(current.id).subscribe(response => {
				this.repository.moveDown(current.id, oldPosition, response.position);
				loading.is = false;
			}, error => {
				loading.is = false;
			});
		}
	}
}