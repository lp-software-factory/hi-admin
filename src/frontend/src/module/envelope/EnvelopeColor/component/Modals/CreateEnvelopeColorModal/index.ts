import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeColorFormModel} from "../../Forms/EnvelopeColorForm/index";
import {EnvelopeColorEntity} from "../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {EnvelopeColorRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-color/EnvelopeColorRESTService";

@Component({
	selector: 'hi-envelope-color-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateEnvelopeColorModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeColorEntity> = new EventEmitter<EnvelopeColorEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeColorFormModel();

	constructor(private service: EnvelopeColorRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && ! this.status.isLoading();
	}
	

	submit() {
		let loading = this.status.addLoading();

		this.service.createEnvelopeColor(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.envelope_color.entity);
			},
			error => {
				loading.is = false;
			}
		);
	}
}