import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {ProductsPackageEntity} from "../../../../../../definitions/src/definitions/products-package/entity/ProductsPackageEntity";
import {TranslationService} from "../../../../i18n/service/TranslationService";

@Component({
	selector: 'hi-products-package-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class ProductsPackageDataTable
{
	@Input('entities') entities: ProductsPackageEntity[] = [];

	constructor(private t: TranslationService,
		private cursor: DTCursor<ProductsPackageEntity>) {
	}

	getCursor(): DTCursor<ProductsPackageEntity> {
		return this.cursor;
	}
}