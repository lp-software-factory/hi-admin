import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {GetAllEventTypeGroupsResponse200} from "../../../../../definitions/src/definitions/events/event-type-group/paths/all";
import {EventTypeGroupRepository} from "../repository/EventTypeGroupRepository";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";

@Injectable()
export class EventTypeGroupRepositoryResolve implements Resolve<GetAllEventTypeGroupsResponse200>
{
	constructor(private repository: EventTypeGroupRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}