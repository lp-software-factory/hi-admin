import {Component, EventEmitter, Output, ViewChild} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeTextureFormModel, EnvelopeTextureForm} from "../../Forms/EnvelopeTextureForm/index";
import {EnvelopeTextureEntity} from "../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {EnvelopeAdditionalTextureFormModel} from "../../Forms/EnvelopeAdditionalTextureForm/index";
import {EnvelopeTextureRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeTextureDefinition} from "../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

@Component({
	selector: 'hi-envelope-texture-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor,
	]
})
export class CreateEnvelopeTextureModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeTextureEntity> = new EventEmitter<EnvelopeTextureEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('envelopeTextureForm') envelopeTextureForm: EnvelopeTextureForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeTextureFormModel();

	constructor(private service: EnvelopeTextureRESTService,
				private l: LoadingManager,
				private m: CRUDGenericModalsHelper,
				private cursor: DTCursor<EnvelopeTextureDefinition>,
				private attachment: AttachmentRESTService
	) {}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0
			&& this.formModel.definitions.length > 0 && (this.formModel.previewModel.uploaded.preview != undefined ||
			this.formModel.previewModel.files.preview != undefined) && ! this.status.isLoading();
	}
	
	addDefinition(entity: EnvelopeAdditionalTextureFormModel) {
		this.formModel.definitions.push(entity);
	}
	
	replaceDefinition(entity: EnvelopeAdditionalTextureFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			return compare;
		})
	}
	
	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(texture => JSON.stringify(texture) !== JSON.stringify(this.cursor.current()));
		this.cursor._current = undefined;
	}

	submit($event) {
		$event.preventDefault();

		this.envelopeTextureForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();

			this.service.createEnvelopeTexture(request).subscribe(
				response => {
					loading.is = false;

					this.successEvent.emit(response.envelope_texture.entity);
				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}