import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardSizeEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {LocaleService} from "../../../../../locale/service/LocaleService";
import {InviteCardSizeRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";


@Component({
	selector: 'hi-invite-card-size-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteInviteCardSizeModal implements AfterViewInit
{
	@Input('invite_card_size') invite_card_size: InviteCardSizeEntity;
	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: InviteCardSizeRESTService, private localeService: LocaleService) {
	}


	getInviteCardSizeName(): string {
		return this.localeService.getLocalization(this.invite_card_size.title).value;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let InviteCardSizeId = this.invite_card_size.id;
		let loading = this.status.addLoading();

		this.service.deleteInviteCardSize(InviteCardSizeId).subscribe(() => {
			this.successEvent.emit(InviteCardSizeId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}