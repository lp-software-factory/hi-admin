import {Component, Input} from "@angular/core";

@Component({
	selector: 'hi-help-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class HelpModal
{
	@Input('md-content') mdContent: string = '';

	private isOpened: boolean = false;

	open() {
		this.isOpened = true;
	}

	close() {
		this.isOpened = false;
	}
}