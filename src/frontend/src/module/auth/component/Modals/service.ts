import {ModalControl} from "../../../common/classes/ModalControl";

export class AuthModalsService
{
	private signInModal: ModalControl = new ModalControl();
	private signOutModal: ModalControl = new ModalControl();

	closeAllModals() {
		this.signInModal.close();
		this.signOutModal.close();
	}

	signIn() {
		this.closeAllModals();
		this.signInModal.open();
	}

	signOut() {
		this.closeAllModals();
		this.signOutModal.open();
	}
}