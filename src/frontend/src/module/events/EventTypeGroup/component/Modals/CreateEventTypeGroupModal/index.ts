import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EventTypeGroupEntity} from "../../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {EventTypeGroupFormModel} from "../../Forms/EventTypeGroupForm/index";
import {EventTypeGroupRESTService} from "../../../../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

@Component({
	selector: 'hi-event-type-group-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class CreateEventTypeGroupModal
{
	@Output('success') successEvent: EventEmitter<EventTypeGroupEntity> = new EventEmitter<EventTypeGroupEntity>();
	@Output('cancel') cancelEvent: EventEmitter<void> = new EventEmitter<void>();

	private formModel: EventTypeGroupFormModel = {
		url: "",
		title: [],
		description: [],
	};
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EventTypeGroupRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createEventTypeGroup(this.formModel).subscribe(
			response => {
				this.successEvent.emit(response.event_type_group);

				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		)
	}
}