import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeColorEntity} from "../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";

@Component({
	selector: 'hi-envelope-color-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeColorDatatable
{
	@Input('entities') entities: EnvelopeColorEntity[] = [];

	constructor(private cursor: DTCursor<EnvelopeColorEntity>) {
	}
}