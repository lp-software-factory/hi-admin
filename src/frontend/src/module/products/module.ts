import {ProductCRUDRoute} from "./routes/ProductCRUDRoute/index";
import {DeleteProductModal} from "./component/Modals/DeleteProductModal/index";
import {CreateProductModal} from "./component/Modals/CreateProductModal/index";
import {EditProductModal} from "./component/Modals/EditProductModal/index";
import {ProductRepositoryResolve} from "./resolve/ProductsPackageRepositoryResolve";
import {CurrencyRepositoryResolve} from "../currency/resolve/CurrencyRepositoryResolve";
import {ProductRepository} from "./repository/ProductRepository";
import {ProductDataTable} from "./component/Elements/ProductDatatable/index";
import {ProductRESTService} from "../../../definitions/src/services/product/ProductRESTService";

export const HIProductModule = {
	routes: [
		ProductCRUDRoute,
	],
	declarations: [
		CreateProductModal,
		DeleteProductModal,
		EditProductModal,
		ProductDataTable
	],
	providers: [
		ProductRepository,
		ProductRepositoryResolve,
	],
};

export const HIProductModuleRoutes = {
	path: 'products',
	component: ProductCRUDRoute,
	pathMatch: 'full',
	resolve: [
		ProductRepositoryResolve,
		CurrencyRepositoryResolve
	]
};