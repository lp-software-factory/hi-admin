import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {DesignerRequestEntity} from "../../../../../../definitions/src/definitions/designer-requests/entity/DesignerRequestEntity";
import {DesignerRequestsRESTService} from "../../../../../../definitions/src/services/designer-request/DesignerRequestsRESTService";

@Component({
	selector: 'hi-designer-request-view-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class ViewDesignerRequestModal
{
	@Input('designer_request') designer_request: DesignerRequestEntity;

	@Output('close') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	@Output('decision-made') decisionMadeEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();

	constructor(private service: DesignerRequestsRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	accept() {
		let loading = this.status.addLoading();

		this.service.acceptDesignerRequest(this.designer_request.id).subscribe(
			response => {
				loading.is = false;

				this.decisionMadeEvent.emit(response.designer_request);
			},
			error => {
				loading.is = false;
			}
		);
	}

	decline() {
		let loading = this.status.addLoading();

		this.service.declineDesignerRequest(this.designer_request.id).subscribe(
			response => {
				loading.is = false;

				this.decisionMadeEvent.emit(response.designer_request);
			},
			error => {
				loading.is = false;
			}
		);
	}

	isReviewed(): boolean {
		return this.designer_request.is_reviewed;
	}
}