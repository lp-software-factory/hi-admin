import {Component, EventEmitter, Output, Input} from "@angular/core";

export enum OAuth2FormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export interface OAuth2ProviderFormModel
{
	code: string;
	handler: string;
	config: {
		clientId: string,
		clientSecret: string,
		redirectUri: string
	}
}

@Component({
	selector: 'hi-oauth2-provider-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class OAuth2ProviderForm
{
	@Output() modelChange: EventEmitter<OAuth2ProviderFormModel> = new EventEmitter<OAuth2ProviderFormModel>();

	@Input('handlers') handlers: string[] = [];
	@Input('mode') mode: OAuth2FormMode = OAuth2FormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: OAuth2ProviderFormModel;

	isCreateForm(): boolean {
		return this.mode === OAuth2FormMode.Create;
	}
}