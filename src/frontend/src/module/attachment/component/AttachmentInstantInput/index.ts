import {Component, Input, Output, EventEmitter} from "@angular/core";

import {AttachmentEntity} from "../../../../../definitions/src/definitions/attachment/entity/AttachmentEntity";
import {AttachmentRESTService} from "../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {LoadingStatus, LoadingManager} from "../../../common/classes/LoadingStatus";
import {basename} from "../../../common/functions/basename";

@Component({
    selector: 'hi-attachment-instant-input',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss')
    ],
})
export class AttachmentInstantInput
{
    @Output() valueChange = new EventEmitter<any>();
    @Output('on-upload') onUploadEvent: EventEmitter<LoadingStatus> = new EventEmitter<LoadingStatus>();
    @Output('on-upload-fail') onUploadFailEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('on-upload-complete') onUploadCompleteEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('on-delete-value') onDeleteValueEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output('on-delete-attachment') onDeleteAttachmentEvent: EventEmitter<any> = new EventEmitter<any>();

    @Input('allow_multiple') allow_multiple: boolean = false;

    @Input()
    get value() {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.valueChange.emit(val);
    }

    private _revert: AttachmentEntity<any>;
    private _value: any[] = [];
    private status: LoadingManager = new LoadingManager();

        constructor(
        private rest: AttachmentRESTService,
    ) {}

    onFileChange($event) {
        if($event.target.files.length) {
            this.upload($event.target.files);
        }
    }

    getFileName(file: AttachmentEntity<any>): string {
        return basename(file.link.url)
    }

    getDateUploaded(file: AttachmentEntity<any>): string {
        return file.date_attached_on;
    }

    upload(files: Blob[]) {
        let loading = this.status.addLoading();

        this.onUploadEvent.emit(loading);

        if(this.allow_multiple) {
            let files_ids: number[] = [];

            if(this.value) {
                this.destroy();
            }

            for(let i = 0; i < files.length; i++) {
                this.rest.upload(files[i]).last().subscribe(
                    response => {
                        this.value.push(response['entity']);
                        files_ids.push(response['entity']);
                        if(i === files.length - 1){
                            this.onUploadCompleteEvent.emit(files_ids);
                        }
                    },
                    error => {
                        this.onUploadFailEvent.emit();

                        loading.is = false;
                    }
                )
            }
        } else {
            this.rest.upload(files[0]).last().subscribe(
                response => {
                    if(this.value) {
                        this.destroy();
                    }
                    this.value.push(response['entity']);
                    this.onUploadCompleteEvent.emit(response['entity']);
                },
                error => {
                    this.onUploadFailEvent.emit();

                    loading.is = false;
                }
            )
        }
    }

    destroy() {
        if(this.value) {
            this.value = [];
            this.onDeleteValueEvent.emit();
        }
    }

    revert(file) {
        if(this.value) {
            if(this.allow_multiple && this.value.length > 0) {
                this.value = this.value.filter(entity => {
                    return file.id !== entity.id;
                });
                this.onDeleteAttachmentEvent.emit(file);
            } else {
                this.destroy();
            }
        }
    }
}