import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

import {GetAllEventTypeGroupsResponse200} from "../../../../../definitions/src/definitions/events/event-type-group/paths/all";
import {EventTypeGroupEntity} from "../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {updatePosition} from "../../../crud/functions/updatePosition";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EventTypeGroupRESTService} from "../../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

@Injectable()
export class EventTypeGroupRepository implements CRUDSerialRepositoryInterface
{
	private etg: EventTypeGroupEntity[] = [];

	constructor(private rest: EventTypeGroupRESTService) {
	}

	fetch(): Observable<GetAllEventTypeGroupsResponse200> {
		let observable = this.rest.getAllEventTypeGroups();

		observable.subscribe(response => {
			this.etg = response.event_type_groups;
		});

		return observable;
	}

	getAll(): EventTypeGroupEntity[] {
		return this.etg;
	}

	getById(id: number): EventTypeGroupEntity {
		let result = this.etg.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`ETG with id "${id}" not found`);
		}

		return result[0];
	}

	addETG(entity: EventTypeGroupEntity) {
		this.etg.push(entity);
	}

	removeETG(id: number) {
		this.etg = this.etg.filter(entity => entity.id !== id);
	}

	replaceETG(id: number, etg: EventTypeGroupEntity) {
		this.etg = this.etg.map(entity => {
			return entity.id === id ? etg : entity;
		})
	}

	getTotal(): number {
		return this.etg.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		updatePosition(this.etg, oldPosition, newPosition);
	}
}