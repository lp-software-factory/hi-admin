export interface DTSort
{
	field: string;
	direction: DTSortDirection;
}

export enum DTSortDirection
{
	Asc = <any>"asc",
	Desc = <any>"desc",
}
