import {Component, EventEmitter, Output, Input} from "@angular/core";

export enum LocaleFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export interface LocaleFormModel
{
	language: string;
	region: string;
}

@Component({
	selector: 'hi-locale-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class LocaleForm
{
	@Output() modelChange: EventEmitter<LocaleFormModel> = new EventEmitter<LocaleFormModel>();

	@Input('mode') mode: LocaleFormMode = LocaleFormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: LocaleFormModel;
}