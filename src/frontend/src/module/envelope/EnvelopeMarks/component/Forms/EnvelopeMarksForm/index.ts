import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AbstractEnvelopeMarksRequest} from "../../../../../../../definitions/src/definitions/envelope/marks/request/EnvelopeMarksRequests";
import {EnvelopeMarkDefinitionEntity} from "../../../../../../../definitions/src/definitions/envelope/mark-definition/entity/EnvelopeMarkDefinition";
import {EnvelopeAdditionalMarksFormModel} from "../EnvelopeAdditionalMarksForm/index";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";

export enum EnvelopeMarkFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class EnvelopeMarksFormModel implements AbstractEnvelopeMarksRequest
{
	id: undefined;
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	attachment_ids: Array<number> = [];
	definitions: EnvelopeAdditionalMarksFormModel[] = []
}

@Component({
	selector: 'hi-envelope-mark-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopeMarkForm
{
	@Output() modelChange: EventEmitter<EnvelopeMarksFormModel> = new EventEmitter<EnvelopeMarksFormModel>();

	@Input('mode') mode: EnvelopeMarkFormMode = EnvelopeMarkFormMode.Create;

	constructor(
		private cursor: DTCursor<EnvelopeMarkDefinitionEntity>,
		private m: CRUDGenericModalsHelper
	) {}

	getFileTitle(file: any) {
		if(file.title !== undefined) {
			if(file.title.length > 30){
				return file.title.substr(0, file.title.length - (file.title.length - 30));
			} else {
				return file.title;
			}
		} else if(file.name !== undefined){
			return file.name.substr(0, file.name.length - (file.name.length - 30));
		} else {
			return file.name;
		}
	}
	
	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EnvelopeMarksFormModel;
}
