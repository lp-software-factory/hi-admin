import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";

import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeMarksEntity} from "../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {EnvelopeTextureRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";

@Component({
	selector: 'hi-envelope-texture-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteEnvelopeTextureModal implements AfterViewInit
{
	@Input('envelope_texture') envelope_texture: EnvelopeMarksEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EnvelopeTextureRESTService) {
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	getTitle(): string {
		return this.envelope_texture.title[0].value;
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let id = this.envelope_texture.id;
		let loading = this.status.addLoading();

		this.service.deleteEnvelopeTexture(id).subscribe(() => {
			this.successEvent.emit(id);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}