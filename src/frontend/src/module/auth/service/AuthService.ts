import {Injectable} from "@angular/core";
import {JWTAuthToken} from "../../../../definitions/src/definitions/auth/entity/JWTAuthToken";

@Injectable()
export class AuthService
{
	private static LOCAL_STORAGE_JWT = 'jwt';

	private current: JWTAuthToken;

	public hasJWT(): boolean {
		return !!window.localStorage.getItem(AuthService.LOCAL_STORAGE_JWT);
	}

	public getJWT(): string {
		return window.localStorage.getItem(AuthService.LOCAL_STORAGE_JWT);
	}

	public clearJWT() {
		window.localStorage.removeItem(AuthService.LOCAL_STORAGE_JWT);
	}

	public getToken(): JWTAuthToken {
		if(!this.isSignedIn()) {
			throw new Error("You're not signed in");
		}

		return this.current;
	}

	public isSignedIn(): boolean {
		return !!this.current;
	}

	public isAdmin(): boolean {
		return !!~this.current.account.app_access.indexOf("admin");
	}

	public signIn(token: JWTAuthToken) {
		this.current = token;

		window.localStorage.setItem(AuthService.LOCAL_STORAGE_JWT, token.jwt);
	}

	public signOut() {
		this.current = null;
		this.clearJWT();
	}
}