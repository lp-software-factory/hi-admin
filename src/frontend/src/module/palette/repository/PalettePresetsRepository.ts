import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {PaletteEntity} from "../../../../definitions/src/definitions/colors/entity/PaletteEntity";
import {FrontendService} from "../../frontend/service/FrontendService";
import {randomInt} from "../../../../definitions/src/definitions/_common/functions/randomInt";

@Injectable()
export class PalettePresetsRepository
{
	palettes: PaletteEntity[];

	constructor(private frontend: FrontendService) {
	}

	fetch(): Observable<any> {
		let observable = Observable.create(observer => {
			observer.next(this.frontend.fetch().subscribe(response => {
				this.palettes = response.palettes;

				observer.next({
					success: true,
					palettes: response.palettes
				});

				observer.complete();
			}));
		});

		observable.subscribe(() => {
		});

		return observable;
	}

	getAll(): PaletteEntity[] {
		return this.palettes;
	}

	getByCode(code: string|MaterialPalettePresets): PaletteEntity {
		let result = this.palettes.filter(palette => palette.code === code);

		if(result.length === 0) {
			throw new Error(`Palette with code "${code}" not found`);
		}

		return result[0];
	}

	getRandom(): PaletteEntity {
		if(this.palettes.length === 0) {
			throw new Error('No palettes available')
		}

		return this.palettes[randomInt(0, this.palettes.length - 1)];
	}
}

export enum MaterialPalettePresets
{
	Red = <any>"red",
	Pink = <any>"pink",
	Purple = <any>"purple",
	DeepPurple = <any>"deep-purple",
	Indigo = <any>"indigo",
	Blue = <any>"blue",
	LightBlue = <any>"light-blue",
	Cyan = <any>"cyan",
	Teal = <any>"teal",
	Green = <any>"green",
	LightGreen = <any>"light-green",
	Lime = <any>"lime",
	Yellow = <any>"yellow",
	Amber = <any>"amber",
	Orange = <any>"orange",
	DeepOrange = <any>"deep-orange",
	Brown = <any>"brown",
	Grey = <any>"grey",
	BlueGrey = <any>"blue-grey",
}