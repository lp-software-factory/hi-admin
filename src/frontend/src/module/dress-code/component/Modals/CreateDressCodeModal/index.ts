import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {DressCodeEntity} from "../../../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {DressCodeFormModel} from "../../Forms/DressCodeForm/index";
import {DressCodeRESTService} from "../../../../../../definitions/src/services/dress-code/DressCodeRESTService";

@Component({
	selector: 'hi-dress-code-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateDressCodeModal
{
	@Output('success') successEvent: EventEmitter<DressCodeEntity> = new EventEmitter<DressCodeEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: DressCodeFormModel = new DressCodeFormModel();

	constructor(private service: DressCodeRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.create(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.dress_code.entity);
			},
			error => {
				loading.is = false;
			}
		);
	}
}