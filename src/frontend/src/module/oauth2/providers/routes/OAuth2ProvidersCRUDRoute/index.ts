import {Component} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {OAuth2ProviderRepository} from "../../repository/OAuth2ProvidersRepository";
import {OAuth2ProviderHandlersRepository} from "../../repository/OAuth2ProviderHandlersRepository";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {OAuth2ProviderEntity} from "../../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";
import {OAuth2ProvidersRESTService} from "../../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: OAuth2ProviderRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: OAuth2ProvidersRESTService
		},
	],
})
export class OAuth2ProvidersCRUDRoute
{
	constructor(private rProviders: OAuth2ProviderRepository,
		private rHandlers: OAuth2ProviderHandlersRepository,
		private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<OAuth2ProviderEntity>,
		private m: CRUDGenericModalsHelper,) {
	}

	getRows(): OAuth2ProviderEntity[] {
		return this.rProviders.getAll();
	}
}