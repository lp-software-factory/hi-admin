import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AbstractEnvelopeColorRequest} from "../../../../../../../definitions/src/definitions/envelope/color/request/EnvelopeColorRequests";


export enum EnvelopeColorFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class EnvelopeColorFormModel implements AbstractEnvelopeColorRequest
{
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	hex_code: string = '';
}

@Component({
	selector: 'hi-envelope-color-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopeColorForm
{
	@Output() modelChange: EventEmitter<EnvelopeColorFormModel> = new EventEmitter<EnvelopeColorFormModel>();

	@Input('mode') mode: EnvelopeColorFormMode = EnvelopeColorFormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EnvelopeColorFormModel = new EnvelopeColorFormModel();
}
