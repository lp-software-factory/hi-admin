import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {LocaleEntity} from "../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {LocaleFormModel} from "../../Form/LocaleForm/index";
import {LocaleRESTService} from "../../../../../../definitions/src/services/locale/LocaleRESTService";

@Component({
	selector: 'hi-locale-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateLocaleModal
{
	@Output('success') successEvent: EventEmitter<LocaleEntity> = new EventEmitter<LocaleEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: LocaleFormModel = {
		language: '',
		region: '',
	};

	constructor(private service: LocaleRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.create(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.locale);
			},
			error => {
				loading.is = false;
			}
		);
	}
}