import {Component, EventEmitter, Output, ViewChild} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeBackdropFormModel, EnvelopeBackdropForm} from "../../Forms/EnvelopeBackdropForm/index";
import {EnvelopeBackdropEntity} from "../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {EnvelopeAdditionalBackdropFormModel} from "../../Forms/EnvelopeAdditionalBackdropForm/index";
import {EnvelopeBackdropRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-backdrop/EnvelopeBackdropRESTService";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeBackdropDefinition} from "../../../../../../../definitions/src/definitions/envelope/backdrop-definition/entity/EnvelopeBackdropDefinition";
import {EnvelopeBackdropDefinitionRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-backdrop-definition/EnvelopeBackdropDefinitionRESTService";
import {CreateEnvelopeBackdropDefinitionRequest} from "../../../../../../../definitions/src/definitions/envelope/backdrop-definition/request/EnvelopeBackdropRequests";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {Observable} from "rxjs";

@Component({
	selector: 'hi-envelope-backdrop-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor,
	]
})
export class CreateEnvelopeBackdropModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeBackdropEntity> = new EventEmitter<EnvelopeBackdropEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('envelopeBackdropForm') envelopeBackdropForm: EnvelopeBackdropForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeBackdropFormModel();

	constructor(private service: EnvelopeBackdropRESTService,
				private definitionService: EnvelopeBackdropDefinitionRESTService,
				private l: LoadingManager,
				private m: CRUDGenericModalsHelper,
				private cursor: DTCursor<EnvelopeBackdropDefinition>,
				private attachments: AttachmentRESTService
	) {}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && this.formModel.definitions.length > 0 && ! this.status.isLoading();
	}
	
	addDefinition(entity: EnvelopeAdditionalBackdropFormModel) {
		this.formModel.definitions.push(entity);
	}
	
	replaceDefinition(entity: EnvelopeAdditionalBackdropFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			return compare; // TODO: Fix or remove
		})
	}
	
	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(backdrop => JSON.stringify(backdrop) !== JSON.stringify(this.cursor.current()));
	}

	submit($event) {
		$event.preventDefault();

		this.envelopeBackdropForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();

			this.service.createEnvelopeBackdrop(request).subscribe(
				response => {
					let entity = response.envelope_backdrop.entity;
					let allObservables = [];
					this.envelopeBackdropForm.model.definitions.map(definition => {
						let observables = [];

						console.log(definition);
						let definitionRequest = {
							title: Array<LocalizedString>(),
							description: Array<LocalizedString>(),
							preview_attachment_id: 0,
							resource_attachment_id: 0,
							owner_envelope_backdrop_id: 0,
						};

						if (definition.uploaded.resource != undefined) {
							definitionRequest.resource_attachment_id = definition.uploaded.resource.id;
						}
						if (definition.uploaded.preview != undefined) {
							definitionRequest.preview_attachment_id = definition.uploaded.preview.id;
						}
						if(definition.files.preview instanceof Blob) {
							let observable = this.attachments.upload(definition.files.preview);

							observable.subscribe(response => {
								if (response.hasOwnProperty('entity')) {
									definitionRequest.preview_attachment_id = response['entity'].id;
								}
							});

							observables.push(observable);
						}
						if(definition.files.resource instanceof Blob) {
							let observable = this.attachments.upload(definition.files.resource);

							observable.subscribe(response => {
								if (response.hasOwnProperty('entity')) {
									definitionRequest.resource_attachment_id = response['entity'].id;
								}
							});

							observables.push(observable);
						}
						definitionRequest.owner_envelope_backdrop_id = entity.id;
						definitionRequest.title = definition.title;
						definitionRequest.description = definition.description;
						if (observables.length > 0) {
							Observable.forkJoin(observables).subscribe(response => {
								allObservables.push(this.definitionService.createEnvelopeBackdropDefinition(definitionRequest));
							})
						} else {
							allObservables.push(this.definitionService.createEnvelopeBackdropDefinition(definitionRequest));
						}
					});

					if (allObservables.length > 0) {
						Observable.forkJoin(allObservables).subscribe(response => {
							loading.is = false;
							this.successEvent.emit(entity);
						},
						error => {
							loading.is = false;
						})
					} else {
						this.successEvent.emit(entity);
					}

				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}