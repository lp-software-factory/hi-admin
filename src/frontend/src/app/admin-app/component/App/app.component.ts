import {Component} from "@angular/core";

@Component({
	selector: 'hi-admin-app',
	template: require('./template.jade'),
})
export class App
{
	constructor() {
		console.log(`HI Admin App: ver${App.version()}`);
	}

	static version(): string {
		return require('./../../../../../package.json').version;
	}
}