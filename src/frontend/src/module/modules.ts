import {HIFrontendModule} from "./frontend/module";
import {HIAuthModule} from "./auth/module";
import {HIUIModule} from "./ui/module";
import {HIAccountModule} from "./account/module";
import {HICommonModule} from "./common/module";
import {HICompanyModule} from "./company/module";
import {HIMessageModule} from "./message/module";
import {HILocaleModule} from "./locale/module";
import {HICurrencyModule} from "./currency/module";
import {HIEventTypeGroupModule} from "./events/EventTypeGroup/module";
import {HIEventTypeModule} from "./events/EventType/module";
import {HIInviteCardColorModule} from "./invites/InviteCardColor/module";
import {HIInviteCardStyleModule} from "./invites/InviteCardStyle/module";
import {HIInviteCardSizeModule} from "./invites/InviteCardSize/module";
import {HIProductModule} from "./products/module";
import {HIProductsPackageModule} from "./products-package/module";
import {HIOAuth2ProvidersModule} from "./oauth2/providers/module";
import {HIDesignerRequestsModule} from "./designer-requests/module";
import {HIInviteCardFamilyRoute} from "./invites/InviteCardFamilies/module";
import {HIProfileRoute} from "./profile/module";
import {HIInviteCardGammaModule} from "./invites/InviteCardGamma/module";
import {HII18nModule} from "./i18n/module";
import {HIInviteCardTemplateRoute} from "./invites/InviteCardTemplate/module";
import {HIImageModule} from "./image/module";
import {HIPaletteModule} from "./palette/module";
import {HIFontModule} from "./fonts/module";
import {HIEnvelopeMarksModule} from "./envelope/EnvelopeMarks/module";
import {HIEnvelopeColorModule} from "./envelope/EnvelopeColor/module";
import {HIEnvelopeModule} from "./envelope/Envelope/module";
import {HIInviteCardModule} from "./invites/InviteCard/module";
import {HIEnvelopeTextureModule} from "./envelope/EnvelopeTexture/module";
import {HIDesignersModule} from "./designers/module";
import {HIAttachmentModule} from "./attachment/module";
import {HIEnvelopePatternModule} from "./envelope/EnvelopePattern/module";
import {HIEnvelopeBackdropModule} from "./envelope/EnvelopeBackdrop/module";
import {HIDressCodeModule} from "./dress-code/module";

export const HI_MODULES = [
	HIAuthModule,
	HIAccountModule,
	HIFrontendModule,
	HIUIModule,
	HICommonModule,
	HIMessageModule,
	HICompanyModule,
	HILocaleModule,
	HICurrencyModule,
	HIEventTypeGroupModule,
	HIEventTypeModule,
	HIInviteCardModule,
	HIInviteCardColorModule,
	HIInviteCardStyleModule,
	HIInviteCardSizeModule,
	HIProductModule,
	HIProductsPackageModule,
	HIOAuth2ProvidersModule,
	HIDesignerRequestsModule,
	HIInviteCardFamilyRoute,
	HIProfileRoute,
	HIInviteCardGammaModule,
	HII18nModule,
	HIInviteCardTemplateRoute,
	HIImageModule,
	HIPaletteModule,
	HIFontModule,
	HIEnvelopeModule,
	HIEnvelopeMarksModule,
	HIEnvelopeColorModule,
	HIEnvelopeTextureModule,
	HIEnvelopeBackdropModule,
	HIDesignersModule,
	HIAttachmentModule,
	HIEnvelopePatternModule,
	HIDressCodeModule
];