import {EnvelopeColorForm} from "./component/Forms/EnvelopeColorForm/index";
import {EnvelopeColorDatatable} from "./component/Elements/EnvelopeColorDatatable/index";
import {CreateEnvelopeColorModal} from "./component/Modals/CreateEnvelopeColorModal/index";
import {DeleteEnvelopeColorModal} from "./component/Modals/DeleteEnvelopeColorModal/index";
import {ColorPicker} from "../../common/component/ColorPicker/index";
import {EnvelopeColorRepository} from "./repository/EnvelopeColorRepository";
import {EnvelopeColorRepositoryResolve} from "./resolve/EnvelopeColorRepositoryResolve";
import {EnvelopeColorCRUDRoute} from "./routes/EnvelopeColorCRUDRoute/index";
import {EditEnvelopeColorModal} from "./component/Modals/EditEnvelopeColorModal/index";
import {EnvelopeColorRESTService} from "../../../../definitions/src/services/envelope/envelope-color/EnvelopeColorRESTService";

export const HIEnvelopeColorModule = {
	routes: [
		EnvelopeColorCRUDRoute,
	],
	declarations: [
		EnvelopeColorForm,
		EnvelopeColorDatatable,
		CreateEnvelopeColorModal,
		EditEnvelopeColorModal,
		DeleteEnvelopeColorModal,
		ColorPicker
	],
	providers: [
		EnvelopeColorRepository,
		EnvelopeColorRepositoryResolve
	],
};

export const HIEnvelopeColorRoute = {
	path: 'color',
	component: EnvelopeColorCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EnvelopeColorRepositoryResolve
	]
};