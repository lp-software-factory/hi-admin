import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EventTypeEntity} from "../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {EventTypeFormModel} from "../../Forms/EventTypeForm/index";
import {EventTypeRESTService} from "../../../../../../../definitions/src/services/event/event-type/EventTypeRESTService";

@Component({
	selector: 'hi-event-type-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateEventTypeModal
{
	@Input('event_type_group_id') groupId: number;

	@Output('success') successEvent: EventEmitter<EventTypeEntity> = new EventEmitter<EventTypeEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: EventTypeFormModel = {
		group_id: 0,
		url: "",
		title: Array<LocalizedString>(),
		description: Array<LocalizedString>()
	};

	constructor(private service: EventTypeRESTService) {
	}

	ngOnInit() {
		this.formModel.group_id = this.groupId;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createEventType(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.event_type);
			},
			error => {
				loading.is = false;
			}
		);
	}
}