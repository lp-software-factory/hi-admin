import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {DressCodeEntity} from "../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {DressCodeRESTService} from "../../../../definitions/src/services/dress-code/DressCodeRESTService";
import {DressCodeListResponse200} from "../../../../definitions/src/definitions/dress-code/path/list";

@Injectable()
export class DressCodeRepository
{
	private entity: DressCodeEntity[] = [];

	constructor(private rest: DressCodeRESTService) {
	}

	fetch(): Observable<DressCodeListResponse200> {
		let observable = this.rest.list();

		observable.subscribe(
			response => {
				this.entity = response.dress_codes.map(dress_code => {
					return dress_code.entity;
				});
			}
		);

		return observable;
	}

	getAll(): DressCodeEntity[] {
		return this.entity;
	}

	getById(id: number): DressCodeEntity {
		let result = this.entity.filter(dress_code => dress_code.id === id);

		if(result.length === 0) {
			throw new Error(`DressCode with ID "${id}" not found`)
		}

		return result[0];
	}

	addDressCode(entity: DressCodeEntity) {
		this.entity.push(entity);
	}

	removeDressCode(id: number) {
		this.entity = this.entity.filter(dress_code => dress_code.id !== id);
	}

	replaceDressCode(entity: DressCodeEntity) {
		this.entity = this.entity.map(dress_code => {
			return dress_code.id === entity.id ? entity : dress_code;
		})
	}
}