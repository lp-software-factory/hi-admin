import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {LocaleRepository} from "../repository/LocaleRepository";
import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";

@Injectable()
export class LocaleRepositoryResolve
{
	constructor(private repository: LocaleRepository,
		private status: UIGlobalLoadingIndicatorService,) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}