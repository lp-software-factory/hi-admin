import {ProductsPackageCRUDRoute} from "./routes/ProductsPackageCRUDRoute/index";
import {DeleteProductsPackageModal} from "./component/Modals/DeleteProductsPackageModal/index";
import {CreateProductsPackageModal} from "./component/Modals/CreateProductsPackageModal/index";
import {EditProductsPackageModal} from "./component/Modals/EditProductsPackageModal/index";
import {PalettePresetsRepositoryResolve} from "../palette/resolve/PalettePresetsRepositoryResolve";
import {ProductsPackageRepositoryResolve} from "./resolve/ProductsPackageRepositoryResolve";
import {ProductsPackageRepository} from "./repository/ProductsPackageRepository";
import {CurrencyRepositoryResolve} from "../currency/resolve/CurrencyRepositoryResolve";
import {SetPackagesModal} from "./component/Modals/SetProductsModal/index";
import {ProductsPackageDataTable} from "./component/Elements/ProductsPackageDatatable/index";
import {ProductRepositoryResolve} from "../products/resolve/ProductsPackageRepositoryResolve";
import {ProductsPackageRESTService} from "../../../definitions/src/services/products-package/ProductsPackageRESTService";

export const HIProductsPackageModule = {
	routes: [
		ProductsPackageCRUDRoute,
	],
	declarations: [
		CreateProductsPackageModal,
		DeleteProductsPackageModal,
		EditProductsPackageModal,
		SetPackagesModal,
		ProductsPackageDataTable
	],
	providers: [
		ProductsPackageRepository,
		ProductsPackageRepositoryResolve,
		PalettePresetsRepositoryResolve,
		ProductRepositoryResolve
	],
};

export const HIProductsPackageRoutes = {
	path: 'packages',
	component: ProductsPackageCRUDRoute,
	pathMatch: 'full',
	resolve: [
		ProductRepositoryResolve,
		PalettePresetsRepositoryResolve,
		ProductsPackageRepositoryResolve,
		CurrencyRepositoryResolve
	]
};