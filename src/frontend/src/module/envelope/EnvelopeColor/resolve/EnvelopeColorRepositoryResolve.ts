import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {EnvelopeColorRepository} from "../repository/EnvelopeColorRepository";
import {EnvelopeColorGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/color/path/get-all";

@Injectable()
export class EnvelopeColorRepositoryResolve implements Resolve<EnvelopeColorGetAllResponse200>
{
	constructor(private repository: EnvelopeColorRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}