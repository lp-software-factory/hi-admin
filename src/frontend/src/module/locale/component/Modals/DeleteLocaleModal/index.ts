import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LocaleEntity} from "../../../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {LocaleRESTService} from "../../../../../../definitions/src/services/locale/LocaleRESTService";

@Component({
	selector: 'hi-locale-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteLocaleModal implements AfterViewInit
{
	@Input('locale') locale: LocaleEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: LocaleRESTService) {
	}

	getLocaleName(): string {
		return this.locale.language + '/' + this.locale.region;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let localeId = this.locale.id;
		let loading = this.status.addLoading();

		this.service.delete(localeId).subscribe(() => {
			this.successEvent.emit(localeId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}