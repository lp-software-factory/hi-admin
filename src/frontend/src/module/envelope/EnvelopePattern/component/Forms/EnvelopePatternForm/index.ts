import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {AbstractEnvelopePatternRequest} from "../../../../../../../definitions/src/definitions/envelope/pattern/request/EnvelopePatternRequests";
import {Observable} from "rxjs/Observable";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";

export enum EnvelopePatternFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class EnvelopePatternFormModel
{
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	preview_file: any;
	texture_file: any;
}

@Component({
	selector: 'hi-envelope-pattern-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EnvelopePatternForm
{
	@Output() modelChange: EventEmitter<EnvelopePatternFormModel> = new EventEmitter<EnvelopePatternFormModel>();

	@Input('mode') mode: EnvelopePatternFormMode;

	constructor(private attachments: AttachmentRESTService){}

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}
	
	onPreviewFileChange(event) {
		this.model.preview_file = event.target.files[0]
	}
	
	onTextureFileChange(event) {
		this.model.texture_file = event.target.files[0]
	}

	deleteFile(type: string, file: any) {
		if(type === 'preview_file'){
			if(file.title !== undefined){
				this.model.preview_file = this.model.preview_file.filter(entity => entity.title !== file.title);
			} else if(file.name !== undefined) {
				this.model.preview_file = this.model.preview_file.filter(entity => entity.name !== file.name);
			}
		}

		if(type === 'texture_file'){
			if(file.title !== undefined){
				this.model.texture_file = this.model.texture_file.filter(entity => entity.title !== file.title);
			} else if(file.name !== undefined) {
				this.model.texture_file = this.model.texture_file.filter(entity => entity.name !== file.name);
			}
		}
	}

	getFileTitle(file: any) {
		if(file.title !== undefined) {
			if(file.title.length > 30){
				return file.title.substr(0, file.title.length - (file.title.length - 30));
			}
		} else if(file.name !== undefined){
			return file.name.substr(0, file.name.length - (file.name.length - 30));
		}
	}
	
	createRequest(): Observable<AbstractEnvelopePatternRequest> {
		return Observable.create(observer => {
			let request = {
				title: this.model.title,
				description: this.model.description,
				texture_attachment_id: undefined,
				preview_attachment_id: undefined
			};

			let textureId: number, previewId: number;

			let previewObservable = this.attachments.upload(this.model.preview_file);
			let textureObservable = this.attachments.upload(this.model.texture_file);

			previewObservable.subscribe(response => {
				if (response.hasOwnProperty('entity')) {
					previewId = response['entity'].id;
				}
			});

			textureObservable.subscribe(response => {
				if (response.hasOwnProperty('entity')) {
					textureId = response['entity'].id;
				}
			});

			let allObservables = Observable.forkJoin(previewObservable.concat(textureObservable));
				
			allObservables.subscribe(
				response => {
						request.texture_attachment_id = textureId,
						request.preview_attachment_id = previewId
					});
			
			Observable.forkJoin(allObservables).subscribe(
				response => {
					observer.next(request);
					observer.complete();
				}
			);
		});
	}

	public _model: EnvelopePatternFormModel;
}
