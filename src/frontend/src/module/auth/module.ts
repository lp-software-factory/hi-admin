import {SignIn} from "./component/Elements/SignIn/index";
import {SignOut} from "./component/Elements/SignOut/index";
import {AuthModals} from "./component/Modals/index";
import {AuthModalsService} from "./component/Modals/service";
import {SignInRoute} from "./routes/SignInRoute/index";
import {JWTValidationResolver} from "./resolve/JWTValidationResolver";
import {SignOutRoute} from "./routes/SignOutRoute/index";
import {AuthService} from "./service/AuthService";
import {AuthRESTService} from "../../../definitions/src/services/auth/AuthRESTService";

export const HIAuthModule = {
	routes: [
		SignInRoute,
		SignOutRoute,
	],
	declarations: [
		SignIn,
		SignOut,
		AuthModals,
	],
	providers: [
		AuthService,
		AuthModalsService,
		JWTValidationResolver,
	],
};