import {Component, EventEmitter, Output, Input} from "@angular/core";
import {CreateInviteCardGammaRequest} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/paths/create";
export enum InviteCardGammmaFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class InviteCardGammmaFormModel implements CreateInviteCardGammaRequest
{
	gamma = {
		title: [],
		description: []
	}
}

@Component({
	selector: 'hi-invite-card-gamma-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class InviteCardGammmaForm
{
	@Output() modelChange: EventEmitter<InviteCardGammmaFormModel> = new EventEmitter<InviteCardGammmaFormModel>();

	@Input('mode') mode: InviteCardGammmaFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: InviteCardGammmaFormModel;
}