import {Component} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {EnvelopePatternRepository} from "../../repository/EnveloperPatternRepository";
import {EnvelopePattern, EnvelopePatternEntity} from "../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopePatternRESTService} from "../../../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: EnvelopePatternRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: EnvelopePatternRESTService
		},
	]
})

export class EnvelopePatternCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<EnvelopePatternEntity>,
		private m: CRUDGenericModalsHelper,
		private r: EnvelopePatternRepository) {
	}

	getEntities(): EnvelopePatternEntity[] {
		return this.r.getAll().map(entity => {
			return entity.entity
		}).sort((a, b) => {
			return a.position - b.position;
		});
	}
}
