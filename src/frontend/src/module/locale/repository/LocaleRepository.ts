import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ListLocalesResponse200} from "../../../../definitions/src/definitions/locale/paths/list";
import {LocaleEntity} from "../../../../definitions/src/definitions/locale/definitions/LocaleEntity";
import {LocaleRESTService} from "../../../../definitions/src/services/locale/LocaleRESTService";

@Injectable()
export class LocaleRepository
{
	private locales: LocaleEntity[] = [];

	constructor(private rest: LocaleRESTService) {
	}

	fetch(): Observable<ListLocalesResponse200> {
		let observable = this.rest.list();

		observable.subscribe(response => {
			this.locales = response.locales;
		});

		return observable;
	}

	getAll(): LocaleEntity[] {
		return this.locales;
	}

	getByRegion(region: string): LocaleEntity {
		let result = this.locales.filter(locale => locale.region === region);

		if(result.length === 0) {
			throw new Error(`Locale with code "${region}" not found`);
		}

		return result[0];
	}

	addLocale(entity: LocaleEntity) {
		this.locales.push(entity);
	}

	removeLocale(entityId: number) {
		this.locales = this.locales.filter(entity => entity.id !== entityId);
	}

	replaceLocale(entityId: number, entity) {
		this.locales = this.locales.map(locale => {
			if(locale.id === entityId) {
				return entity;
			} else {
				return locale;
			}
		})
	}
}