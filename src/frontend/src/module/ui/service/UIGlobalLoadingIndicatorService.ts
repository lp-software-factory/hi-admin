import {Injectable} from "@angular/core";
import {LoadingManager, LoadingStatus} from "../../common/classes/LoadingStatus";

@Injectable()
export class UIGlobalLoadingIndicatorService
{
	private status: LoadingManager = new LoadingManager();

	public createLoadingIndicator(): LoadingStatus {
		return this.status.addLoading();
	}

	public isLoading(): boolean {
		return this.status.isLoading();
	}
}