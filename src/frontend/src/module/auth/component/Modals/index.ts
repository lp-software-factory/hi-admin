import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthModalsService} from "./service";

@Component({
	selector: 'hi-auth-modals',
	template: require('./template.jade')
})
export class AuthModals
{
	constructor(private router: Router,
		private service: AuthModalsService) {
	}

	private navigateProtected() {
		this.service.closeAllModals();
		this.router.navigate(['/admin/protected']);
	}

	private navigateAuth() {
		this.router.navigate(['/admin/public']);
	}
}