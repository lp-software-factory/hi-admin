import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {TranslationService} from "../../../../i18n/service/TranslationService";
import {ProductEntity} from "../../../../../../definitions/src/definitions/products/entity/ProductEntity";
import {ProductRESTService} from "../../../../../../definitions/src/services/product/ProductRESTService";


@Component({
	selector: 'hi-product-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteProductModal implements AfterViewInit
{
	@Input('product') product: ProductEntity;
	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: ProductRESTService,
		private t: TranslationService) {
	}


	getProduct(): string {
		return this.t.localize(this.product.title);
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				clearInterval(interval);
			}
		}, 1000);
	}


	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let productId = this.product.id;
		let loading = this.status.addLoading();

		this.service.deleteProduct(productId).subscribe(() => {
			this.successEvent.emit(productId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}