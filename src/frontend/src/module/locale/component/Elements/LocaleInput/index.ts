import {Component, EventEmitter, Output, Input, OnInit} from "@angular/core";
import {LocaleService} from "../../../service/LocaleService";
import {LocalizedString} from "../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

@Component({
	selector: 'hi-locale-input',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class LocaleInput implements OnInit
{
	@Output() valueChange = new EventEmitter<LocalizedString[]>();

	@Input()
	get value() {
		return this.localeStrings;
	}

	set value(val) {
		this.localeStrings = val;
		this.valueChange.emit(val);
	}

	private localeStrings: LocalizedString[];
	private shadowLocaleStrings: LocalizedString[];

	constructor(private localeService: LocaleService) {
	}

	ngOnInit() {
		this.initTranslations();
	}

	initTranslations() {
		let map: { [region: string]: LocalizedString } = {};

		this.shadowLocaleStrings = [];

		this.localeService.locales.forEach(locale => {
			map[locale.region] = {
				region: locale.region,
				value: ''
			};

			this.shadowLocaleStrings.push(map[locale.region]);
		});

		this.value.forEach(localizedString => {
			if(map.hasOwnProperty(localizedString.region)) {
				map[localizedString.region].value = localizedString.value;
			} else {
				this.shadowLocaleStrings.push(JSON.parse(JSON.stringify(localizedString)));
			}
		});
	}

	getFlagForLocaleString(localeString: LocalizedString) {
		return localeString.region.split('_')[1].toUpperCase();
	}

	buildLocalizedStrings() {
		this.localeStrings = [];

		for(let shadowLocaleString of this.shadowLocaleStrings) {
			let region = shadowLocaleString.region;
			let value = shadowLocaleString.value;

			if(!(value === undefined || value === null || value.toString() === '')) {
				this.localeStrings.push({
					region: region,
					value: value
				});
			}
		}

		this.valueChange.emit(this.value);
	}

	checkInput(item) {
		if(item.value === '') {
			item = undefined;
		}
	}
}