import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CurrencyEntity} from "../../../../../../definitions/src/definitions/money/entity/CurrencyEntity";

@Component({
	selector: 'hi-currency-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CurrencyDataTable
{
	@Input('entities') entities: CurrencyEntity[] = [];

	constructor(private cursor: DTCursor<CurrencyEntity>,) {
	}
}