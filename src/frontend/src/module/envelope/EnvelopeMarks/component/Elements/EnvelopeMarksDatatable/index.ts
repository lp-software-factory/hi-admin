import {Component, Input} from "@angular/core";
import {EnvelopeMarksEntity} from "../../../../../../../definitions/src/definitions/envelope/marks/entity/EnvelopeMarks";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-envelope-marks-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeMarksDatatable
{
	@Input('entities') entities: EnvelopeMarksEntity[] = [];

	constructor(private cursor: DTCursor<EnvelopeMarksEntity>) {}
}