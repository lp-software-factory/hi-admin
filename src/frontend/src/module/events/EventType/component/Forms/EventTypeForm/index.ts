import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";

export enum EventTypeFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export interface EventTypeFormModel
{
	group_id: number;
	url: string;
	title: LocalizedString[];
	description: LocalizedString[];
}

@Component({
	selector: 'hi-event-type-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EventTypeForm
{
	@Output() modelChange: EventEmitter<EventTypeFormModel> = new EventEmitter<EventTypeFormModel>();

	@Input('mode') mode: EventTypeFormMode = EventTypeFormMode.Create;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	public _model: EventTypeFormModel;
}