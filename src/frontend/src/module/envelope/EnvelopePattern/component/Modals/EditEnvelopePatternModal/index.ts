import {Component, EventEmitter, Output, Input, ViewChild} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopePatternEntity, EnvelopePattern} from "../../../../../../../definitions/src/definitions/envelope/pattern/entity/EnvelopePattern";
import {EnvelopePatternFormModel, EnvelopePatternForm} from "../../Forms/EnvelopePatternForm/index";
import {Observable} from "rxjs/Observable";
import {EditEnvelopePatternRequest} from "../../../../../../../definitions/src/definitions/envelope/pattern/path/edit";
import {UploadAttachmentResponse200} from "../../../../../../../definitions/src/definitions/attachment/entity/paths/upload";
import {EnvelopePatternRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-patterns/EnvelopePatternRESTService";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";

@Component({
	selector: 'hi-envelope-pattern-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditEnvelopePatternModal
{
	@Input('envelope_pattern') envelope_pattern: EnvelopePatternEntity;

	@Output('success') successEvent: EventEmitter<EnvelopePattern> = new EventEmitter<EnvelopePattern>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	
	@ViewChild('envelopePatternForm') envelopePatternForm: EnvelopePatternForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopePatternFormModel();

	constructor(private service: EnvelopePatternRESTService,
			private attachmentService: AttachmentRESTService,
				private l: LoadingManager,
	) {
	}

	ngOnInit() {
		console.log(this.formModel, this.envelope_pattern);
		this.formModel.title = JSON.parse(JSON.stringify(this.envelope_pattern.title));
		this.formModel.description = JSON.parse(JSON.stringify(this.envelope_pattern.description));
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 &&
			this.formModel.preview_file !== undefined && this.formModel.texture_file !== undefined &&
			!this.status.isLoading();
	}

	checkChanges(): boolean{
		return JSON.stringify(this.formModel.title) !== JSON.stringify(this.envelope_pattern.title) ||
			JSON.stringify(this.formModel.description) !== JSON.stringify(this.envelope_pattern.description)
	}

	isSubmitButtonActive(): boolean {
		return this.checkRequiredFields() && this.checkChanges();
	}

	cancel() {
		this.cancelEvent.emit();
	}
	
	submit($event) {
		$event.preventDefault();
		
		this.envelopePatternForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();
			
			this.service.editEnvelopePattern(this.envelope_pattern.id, request).subscribe(
				response => {
					loading.is = false;
					
					this.successEvent.emit(response.envelope_pattern);
				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}