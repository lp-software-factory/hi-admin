import {Component} from "@angular/core";

@Component({
	selector: 'hi-loading-linear-indicator',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	]
})
export class LoadingLinearIndicator
{
}