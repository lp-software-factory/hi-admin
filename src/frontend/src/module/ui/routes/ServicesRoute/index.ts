import {Component} from "@angular/core";

import {HIProductModuleRoutes} from "../../../products/module";
import {HIProductsPackageRoutes} from "../../../products-package/module";

@Component({
	template: '<router-outlet></router-outlet>'
})
export class ServicesRoute
{
}

export const HIServicesRoutes = {
	path: 'services',
	component: ServicesRoute,
	children: [
		HIProductModuleRoutes,
		HIProductsPackageRoutes,
	]
};