import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {EventTypeEntity} from "../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EventTypeRESTService} from "../../../../../../../definitions/src/services/event/event-type/EventTypeRESTService";

@Component({
	selector: 'hi-event-type-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteEventTypeModal implements AfterViewInit
{
	@Input('event_type') event_type: EventTypeEntity;

	@Output('success') successEvent: EventEmitter<number> = new EventEmitter<number>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3;
	/* seconds */
	private status: LoadingManager = new LoadingManager();

	constructor(private service: EventTypeRESTService) {
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	getEventTypeName(): string {
		return this.event_type.title[0].value;
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let eventTypeId = this.event_type.id;
		let loading = this.status.addLoading();

		this.service.deleteEventType(eventTypeId).subscribe(() => {
			this.successEvent.emit(eventTypeId);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}