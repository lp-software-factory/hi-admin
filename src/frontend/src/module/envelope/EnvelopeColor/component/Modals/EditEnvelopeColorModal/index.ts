import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeColorFormModel} from "../../Forms/EnvelopeColorForm/index";
import {EnvelopeColorEntity} from "../../../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {EnvelopeColorRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-color/EnvelopeColorRESTService";

@Component({
	selector: 'hi-envelope-color-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditEnvelopeColorModal
{
	@Input('envelope_color') envelope_color: EnvelopeColorEntity;

	@Output('success') successEvent: EventEmitter<EnvelopeColorEntity> = new EventEmitter<EnvelopeColorEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeColorFormModel();

	constructor(private service: EnvelopeColorRESTService) {
	}

	ngOnInit() {
		this.formModel.hex_code = this.envelope_color.hex_code;
		this.formModel.title = JSON.parse(JSON.stringify(this.envelope_color.title));
		this.formModel.description = JSON.parse(JSON.stringify(this.envelope_color.description));
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && ! this.status.isLoading();
	}

	checkChanges(): boolean {
		return this.formModel.hex_code !== this.envelope_color.hex_code ||
			JSON.stringify(this.formModel.title) !== JSON.stringify(this.envelope_color.title) ||
			JSON.stringify(this.formModel.description) !== JSON.stringify(this.envelope_color.description)
	}

	isSubmitButtonActive(): boolean {
		return this.checkRequiredFields() && this.checkChanges();
	}

	submit() {
		let loading = this.status.addLoading();
		this.service.editEnvelopeColor(this.envelope_color.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.envelope_color.entity);
			},
			error => {
				loading.is = false;
			}
		);
	}
}