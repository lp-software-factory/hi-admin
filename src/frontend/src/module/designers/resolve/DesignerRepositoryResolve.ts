import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

import {UIGlobalLoadingIndicatorService} from "../../ui/service/UIGlobalLoadingIndicatorService";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {DesignerRepository} from "../repository/DesignerRepository";

@Injectable()
export class DesignerRepositoryResolve
{
	constructor(private repository: DesignerRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}