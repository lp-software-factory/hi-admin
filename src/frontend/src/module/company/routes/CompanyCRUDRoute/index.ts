import {Component} from "@angular/core";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../crud/helpers/generic-modals/helper";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {CompanyEntity} from "../../../../../definitions/src/definitions/company/entity/CompanyEntity";
import {CompanyRepository} from "../../repository/CompanyRepository";
import {CompanyRESTService} from "../../../../../definitions/src/services/company/CompanyRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		DTCursor,
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		{
			provide: CRUDRepository,
			useExisting: CompanyRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: CompanyRESTService
		},
	],
})
export class CompanyCRUDRoute
{
	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<CompanyEntity>,
		private m: CRUDGenericModalsHelper,
		private r: CompanyRepository) {
	}

	getRows(): CompanyEntity[] {
		return this.r.getAll();
	}
}