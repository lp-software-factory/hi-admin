import {Component, EventEmitter, Output, Input, ViewChild} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeTextureFormModel, EnvelopeTextureForm} from "../../Forms/EnvelopeTextureForm/index";
import {EnvelopeTextureEntity} from "../../../../../../../definitions/src/definitions/envelope/texture/entity/EnvelopeTexture";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {EnvelopeAdditionalTextureFormModel} from "../../Forms/EnvelopeAdditionalTextureForm/index";
import {clone} from "../../../../../common/functions/clone";
import {EnvelopeTextureRESTService} from "../../../../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";
import {EnvelopeTextureDefinition} from "../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

@Component({
	selector: 'hi-envelope-texture-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
	providers: [
		DTCursor
	]
})
export class EditEnvelopeTextureModal
{
	@Input('envelope_texture') envelope_texture: EnvelopeTextureEntity;

	@Output('success') successEvent: EventEmitter<EnvelopeTextureEntity> = new EventEmitter<EnvelopeTextureEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('envelopeTextureForm') envelopeTextureForm: EnvelopeTextureForm;

	private status: LoadingManager = new LoadingManager();
	private formModel = new EnvelopeTextureFormModel();

	constructor(
		private service: EnvelopeTextureRESTService,
		private l: LoadingManager,
		private m: CRUDGenericModalsHelper,
		private cursor: DTCursor<EnvelopeTextureDefinition>,
	) {}

	ngOnInit() {
		this.formModel.title = clone(this.envelope_texture.title);
		this.formModel.description = clone(this.envelope_texture.description);
		this.formModel.previewModel = {
			files: {
				preview: undefined,
			},
			uploaded: {
				preview: this.envelope_texture.preview
			}
		};
		this.formModel.definitions = this.envelope_texture.definitions.map(definition => {
			return clone({
				files: {
					preview: undefined,
				},
				uploaded: {
					preview: definition.entity.preview
				}
			});
		});
	}

	checkChanges(): boolean {
		return JSON.stringify(this.formModel.title) !== JSON.stringify(this.envelope_texture.title) ||
			    JSON.stringify(this.formModel.description) !== JSON.stringify(this.envelope_texture.description) ||
				JSON.stringify(this.formModel.definitions) !== JSON.stringify(this.envelope_texture.definitions);
	}

	isSubmitButtonActive(): boolean {
		return this.checkRequiredFields() && this.checkChanges();
	}

	cancel() {
		this.cancelEvent.emit();
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0 && this.formModel.definitions.length > 0 && ! this.status.isLoading();
	}

	addDefinition(entity: EnvelopeAdditionalTextureFormModel) {
		this.formModel.definitions.push(entity);
	}

	replaceDefinition(entity: EnvelopeAdditionalTextureFormModel) {
		this.formModel.definitions = this.formModel.definitions.map(compare => {
			return compare; // TODO: fix or remove
		})
	}

	removeDefinition() {
		this.formModel.definitions = this.formModel.definitions.filter(texture => JSON.stringify(texture) !== JSON.stringify(this.cursor.current()));
		this.cursor._current = undefined;
	}

	submit($event) {
		$event.preventDefault();

		this.envelopeTextureForm.createRequest().subscribe(request => {
			let loading = this.l.addLoading();

			this.service.editEnvelopeTexture(this.envelope_texture.id, request).subscribe(
				response => {
					loading.is = false;

					this.successEvent.emit(response.envelope_texture.entity);
				},
				error => {
					loading.is = false;
				}
			);
		});
	}
}