import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EventTypeEntity} from "../../../../../../../definitions/src/definitions/events/event-type/entity/EventTypeEntity";

@Component({
	selector: 'hi-event-type-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EventTypeDataTable
{
	@Input('entities') entities: EventTypeEntity[] = [];

	constructor(private cursor: DTCursor<EventTypeEntity>,) {
	}

	getCursor(): DTCursor<EventTypeEntity> {
		return this.cursor;
	}
}