import {Component, EventEmitter, Output, Input} from "@angular/core";
import {InviteCardStyleEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";


@Component({
	selector: 'hi-card-style-selector',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CardStyleSelector
{
	@Input('chosenCardStyle') chosenCardStyle: number;
	@Input('card_styles') card_styles: Array<InviteCardStyleEntity>;
	@Output('changeCardStyle') changeCardStyle: any = new EventEmitter<any>();

	constructor() {
	}

	getCardStyleId(cardStyle: InviteCardStyleEntity) {
		let id = cardStyle.id;
		this.changeCardStyle.emit(id);
	}

	changeValue(event) {
		this.changeCardStyle.emit(event);
	}
}


interface InviteCardStyleTableEntity extends InviteCardStyleEntity
{
	localizedTitle: string;
	localizedDescription: string;
}
