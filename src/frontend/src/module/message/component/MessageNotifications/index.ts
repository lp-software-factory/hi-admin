import {Component} from "@angular/core";
import {MessageService, Message, MESSAGE_LEVEL} from "../../service/MessageService";

@Component({
	selector: 'hi-message-notifications',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class MessageNotifications
{
	constructor(private service: MessageService) {
	}

	getMessages(): Message[] {
		return this.service.list().slice(0, 5);
	}

	remove(message: Message) {
		this.service.remove(message.id);
	}

	getNgClass(message: Message) {
		let css = {};

		css[this.getCSSClassName(message)] = true;

		return css;
	}

	getCSSClassName(message: Message): string {
		switch(message.level) {
			default:
				return '';
			case MESSAGE_LEVEL.Primary:
				return 'is-primary';
			case MESSAGE_LEVEL.Danger:
				return 'is-danger';
			case MESSAGE_LEVEL.Info:
				return 'is-info';
			case MESSAGE_LEVEL.Success:
				return 'is-success';
			case MESSAGE_LEVEL.Warning:
				return 'is-warning';
		}
	}
}