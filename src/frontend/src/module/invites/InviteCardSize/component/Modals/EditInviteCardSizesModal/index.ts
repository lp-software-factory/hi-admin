import {Component, EventEmitter, Output, Input} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardSizeEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {InviteCardSizeFormModel} from "../../Forms/InviteCardSizeForm/index";
import {InviteCardSizeRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";


@Component({
	selector: 'hi-invite-card-size-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditInviteCardSizeModal
{
	@Input('invite_card_size') invite_card_size: InviteCardSizeEntity;
	@Output('success') successEvent: EventEmitter<InviteCardSizeEntity> = new EventEmitter<InviteCardSizeEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();


	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardSizeFormModel();

	constructor(private service: InviteCardSizeRESTService) {
	}


	ngOnInit() {
		this.initInviteCardSize();
	}

	initInviteCardSize() {
		this.formModel = JSON.parse(JSON.stringify(this.invite_card_size));
	}

	cancel() {
		this.initInviteCardSize();
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();


		this.service.editInviteCardSize(this.invite_card_size.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.card_size);
			},
			error => {
				loading.is = false;
			}
		);
	}
}