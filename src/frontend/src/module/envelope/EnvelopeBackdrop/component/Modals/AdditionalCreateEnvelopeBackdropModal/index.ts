import {Component, EventEmitter, Output, ViewChild} from "@angular/core";

import {EnvelopeAdditionalBackdropFormModel, EnvelopeAdditionalBackdropForm} from "../../Forms/EnvelopeAdditionalBackdropForm/index";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";

@Component({
	selector: 'hi-envelope-backdrop-additional-crud-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalCreateEnvelopeBackdropModal
{
	@ViewChild('definitionForm') definitionForm: EnvelopeAdditionalBackdropForm;

	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalBackdropFormModel> = new EventEmitter<EnvelopeAdditionalBackdropFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();

	private formModel: EnvelopeAdditionalBackdropFormModel = new EnvelopeAdditionalBackdropFormModel();

	cancel() {
		this.cancelEvent.emit()
	}
	
	submitButtonActive(): boolean {
		if(this.formModel.files !== undefined) {
			return this.formModel.files.resource !== undefined && this.formModel.files.preview !== undefined;
		} else if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.resource !== undefined && this.formModel.uploaded.preview !== undefined
		}
	}

	submit() {
		this.successEvent.emit(this.formModel);
	}
}