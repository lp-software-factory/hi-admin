import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {EnvelopeBackdropRepository} from "../repository/EnvelopeBackdropRepository";
import {EnvelopeBackdropGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/backdrop/path/get-all";

@Injectable()
export class EnvelopeBackdropRepositoryResolve implements Resolve<EnvelopeBackdropGetAllResponse200>
{
	constructor(private repository: EnvelopeBackdropRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}