import {Observable} from "rxjs";
import {CRUDSerialRepositoryInterface, CRUDSerialEntityRESTService, UpdatePositionResponse200} from "../helpers/position/interfaces";

export abstract class CRUDRepository implements CRUDSerialRepositoryInterface
{
	abstract getTotal();

	abstract moveUp(id: number, oldPosition: number, newPosition: number): void;

	abstract moveDown(id: number, oldPosition: number, newPosition: number): void;
}

export abstract class CRUDRESTService implements CRUDSerialEntityRESTService
{
	abstract moveEntityUp(id: number): Observable<UpdatePositionResponse200>;

	abstract moveEntityDown(id: number): Observable<UpdatePositionResponse200>;
}