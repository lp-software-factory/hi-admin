import {Component, Output, EventEmitter, Input} from "@angular/core";
import {ScreenControls} from "../../../common/classes/ScreenControls";
import {UploadImageService} from "./service";
import {UploadImageCropModel} from "./strategy";
import {ImageCropperService} from "../../../common/component/ImageCropper/index";
import {InviteCardSizeEntity} from "../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";

enum UploadImageScreen {
	File = <any>"File",
	Crop = <any>"Crop",
	Processing = <any>"Processing",
}

@Component({
	selector: 'hi-upload-image-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		ImageCropperService,
	],
})

export class UploadImageModal
{
	public progress = new UploadProgress();

	private screen: ScreenControls<UploadImageScreen> = new ScreenControls<UploadImageScreen>(UploadImageScreen.File, (sc: ScreenControls<UploadImageScreen>) => {
		sc.add({
			from: UploadImageScreen.File,
			to: UploadImageScreen.Crop
		});
	});

	@Input('crop_size') crop_size: InviteCardSizeEntity;
	@Output('close') closeEvent = new EventEmitter<UploadImageModal>();
	@Output('crop') cropEvent = new EventEmitter<UploadImageModal>();
	@Output('processing') processingEvent = new EventEmitter<UploadImageModal>();
	@Output('abort') abortEvent = new EventEmitter<UploadImageModal>();

	constructor(public cropper: ImageCropperService,
		public service: UploadImageService) {
	}

	private onFileChange($event): void {
		console.log($event);
		this.cropper.setFile($event.target.files[0]);
		this.cropper.setSize(this.crop_size);
		this.screen.goto(UploadImageScreen.Crop);
		this.cropEvent.emit(this);
	}

	process() {
		let model: UploadImageCropModel = {
			x: this.cropper.getX(),
			y: this.cropper.getY(),
			width: this.cropper.getWidth(),
			height: this.cropper.getHeight()
		};

		this.processingEvent.emit(this);

		this.service.process(this.cropper.getFile(), model, this);
	}

	close() {
		this.closeEvent.emit(this);
	}
}

class UploadProgress
{
	public value: number = 0;

	public reset() {
		this.value = 0;
	}

	abort() {
		this.value = 0;
	}

	complete() {
		this.value = 100;
	}

	update(value: number) {
		if(value < 0 || value > 100) {
			throw new Error(`Invalid progress ${value}`);
		}

		this.value = value;
	}
}