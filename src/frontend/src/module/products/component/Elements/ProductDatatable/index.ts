import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {TranslationService} from "../../../../i18n/service/TranslationService";
import {ProductEntity} from "../../../../../../definitions/src/definitions/products/entity/ProductEntity";

@Component({
	selector: 'hi-product-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class ProductDataTable
{
	@Input('entities') entities: ProductEntity[] = [];

	constructor(private t: TranslationService,
		private cursor: DTCursor<ProductEntity>) {
	}

	getCursor(): DTCursor<ProductEntity> {
		return this.cursor;
	}
}