import {Router} from "@angular/router";
import {Component, OnInit} from "@angular/core";
import {EventTypeGroupEntity} from "../../../../../../definitions/src/definitions/events/event-type-group/entity/EventTypeGroupEntity";
import {EventTypeGroupRepository} from "../../../EventTypeGroup/repository/EventTypeGroupRepository";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	],
})
export class EventTypeGatewayRoute implements OnInit
{
	constructor(private router: Router,
		private repository: EventTypeGroupRepository,) {
	}

	ngOnInit(): void {
		this.router.navigate(['/admin/protected/catalogs/event_type/', this.repository.getAll().sort((a, b) => {
			return a.position - b.position;
		})[0].id]);
	}

	getGroups(): EventTypeGroupEntity[] {
		return this.repository.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}