import {Component, EventEmitter, Output, Input} from "@angular/core";

import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {EnvelopeAdditionalMarksFormModel} from "../../Forms/EnvelopeAdditionalMarksForm/index";

@Component({
	selector: 'hi-envelope-marks-additional-crud-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AdditionalEditEnvelopeMarksModal
{
	@Output('success') successEvent: EventEmitter<EnvelopeAdditionalMarksFormModel> = new EventEmitter<EnvelopeAdditionalMarksFormModel>();
	@Output('cancel') cancelEvent: EventEmitter<EnvelopeAdditionalMarksFormModel> = new EventEmitter<EnvelopeAdditionalMarksFormModel>();
	
	@Input('entity') formModel: EnvelopeAdditionalMarksFormModel;

	private status: LoadingManager = new LoadingManager();
	private tmpFormModel: EnvelopeAdditionalMarksFormModel;

	ngOnInit() {
		this.tmpFormModel = JSON.parse(JSON.stringify(this.formModel));
	}

	cancel() {
		this.cancelEvent.emit(this.tmpFormModel);
	}

	submitButtonActive(): boolean {
		if(this.formModel.uploaded !== undefined) {
			return this.formModel.uploaded.attachment !== undefined
		}
	}

	submit() {
		console.log("test", this.formModel);
		this.successEvent.emit(this.formModel);
	}
}