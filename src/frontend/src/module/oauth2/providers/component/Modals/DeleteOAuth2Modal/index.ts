import {Component, Input, Output, EventEmitter, AfterViewInit} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {OAuth2ProviderEntity} from "../../../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";
import {OAuth2ProvidersRESTService} from "../../../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Component({
	selector: 'hi-oauth2-provider-delete-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class DeleteOAuth2ProvidersModal implements AfterViewInit
{
	@Input('entity') entity: OAuth2ProviderEntity;

	@Output('success') successEvent: EventEmitter<string> = new EventEmitter<string>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private buttonDisabledFor: number = 3 /* seconds */;
	private status: LoadingManager = new LoadingManager();

	constructor(private service: OAuth2ProvidersRESTService) {
	}

	getProviderCode(): string {
		return this.entity.code;
	}

	ngAfterViewInit() {
		let interval = setInterval(() => {
			if(this.buttonDisabledFor > 0) {
				--this.buttonDisabledFor;
			} else {
				//noinspection TypeScriptUnresolvedFunction
				clearInterval(interval);
			}
		}, 1000);
	}

	isSubmitButtonDisabled(): boolean {
		return this.buttonDisabledFor > 0;
	}

	submit() {
		let OAuth2ProviderCode = this.entity.code;
		let loading = this.status.addLoading();

		this.service.deleteOAuth2Providers(OAuth2ProviderCode).subscribe(() => {
			this.successEvent.emit(OAuth2ProviderCode);

			loading.is = false;
		});
	}

	cancel() {
		this.cancelEvent.emit();
	}
}