import {Injectable} from "@angular/core";

@Injectable()
export class DTCursor<T>
{
	public _current: T;

	current(): T {
		if(!this.has()) {
			throw new Error('Nothing selected');
		}

		return this._current;
	}

	blur(): void {
		this._current = undefined;
	}

	select(entity: T) {
		this._current = entity;
	}

	has(): boolean {
		return this._current !== undefined;
	}

	isActive(entity: T) {
		return this._current === entity;
	}
}