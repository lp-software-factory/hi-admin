import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

import {GetAllDesignersResponse200} from "../../../../definitions/src/definitions/designers/path/get-all";
import {DesignerEntity} from "../../../../definitions/src/definitions/designers/entity/DesignerEntity";
import {DesignerRESTService} from "../../../../definitions/src/services/designer/DesignerRESTService";

@Injectable()
export class DesignerRepository
{
    private requests: DesignerEntity[] = [];

    constructor(private rest: DesignerRESTService) {
    }

    fetch(): Observable<GetAllDesignersResponse200> {
        let observable = this.rest.getAllDesigners();

        observable.subscribe(response => {
            this.requests = response.designers;
        });

        return observable;
    }

    getAll(): DesignerEntity[] {
        return this.requests;
    }

    getDesignerName(designer: DesignerEntity) {
        return designer.profile.first_name + ' ' + designer.profile.last_name;
    }

    getById(id: number): DesignerEntity {
        let result = this.requests.filter(entity => entity.id === id);

        if(result.length === 0) {
            throw new Error(`Designer with ID "${id}" not found`);
        }

        return result[0];
    }

    getByProfileId(profileId: number) {
        let result = this.requests.filter(entity => entity.profile.id === profileId);

        if(result.length === 0) {
            throw new Error(`Designer with ProfileID "${profileId}" not found`);
        }

        return result[0];
    }
}