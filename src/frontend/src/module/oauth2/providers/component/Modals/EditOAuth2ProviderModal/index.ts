import {Component, EventEmitter, Output, Input, OnInit, OnChanges, SimpleChanges} from "@angular/core";
import {clone} from "../../../../../common/functions/clone";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {OAuth2ProviderEntity} from "../../../../../../../definitions/src/definitions/oauth2/providers/entity/OAuth2ProviderEntity";
import {OAuth2ProviderRepository} from "../../../repository/OAuth2ProvidersRepository";
import {OAuth2ProviderHandlersRepository} from "../../../repository/OAuth2ProviderHandlersRepository";
import {OAuth2ProviderFormModel} from "../../Forms/OAuth2ProviderForm/index";
import {OAuth2ProvidersRESTService} from "../../../../../../../definitions/src/services/oauth2-providers/OAuth2ProvidersRESTService";

@Component({
	selector: 'hi-oauth2-provider-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditOAuth2ProvidersModal implements OnInit, OnChanges
{
	@Input('entity') entity: OAuth2ProviderEntity;

	@Output('success') successEvent: EventEmitter<OAuth2ProviderEntity> = new EventEmitter<OAuth2ProviderEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: OAuth2ProviderFormModel = {
		code: '',
		handler: '',
		config: {
			clientId: '',
			clientSecret: '',
			redirectUri: ''
		}
	};

	constructor(private service: OAuth2ProvidersRESTService,
		private providers: OAuth2ProviderRepository,
		private handlers: OAuth2ProviderHandlersRepository,) {
	}

	ngOnInit(): void {
		if(this.hasAvailableHandlers()) {
			this.formModel.handler = this.getAvailableHandlers()[0];
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		let e = this.entity;

		this.formModel = {
			code: e.code,
			handler: e.handler,
			config: clone(e.config)
		};
	}

	hasAvailableHandlers(): boolean {
		return this.getAvailableHandlers().length > 0;
	}

	getAvailableHandlers(): string[] {
		let exists = this.providers.getAll()
			.filter(entity => entity.handler !== this.entity.handler)
			.map(entity => entity.handler)
			;

		return this.handlers.getAll().filter(handler => !~exists.indexOf(handler));
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.setOAuth2ProviderConfig(this.formModel.code, {config: this.formModel.config}).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.provider);
			},
			error => {
				loading.is = false;
			}
		);
	}
}