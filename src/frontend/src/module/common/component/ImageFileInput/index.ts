import {Component, Input, Output, EventEmitter} from "@angular/core";

import {MessageService, MESSAGE_LEVEL} from "../../../message/service/MessageService";
import {TranslationService} from "../../../i18n/service/TranslationService";
import {basename} from "../../functions/basename";
import {ModalControl} from "../../classes/ModalControl";

@Component({
    selector: 'hi-image-file-input',
    template: require('./template.jade'),
    styles: [
        require('./style.shadow.scss'),
    ],
})
export class ImageFileInput
{
    private _value: File;
    private enabled: boolean = true;
    private lastFileName: string = '';
    private lastObjectUrl: string;

    public modal: ModalControl = new ModalControl();

    @Input('preview') preview: string;
    @Input('allowed') allowed: string[] = ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'];
    @Output('change-file') changeEvent: EventEmitter<File> = new EventEmitter<File>();
    @Output() valueChange = new EventEmitter<File>();

    @Input()
    get value(): File {
        return this._value;
    }

    set value(val: File) {
        this._value = val;
        this.valueChange.emit(val);
        this.changeEvent.emit(val);
    }

    constructor(
        private messages: MessageService,
        private translate: TranslationService,
    ) {}
    
    onFileChange($event) {
        if($event.target.files.length) {
            let file: File = $event.target.files[0];

            if(!~this.allowed.indexOf(file.type)) {
                this.messages.push(MESSAGE_LEVEL.Danger, "common.image-file-input.not-an-image", true);

                this.enabled = false;

                setTimeout(() => {
                    this.enabled = true;
                }, 500);
            }

            this.value = file;
            this.lastFileName = file.name;
            this.lastObjectUrl = undefined;
        }
    }

    getTitle(): string {
        if(this.value instanceof Blob) {
            return basename(this.lastFileName);
        }else if(this.preview){
            return basename(this.preview);
        }else{
            return this.translate.translate('common.image-file-input.no-file-title');
        }
    }

    hasPreview(): boolean {
        return !!this.preview || !!this.value;
    }

    getPreview(): string {
        if(this.value instanceof Blob) {
            if(!this.lastObjectUrl) {
                this.lastObjectUrl = URL.createObjectURL(this.value);
            }

            return this.lastObjectUrl;
        }else{
            return this.preview;
        }
    }
}