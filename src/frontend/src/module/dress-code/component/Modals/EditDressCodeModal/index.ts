import {Component, EventEmitter, Output, Input, OnChanges, SimpleChanges} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {DressCodeFormModel} from "../../Forms/DressCodeForm/index";
import {DressCodeEntity} from "../../../../../../definitions/src/definitions/dress-code/entity/DressCode";
import {DressCodeRESTService} from "../../../../../../definitions/src/services/dress-code/DressCodeRESTService";

@Component({
	selector: 'hi-dress-code-edit-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EditDressCodeModal
{
	@Input('entity') dress_code: DressCodeEntity;

	@Output('success') successEvent: EventEmitter<DressCodeEntity> = new EventEmitter<DressCodeEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: DressCodeFormModel =  new DressCodeFormModel();

	constructor(private service: DressCodeRESTService) {
	}
	
	ngOnInit() {
		this.formModel.title = JSON.parse(JSON.stringify(this.dress_code.title));
		this.formModel.description = JSON.parse(JSON.stringify(this.dress_code.description));
		this.formModel.description_men = JSON.parse(JSON.stringify(this.dress_code.description_men));
		this.formModel.description_women = JSON.parse(JSON.stringify(this.dress_code.description_women));
		this.formModel.image_attachment_id = JSON.parse(JSON.stringify(this.dress_code.image.id));
	}
	

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.edit(this.dress_code.id, this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.dress_code.entity);
			},
			error => {
				loading.is = false;
			}
		);
	}
}