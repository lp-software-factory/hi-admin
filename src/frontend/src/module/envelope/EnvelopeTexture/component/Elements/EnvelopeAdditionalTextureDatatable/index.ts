import {Component, Input} from "@angular/core";

import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeAdditionalTextureFormModel} from "../../Forms/EnvelopeAdditionalTextureForm/index";
import {getEnvelopeTextureFileTitle} from "../../../functions/getFileTitle";

@Component({
	selector: 'hi-envelope-additional-texture-data-table',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeAdditionalTextureCrudDataTable
{
	@Input('entities') entities: EnvelopeAdditionalTextureFormModel[] = [];

	constructor(private cursor: DTCursor<EnvelopeAdditionalTextureFormModel>)
	{}
	
	getFileTitle(type: string, entity: EnvelopeAdditionalTextureFormModel) {
		return getEnvelopeTextureFileTitle(type, entity);
	}
}