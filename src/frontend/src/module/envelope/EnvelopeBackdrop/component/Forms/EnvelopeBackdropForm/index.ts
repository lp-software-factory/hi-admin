import {Observable} from "rxjs";
import {Component, EventEmitter, Output, Input} from "@angular/core";

import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {Validators} from "../../../../../common/functions/validators";
import {EnvelopeAdditionalBackdropFormModel} from "../EnvelopeAdditionalBackdropForm/index";
import {CreateEnvelopeBackdropRequest} from "../../../../../../../definitions/src/definitions/envelope/backdrop/request/EnvelopeBackdropRequests";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeBackdropDefinition} from "../../../../../../../definitions/src/definitions/envelope/backdrop-definition/entity/EnvelopeBackdropDefinition";
import {EnvelopeAdditionalTextureFormModel} from "../../../../EnvelopeTexture/component/Forms/EnvelopeAdditionalTextureForm/index";

export enum EnvelopeBackdropFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class EnvelopeBackdropFormModel
{
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	definitions: EnvelopeAdditionalBackdropFormModel[] = [];
	previewModel: EnvelopeAdditionalTextureFormModel = new EnvelopeAdditionalTextureFormModel();
}

@Component({
	selector: 'hi-envelope-backdrop-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeBackdropForm
{
	@Output() modelChange: EventEmitter<EnvelopeBackdropFormModel> = new EventEmitter<EnvelopeBackdropFormModel>();

	@Input('mode') mode: EnvelopeBackdropFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	private _model: EnvelopeBackdropFormModel = new EnvelopeBackdropFormModel();

	constructor(
		private l: LoadingManager,
		private m: CRUDGenericModalsHelper,
		private cursor: DTCursor<EnvelopeBackdropDefinition>,
		private attachments: AttachmentRESTService
	) {}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.localeInputNotEmpty(this.model.title) },
			() => { return Validators.localeInputNotEmpty(this.model.description) }
		];

		for(let definition of this.model.definitions) {
			validators.push(() => {
				return Validators.localeInputNotEmpty(definition.title)
					&& Validators.localeInputNotEmpty(definition.description)
					&& (definition.files.resource instanceof Blob)
					&& (definition.files.preview instanceof Blob)
			});
		}

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}

	onFilePreviewChange(event) {
		this.model.previewModel.files.preview = event.target.files[0];
	}

	deleteFile(type: string) {
		if(type === 'preview'){
			this.model.previewModel.uploaded.preview = undefined;
			this.model.previewModel.files.preview = undefined;
		}
	}

	createRequest(): Observable<CreateEnvelopeBackdropRequest> {
		return Observable.create(observer => {
			let request = {
				title: this.model.title,
				description: this.model.description,
				preview_attachment_id: -1
			};
			if(this.model.previewModel.uploaded.preview != undefined) {
				request.preview_attachment_id = this.model.previewModel.uploaded.preview.id;
				observer.next(request);
				observer.complete();
			} else if (this.model.previewModel.files.preview instanceof Blob) {
				let observable = this.attachments.upload(this.model.previewModel.files.preview);

				observable.subscribe(response => {
					let entity = response['entity'];
					if (entity) {
						request.preview_attachment_id = entity.id;
						observer.next(request);
						observer.complete();
					}
				});
			}
		})
	}
}
