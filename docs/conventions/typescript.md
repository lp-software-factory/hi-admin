TypeScript Code Conventions
===========================

- В декларациях класса `{` помещается на новую строку. Во всех остальных случаях `{` помещается на той же строке:

```
class MyClass 
{
    public myMethod() {
        if(! this.isUpdated) { // Важно: после ! следует пробел
            this.service.fetch(response => {
                // ...
            });
        }else{ // Важно! Здесь без пробелов!
            do {
                this.service.update();
            } while(this.limitExceed);
        }
    }
}
```

- Импорты всегда должны быть в одной строке и не содержать пробелов вокруг фигурных скобок:

```typescript
import {Component, Output, EventEmitter, ViewChild, ElementRef} from "@angular/core"; // Правильно

import { Component, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core"; // Неправильно!
import {
    Component, 
    Output, 
    EventEmitter, 
    ViewChild, 
    ElementRef
} from "@angular/core"; // Неправильно!

```

- Конструктор обязан иметь строго определенный (в плане пробелов, переводов строк и.т.д..) ниже вид:
```typescript
class MyClass implements OnInit
{
    // Далее идет конструктор
    constructor(
        private myService: MyService,
        private myHelper: MyHelper,
    ) {}
}
```

- Существует строго определенный порядок в описании свойств и методов класса:
```typescript
class MyClass implements OnInit
{
    // Сначала идет блок со статическими свойствами класса
    public static MY_STATIC_PROPERTY = '...';
    
    // Далее идет блок c @Input, @Otput, @ViewChild и прочими свойствами, обернутый @angular-декораторами
    @Input('my-input') myInput: string;
    @Input('my-another-input') myAnotherInpuy: number;
    
    @Output('open') openEvent: EventEmitter<MyEntity> = new EventEmitter(MyEntity);
    
    // Далее идет блок со свойствами класса
    private current: MyEntity;
    
    // Далее идет конструктор
    constructor(
        private myService: MyService,
        private myHelper: MyHelper,
    ) {}
    
    // Далее идут lifecycle-методы
    ngOnInit(): void {
        this.myService.update();
    }
    
    // И после, в самом конце, идут все остальные методы
    getAll(): MyEntity[] {
        return this.myHelper.getAllEntities();
    }
}
```

- В конструкторе компонентов не должно быть никаких инициализирующих инструкций. Используйте для инициализации 
возможность задания дефолтных значений свойств и lifecycle-методы, такие как `ngOnInit`, `ngOnChanges` и.т.д..