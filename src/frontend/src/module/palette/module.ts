import {PalettePresetsRepository} from "./repository/PalettePresetsRepository";

export const HIPaletteModule = {
	routes: [],
	declarations: [],
	providers: [
		PalettePresetsRepository,
	],
};