import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {FontFormModel} from "../../Forms/FontForm/index";
import {FontEntity, Font} from "../../../../../../definitions/src/definitions/font/entity/Font";
import {FontRESTService} from "../../../../../../definitions/src/services/font/FontRESTService";

@Component({
	selector: 'hi-font-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateFontModal
{
	@Output('success') successEvent: EventEmitter<Font> = new EventEmitter<Font>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel: FontFormModel = new FontFormModel();

	constructor(private service: FontRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.create(this.formModel).subscribe(
			response => {
				loading.is = false;
				this.successEvent.emit(response.font);
			},
			error => {
				loading.is = false;
			}
		);
	}
}