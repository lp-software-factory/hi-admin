import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {InviteCardStyleEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-style/entity/InviteCardStyleEntity";

@Component({
	selector: 'hi-invite-card-style-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class InviteCardStyleDatatable
{
	@Input('entities') entities: InviteCardStyleEntity[] = [];

	constructor(private cursor: DTCursor<InviteCardStyleEntity>) {
	}
}