import {EnvelopeTextureForm} from "./component/Forms/EnvelopeTextureForm/index";
import {EnvelopeTextureDatatable} from "./component/Elements/EnvelopeTextureDatatable/index";
import {CreateEnvelopeTextureModal} from "./component/Modals/CreateEnvelopeTextureModal/index";
import {DeleteEnvelopeTextureModal} from "./component/Modals/DeleteEnvelopeTextureModal/index";
import {EnvelopeTextureRepository} from "./repository/EnvelopeTextureRepository";
import {EnvelopeTextureRepositoryResolve} from "./resolve/EnvelopeTextureRepositoryResolve";
import {EnvelopeTextureCRUDRoute} from "./routes/EnvelopeTextureCRUDRoute/index";
import {EditEnvelopeTextureModal} from "./component/Modals/EditEnvelopeTextureModal/index";
import {EnvelopeAdditionalTextureCrudDataTable} from "./component/Elements/EnvelopeAdditionalTextureDatatable/index";
import {AdditionalCreateEnvelopeTextureModal} from "./component/Modals/AdditionalCreateEnvelopeTextureModal/index";
import {AdditionalEditEnvelopeTextureModal} from "./component/Modals/AdditionalEditEnvelopeTextureModal/index";
import {AdditionalDeleteEnvelopeTextureModal} from "./component/Modals/AdditionalDeleteEnvelopeTextureModal/index";
import {EnvelopeAdditionalTextureForm} from "./component/Forms/EnvelopeAdditionalTextureForm/index";
import {EnvelopeTextureRESTService} from "../../../../definitions/src/services/envelope/envelope-texture/EnvelopeTextureRESTService";
import {AttachmentRESTService} from "../../../../definitions/src/services/attachment/AttachmentRESTService";

export const HIEnvelopeTextureModule = {
	routes: [
		EnvelopeTextureCRUDRoute,
	],
	declarations: [
		AdditionalCreateEnvelopeTextureModal,
		AdditionalEditEnvelopeTextureModal,
		AdditionalDeleteEnvelopeTextureModal,
		EnvelopeAdditionalTextureForm,
		EnvelopeTextureForm,
		EnvelopeTextureDatatable,
		EnvelopeAdditionalTextureCrudDataTable,
		CreateEnvelopeTextureModal,
		EditEnvelopeTextureModal,
		DeleteEnvelopeTextureModal
	],
	providers: [
		EnvelopeTextureRepository,
		EnvelopeTextureRepositoryResolve
	],
};

export const HIEnvelopeTextureRoute = {
	path: 'texture',
	component: EnvelopeTextureCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EnvelopeTextureRepositoryResolve
	]
};