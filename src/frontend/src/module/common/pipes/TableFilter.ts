import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'filter'
})
@Injectable()
export class TableFilter implements PipeTransform {
	transform(entities: any[], searchStr: string): any {
		return entities.filter(input => input.entity.id.toString().indexOf(searchStr) !== -1 || input.entity.title.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1);
	}
}