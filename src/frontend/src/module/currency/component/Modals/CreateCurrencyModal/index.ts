import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CurrencyEntity} from "../../../../../../definitions/src/definitions/money/entity/CurrencyEntity";
import {CurrencyFormModel} from "../../Forms/CurrencyForm/index";
import {CurrencyRESTService} from "../../../../../../definitions/src/services/currency/CurrencyRESTService";

@Component({
	selector: 'hi-currency-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateCurrencyModal
{
	@Output('success') successEvent: EventEmitter<CurrencyEntity> = new EventEmitter<CurrencyEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new CurrencyFormModel();

	constructor(private service: CurrencyRESTService) {
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createCurrency(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.currency);
			},
			error => {
				loading.is = false;
			}
		);
	}
}