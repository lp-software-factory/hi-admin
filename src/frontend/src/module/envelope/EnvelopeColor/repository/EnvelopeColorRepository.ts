import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {EnvelopeColorEntity} from "../../../../../definitions/src/definitions/envelope/color/entity/EnvelopeColor";
import {EnvelopeColorGetAllResponse200} from "../../../../../definitions/src/definitions/envelope/color/path/get-all";
import {EnvelopeColorRESTService} from "../../../../../definitions/src/services/envelope/envelope-color/EnvelopeColorRESTService";

@Injectable()
export class EnvelopeColorRepository implements CRUDSerialRepositoryInterface
{
	private colors: EnvelopeColorEntity[] = [];

	constructor(private rest: EnvelopeColorRESTService) {
	}

	fetch(): Observable<EnvelopeColorGetAllResponse200> {

		let observable = this.rest.getAllEnvelopeColor();

		observable.subscribe(response => {
			this.colors = response.envelope_colors.map(entity => {
				return entity.entity
			});
		});

		return observable;
	}

	getAll(): EnvelopeColorEntity[] {
		return this.colors;
	}

	getById(id: number): EnvelopeColorEntity {
		let result = this.colors.filter(entity => entity.id === id);

		if(result.length === 0) {
			throw new Error(`Marks with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.colors.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.colors, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.colors, oldPosition, newPosition);
	}

	updatePosition(rows: Array<EnvelopeColorEntity>, oldPosition, newPosition) {
		let tempRow;

		for(let index = 0; index < rows.length; index++) {
			if(rows[index].position === newPosition) {
				tempRow = rows[index];
				for(let ind = 0; ind < rows.length; ind++) {
					if(rows[ind].position === oldPosition) {
						tempRow.position = rows[ind].position;
						rows[ind].position = newPosition;
						break;
					}
				}
				break;
			}
		}
	}

	add(element: EnvelopeColorEntity) {
		this.colors.push(element);
	}

	remove(id: number) {
		this.colors = this.colors.filter(entity => entity.id !== id);
	}

	replace(element: EnvelopeColorEntity) {
		this.colors = this.colors.map(entity => {
			if(entity.id === element.id) {
				return element;
			} else {
				return entity;
			}
		})
	}
}