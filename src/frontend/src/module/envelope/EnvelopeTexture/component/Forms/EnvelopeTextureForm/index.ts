import {Observable} from "rxjs/Observable";
import {Component, EventEmitter, Output, Input} from "@angular/core";

import {LocalizedString} from "../../../../../../../definitions/src/definitions/locale/definitions/LocalizedString";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {CRUDGenericModalsHelper} from "../../../../../crud/helpers/generic-modals/helper";
import {CRUDCurrentRecordHelper} from "../../../../../crud/helpers/current/helper";
import {Validators} from "../../../../../common/functions/validators";
import {EnvelopeAdditionalTextureFormModel} from "../EnvelopeAdditionalTextureForm/index";
import {AbstractEnvelopeTextureRequest} from "../../../../../../../definitions/src/definitions/envelope/texture/request/EnvelopeTextureRequests";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {AttachmentRESTService} from "../../../../../../../definitions/src/services/attachment/AttachmentRESTService";
import {EnvelopeTextureDefinition} from "../../../../../../../definitions/src/definitions/envelope/texture-definition/entity/EnvelopeTextureDefinition";

export enum EnvelopeTextureFormMode {
	Create = <any>"create",
	Edit = <any>"edit",
}

export class EnvelopeTextureFormModel
{
	title: LocalizedString[] = [];
	description: LocalizedString[] = [];
	previewModel: EnvelopeAdditionalTextureFormModel = new EnvelopeAdditionalTextureFormModel();
	definitions: EnvelopeAdditionalTextureFormModel[] = [];
}

@Component({
	selector: 'hi-envelope-texture-form',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeTextureForm
{
	@Output() modelChange: EventEmitter<EnvelopeTextureFormModel> = new EventEmitter<EnvelopeTextureFormModel>();

	@Input('mode') mode: EnvelopeTextureFormMode;

	@Input()
	get model() {
		return this._model;
	}

	set model(val) {
		this._model = val;
		this.modelChange.emit(val);
	}

	private _model: EnvelopeTextureFormModel = new EnvelopeTextureFormModel();

	constructor(
		private l: LoadingManager,
		private m: CRUDGenericModalsHelper,
		private cursor: DTCursor<EnvelopeTextureDefinition>,
		private attachments: AttachmentRESTService,
	) {}

	isValid(): boolean {
		let validators: { (): boolean }[] = [
			() => { return Validators.localeInputNotEmpty(this.model.title) },
			() => { return Validators.localeInputNotEmpty(this.model.description) },
			() => { return this.model.previewModel.files.preview !== undefined || this.model.previewModel.uploaded.preview !== undefined}
		];

		for(let definition of this.model.definitions) {
			validators.push(() => {
				return (definition.files.preview instanceof Blob)
			});
		}

		for(let v of validators) {
			if(! v()) {
				return false;
			}
		}

		return true;
	}

	createRequest(): Observable<AbstractEnvelopeTextureRequest> {
		return Observable.create(observer => {
			let request = {
				title: this.model.title,
				description: this.model.description,
				preview_attachment_id: -1
			};
			if(this.model.previewModel.uploaded.preview != undefined) {
				request.preview_attachment_id = this.model.previewModel.uploaded.preview.id;
				observer.next(request);
				observer.complete();
			} else if (this.model.previewModel.files.preview instanceof Blob) {
				let observable = this.attachments.upload(this.model.previewModel.files.preview);

				observable.subscribe(response => {
					let entity = response['entity'];
					if (entity) {
						request.preview_attachment_id = entity.id;
						observer.next(request);
						observer.complete();
					}
				});
			}
		})
	}
}
