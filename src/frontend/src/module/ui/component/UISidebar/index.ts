import {Component} from "@angular/core";
import {AuthModalsService} from "../../../auth/component/Modals/service";
import {LocaleService} from "../../../locale/service/LocaleService";

@Component({
	selector: 'hi-ui-sidebar',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class UISidebar
{
	constructor(private authModals: AuthModalsService,
		private localeService: LocaleService) {
	}

	changeCurrentLocale(event) {
		for(let i = 0; i < this.localeService.locales.length; i++) {
			if(this.localeService.locales[i].id === event) {
				this.localeService.changeCurrentLocale(this.localeService.locales[i].region);
			}
		}
	}

	signOut($event) {
		$event.preventDefault();
		$event.stopPropagation();

		this.authModals.signOut();

		return false;
	}
}