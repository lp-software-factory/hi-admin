import {Component} from "@angular/core";
import {DesignerRequestEntity} from "../../../../../definitions/src/definitions/designer-requests/entity/DesignerRequestEntity";
import {LoadingManager} from "../../../common/classes/LoadingStatus";
import {DesignerRequestsRepository} from "../../repository/DesignerRequestsRepository";
import {CRUDCurrentRecordHelper} from "../../../crud/helpers/current/helper";
import {DTCursor} from "../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../crud/provide/provides";
import {ModalControl} from "../../../common/classes/ModalControl";
import {DesignerRequestsRESTService} from "../../../../../definitions/src/services/designer-request/DesignerRequestsRESTService";


@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: DesignerRequestsRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: DesignerRequestsRESTService
		},
	]
})
export class DesignerRequestsRoute
{

	private help: string = require('./inline.help.md');

	private modals: { view: ModalControl } = {
		view: new ModalControl()
	};

	constructor(private l: LoadingManager,
		private c: CRUDCurrentRecordHelper<DesignerRequestEntity>,
		private r: DesignerRequestsRepository,
		private rest: DesignerRequestsRESTService) {
	}

	getEntities(): DesignerRequestEntity[] {
		return this.r.getAll();
	}

	isReviewed(): boolean {
		if(this.c.hasActiveEntity()) {
			return this.c.getCurrentEntity().is_reviewed;
		} else return true
	}

	accept() {
		let loading = this.l.addLoading();

		this.rest.acceptDesignerRequest(this.c.getCurrentEntity().id).subscribe(
			response => {
				loading.is = false;

				this.r.replaceDesignerRequest(response.designer_request.id, response.designer_request);
				this.r.updateRows();
			},
			error => {
				loading.is = false;
			}
		);
	}

	decline() {
		let loading = this.l.addLoading();

		this.rest.declineDesignerRequest(this.c.getCurrentEntity().id).subscribe(
			response => {
				loading.is = false;

				this.r.replaceDesignerRequest(response.designer_request.id, response.designer_request);
				this.r.updateRows();
			},
			error => {
				loading.is = false;
			}
		);
	}
}
