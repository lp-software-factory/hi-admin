import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {InviteCardStyleRepository} from "../repository/InviteCardStyleRepository";
import {GetAllInviteCardStylesResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-style/paths/get-all";

@Injectable()
export class InviteCardStyleRepositoryResolve implements Resolve<GetAllInviteCardStylesResponse200>
{
	constructor(private repository: InviteCardStyleRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}