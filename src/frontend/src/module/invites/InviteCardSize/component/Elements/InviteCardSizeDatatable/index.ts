import {Component, Input} from "@angular/core";
import {InviteCardSizeEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";

@Component({
	selector: 'hi-invite-card-size-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class InviteCardSizeDatatable
{
	@Input('entities') entities: InviteCardSizeEntity[] = [];

	constructor(private cursor: DTCursor<InviteCardSizeEntity>) {
	}
}