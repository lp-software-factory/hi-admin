import {InviteCardStyleCRUDRoute} from "./routes/InviteCardStyleCRUDRoute/index";
import {CreateInviteCardStyleModal} from "./component/Modals/CreateInviteCardStyleModal/index";
import {DeleteInviteCardStyleModal} from "./component/Modals/DeleteInviteCardStyleModal/index";
import {EditInviteCardStyleModal} from "./component/Modals/EditInviteCardStyleModal/index";
import {CardStyleSelector} from "./component/Elements/CardStyleSelector/index";
import {InviteCardStyleRepositoryResolve} from "./resolve/InviteCardStyleRepositoryResolve";
import {InviteCardStyleDatatable} from "./component/Elements/InviteCardStyleDatatable/index";
import {InviteCardStyleRepository} from "./repository/InviteCardStyleRepository";
import {InviteCardStyleForm} from "./component/Forms/InviteCardStyleForm/index";
import {InviteCardStyleRESTService} from "../../../../definitions/src/services/invite-card/invite-card-style/InviteCardStyleRESTService";

export const HIInviteCardStyleModule = {
	routes: [
		InviteCardStyleCRUDRoute,
	],
	declarations: [
		InviteCardStyleDatatable,
		InviteCardStyleForm,
		CreateInviteCardStyleModal,
		DeleteInviteCardStyleModal,
		EditInviteCardStyleModal,
		CardStyleSelector
	],
	providers: [
		InviteCardStyleRepository,
		InviteCardStyleRepositoryResolve
	],
};

export const HIInviteCardStyleRoute = {
	path: 'styles',
	component: InviteCardStyleCRUDRoute,
	pathMatch: 'full',
	resolve: [
		InviteCardStyleRepositoryResolve
	]
};