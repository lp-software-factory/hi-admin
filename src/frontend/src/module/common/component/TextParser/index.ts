import {Component, Input, ViewEncapsulation} from "@angular/core";

var md = require('markdown-it')({
	breaks: true,
	linkify: true,
	quotes: ">"
});

@Component({
	selector: 'hi-text-parser',
	styles: [
		require('./style.shadow.scss')
	],
	encapsulation: ViewEncapsulation.None,
	template: require('./template.jade')
})

export class TextParser
{
	@Input('text') text: string;

	parseText() {
		md.validateLink = (link) => {
			return true;
		};

		return md.render(this.text);
	}
}