import {Component} from "@angular/core";
import {InviteCardSizeEntity} from "../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {LoadingManager} from "../../../../common/classes/LoadingStatus";
import {CRUDPositionEntityHelper} from "../../../../crud/helpers/position/helper";
import {CRUDCurrentRecordHelper} from "../../../../crud/helpers/current/helper";
import {CRUDGenericModalsHelper} from "../../../../crud/helpers/generic-modals/helper";
import {InviteCardSizeRepository} from "../../repository/InviteCardSizeRepository";
import {DTCursor} from "../../../../datatable/helpers/DTCursor";
import {CRUDRepository, CRUDRESTService} from "../../../../crud/provide/provides";
import {InviteCardSizeRESTService} from "../../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss')
	],
	providers: [
		LoadingManager,
		CRUDCurrentRecordHelper,
		CRUDGenericModalsHelper,
		CRUDPositionEntityHelper,
		DTCursor,
		{
			provide: CRUDRepository,
			useExisting: InviteCardSizeRepository
		},
		{
			provide: CRUDRESTService,
			useExisting: InviteCardSizeRESTService
		},
	]
})

export class InviteCardSizeCRUDRoute
{

	private help: string = require('./inline.help.md');

	constructor(private l: LoadingManager,
		private p: CRUDPositionEntityHelper,
		private c: CRUDCurrentRecordHelper<InviteCardSizeEntity>,
		private m: CRUDGenericModalsHelper,
		private r: InviteCardSizeRepository) {
	}

	getEntities(): InviteCardSizeEntity[] {
		return this.r.getAll().sort((a, b) => {
			return a.position - b.position;
		});
	}
}

export interface InviteCardSizeTableEntity extends InviteCardSizeEntity
{
	localizedTitle: string;
	localizedDescription: string;
}
