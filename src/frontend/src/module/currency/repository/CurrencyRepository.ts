import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CurrencyEntity} from "../../../../definitions/src/definitions/money/entity/CurrencyEntity";
import {ListCurrencyResponse200} from "../../../../definitions/src/definitions/money/paths/list-currency";
import {CurrencyRESTService} from "../../../../definitions/src/services/currency/CurrencyRESTService";

@Injectable()
export class CurrencyRepository
{
	private currencies: CurrencyEntity[] = [];

	constructor(private rest: CurrencyRESTService) {
	}

	fetch(): Observable<ListCurrencyResponse200> {
		let observable = this.rest.getCurrenciesList();

		observable.subscribe(
			response => {
				this.currencies = response.currencies;
			}
		);

		return observable;
	}

	getAll(): CurrencyEntity[] {
		return this.currencies;
	}

	getById(id: number): CurrencyEntity {
		let result = this.currencies.filter(currency => currency.id === id);

		if(result.length === 0) {
			throw new Error(`Currency with ID "${id}" not found`)
		}

		return result[0];
	}

	getDefaultCurrency(): CurrencyEntity {
		if(this.currencies.length === 0) {
			throw new Error('No currencies available');
		}

		return this.currencies[0];
	}

	addCurrency(entity: CurrencyEntity) {
		this.currencies.push(entity);
	}

	deleteCurrency(id: number) {
		this.currencies = this.currencies.filter(currency => currency.id !== id);
	}

	replaceCurrency(entity: CurrencyEntity) {
		this.currencies = this.currencies.map(currency => {
			return currency.id === entity.id ? entity : currency;
		})
	}
}