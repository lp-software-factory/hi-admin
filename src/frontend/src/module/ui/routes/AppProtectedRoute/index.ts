import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../../../auth/service/AuthService";

@Component({
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class AppProtectedRoute
{
	constructor(private router: Router,
		private auth: AuthService,) {
		if(!auth.isSignedIn()) {
			router.navigate(['/admin/public']);
		}
	}
}