import {Component, Input} from "@angular/core";
import {DTCursor} from "../../../../../datatable/helpers/DTCursor";
import {EnvelopeBackdropEntity} from "../../../../../../../definitions/src/definitions/envelope/backdrop/entity/EnvelopeBackdrop";

@Component({
	selector: 'hi-envelope-backdrop-datatable',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class EnvelopeBackdropDatatable
{
	@Input('entities') entities: EnvelopeBackdropEntity[] = [];

	constructor(private cursor: DTCursor<EnvelopeBackdropEntity>) {
	}
}