import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

import {Font} from "../../../../definitions/src/definitions/font/entity/Font";
import {GetAllFontsResponse200} from "../../../../definitions/src/definitions/font/path/get-all";
import {FontRESTService} from "../../../../definitions/src/services/font/FontRESTService";

@Injectable()
export class FontRepository
{
	public static DEFAULT_OFFSET: number = 0;
	public static DEFAULT_LIMIT: number = 100;

	private entities: Font[] = [];

	constructor(private rest: FontRESTService) {
	}

	fetch(offset: number = FontRepository.DEFAULT_OFFSET, limit: number = FontRepository.DEFAULT_LIMIT): Observable<GetAllFontsResponse200> {
		let observable = this.rest.getAll();

		observable.subscribe(
			response => { this.entities = response.fonts; }
		);

		return observable;
	}

	getAll(): Font[] {
		return this.entities;
	}

	getById(id: number): Font {
		let result = this.entities.filter(font => font.entity.id === id);

		if(result.length === 0) {
			throw new Error(`font with ID "${id}" not found`)
		}

		return result[0];
	}

	add(entity: Font) {
		this.entities.push(entity);
	}

	remove(id: number) {
		this.entities = this.entities.filter(font => font.entity.id !== id);
	}

	replace(entity: Font) {
		this.entities = this.entities.map(font => {
			return entity.entity.id === font.entity.id ? entity : font;
		})
	}
}