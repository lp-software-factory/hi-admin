import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {CRUDSerialRepositoryInterface} from "../../../crud/helpers/position/interfaces";
import {InviteCardGamma} from "../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {GetAllInviteCardGammasResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-gamma/paths/get-all";
import {InviteCardGammaRESTService} from "../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";

@Injectable()
export class InviteCardGammaRepository implements CRUDSerialRepositoryInterface
{
	private gammas: InviteCardGamma[] = [];

	constructor(private rest: InviteCardGammaRESTService) {
	}

	fetch(): Observable<GetAllInviteCardGammasResponse200> {

		let observable = this.rest.getAllInviteCardGammas();

		observable.subscribe(response => {
			this.gammas = response.gammas;
		});

		return observable;
	}

	getAll(): InviteCardGamma[] {
		return this.gammas;
	}

	getById(id: number): InviteCardGamma {
		let result = this.gammas.filter(entity => entity.entity.id === id);

		if(result.length === 0) {
			throw new Error(`Gamma with ID "${id}" not found`);
		}

		return result[0];
	}

	getTotal(): number {
		return this.gammas.length;
	}

	moveUp(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.gammas, oldPosition, newPosition);
	}

	moveDown(id: number, oldPosition: number, newPosition: number): void {
		this.updatePosition(this.gammas, oldPosition, newPosition);
	}

	updatePosition(rows: Array<InviteCardGamma>, oldPosition, newPosition) {
		let tempRow;

		for(let index = 0; index < rows.length; index++) {
			if(rows[index].entity.position === newPosition) {
				tempRow = rows[index];
				for(let ind = 0; ind < rows.length; ind++) {
					if(rows[ind].entity.position === oldPosition) {
						tempRow.entity.position = rows[ind].entity.position;
						rows[ind].entity.position = newPosition;
						break;
					}
				}
				break;
			}
		}
	}

	add(gamma: InviteCardGamma) {
		this.gammas.push(gamma);
	}

	remove(id: number) {
		this.gammas = this.gammas.filter(entity => entity.entity.id !== id);
	}

	replace(gamma: InviteCardGamma) {
		this.gammas = this.gammas.map(entity => {
			if(entity.entity.id === gamma.entity.id) {
				return gamma;
			} else {
				return entity;
			}
		})
	}
}