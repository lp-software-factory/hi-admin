import {Component, EventEmitter, Output, Input} from "@angular/core";
import {UIGlobalLoadingIndicatorService} from "../../../../../ui/service/UIGlobalLoadingIndicatorService";
import {LocaleService} from "../../../../../locale/service/LocaleService";
import {InviteCardGamma} from "../../../../../../../definitions/src/definitions/invites/invite-card-gamma/entity/InviteCardGammaEntity";
import {InviteCardGammaRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-gamma/InviteCardGammaRESTService";


@Component({
	selector: 'hi-card-gamma-selector',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CardGammaSelector
{
	@Input('chosenCardGamma') chosenCardGamma: number;
	@Input('card_gammas') card_gammas: Array<InviteCardGamma>;
	@Output('changeCardGamma') changeCardGamma: any = new EventEmitter<any>();

	constructor(private localeService: LocaleService,
		private loading: UIGlobalLoadingIndicatorService,
		private inviteCardGammaRESTService: InviteCardGammaRESTService) {
	}

	getCardGammaId(card_gamma: any) {
		let id = card_gamma.id;
		this.changeCardGamma.emit(id);
	}

	changeValue(event) {
		this.changeCardGamma.emit(event);
	}
}


interface InviteCardGammaTableEntity extends InviteCardGamma
{
	localizedTitle: string;
	localizedDescription: string;
}
