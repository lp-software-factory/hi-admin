import {Component, EventEmitter, Output} from "@angular/core";
import {LoadingManager} from "../../../../../common/classes/LoadingStatus";
import {InviteCardSizeEntity} from "../../../../../../../definitions/src/definitions/invites/invite-card-sizes/entity/InviteCardSizeEntity";
import {InviteCardSizeFormModel} from "../../Forms/InviteCardSizeForm/index";
import {InviteCardSizeRESTService} from "../../../../../../../definitions/src/services/invite-card/invite-card-size/InviteCardSizeRESTService";

@Component({
	selector: 'hi-invite-card-size-create-modal',
	template: require('./template.jade'),
	styles: [
		require('./style.shadow.scss'),
	]
})
export class CreateInviteCardSizeModal
{
	@Output('success') successEvent: EventEmitter<InviteCardSizeEntity> = new EventEmitter<InviteCardSizeEntity>();
	@Output('cancel') cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	private status: LoadingManager = new LoadingManager();
	private formModel = new InviteCardSizeFormModel();

	constructor(private service: InviteCardSizeRESTService) {
	}

	checkRequiredFields(): boolean {
		return this.formModel.title.length > 0 && this.formModel.description.length > 0;
	}

	cancel() {
		this.cancelEvent.emit();
	}

	submit() {
		let loading = this.status.addLoading();

		this.service.createInviteCardSize(this.formModel).subscribe(
			response => {
				loading.is = false;

				this.successEvent.emit(response.card_size);
			},
			error => {
				loading.is = false;
			}
		);
	}
}