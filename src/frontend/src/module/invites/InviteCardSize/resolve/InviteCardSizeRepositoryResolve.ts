import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {UIGlobalLoadingIndicatorService} from "../../../ui/service/UIGlobalLoadingIndicatorService";
import {InviteCardSizeRepository} from "../repository/InviteCardSizeRepository";
import {GetAllInviteCardSizesResponse200} from "../../../../../definitions/src/definitions/invites/invite-card-sizes/paths/get-all";

@Injectable()
export class InviteCardSizeRepositoryResolve implements Resolve<GetAllInviteCardSizesResponse200>
{
	constructor(private repository: InviteCardSizeRepository,
		private status: UIGlobalLoadingIndicatorService) {
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
		let loading = this.status.createLoadingIndicator();
		let observable = this.repository.fetch();

		observable.subscribe(
			response => {
				loading.is = false;
			},
			error => {
				loading.is = false;
			}
		);

		return observable;
	}
}