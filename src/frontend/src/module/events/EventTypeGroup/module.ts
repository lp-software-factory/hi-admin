import {EventTypeGroupCRUDRoute} from "./routes/EventTypeGroupCRUDRoute/index";
import {CreateEventTypeGroupModal} from "./component/Modals/CreateEventTypeGroupModal/index";
import {EditEventTypeGroupModal} from "./component/Modals/EditEventTypeGroupModal/index";
import {DeleteEventTypeGroupModal} from "./component/Modals/DeleteEventTypeGroupModal/index";
import {EventTypeGroupRepository} from "./repository/EventTypeGroupRepository";
import {EventTypeGroupRepositoryResolve} from "./resolve/EventTypeGroupRepositoryResolve";
import {EventTypeGroupDataTable} from "./component/Elements/EventTypeGroupDatatable/index";
import {EventTypeGroupForm} from "./component/Forms/EventTypeGroupForm/index";
import {EventTypeGroupRESTService} from "../../../../definitions/src/services/event/event-type-group/EventTypeGroupRESTService";

export const HIEventTypeGroupModule = {
	routes: [
		EventTypeGroupCRUDRoute,
	],
	declarations: [
		EventTypeGroupForm,
		CreateEventTypeGroupModal,
		DeleteEventTypeGroupModal,
		EditEventTypeGroupModal,
		EventTypeGroupDataTable,
	],
	providers: [
		EventTypeGroupRepository,
		EventTypeGroupRepositoryResolve,
	],
};

export const HIEventTypeGroupRoutes = {
	path: 'event_type_group',
	component: EventTypeGroupCRUDRoute,
	pathMatch: 'full',
	resolve: [
		EventTypeGroupRepositoryResolve,
	],
};